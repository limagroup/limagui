from limagui.paramtree.tree.extra import create_tg_io_class
from .tangoionames import TangoLimaCtVideo
from .conversions import LIMA_TANGO_CONVERSIONS


TangoSupportedVideoMode = create_tg_io_class(TangoLimaCtVideo.SUPPORTEDVIDEOMODE,
                                         tango_name='getAttrStringValueList',
                                         tango_read_args=['video_mode'],
                                         tango_writable=False,
                                         tango_conversion=LIMA_TANGO_CONVERSIONS[TangoLimaCtVideo.SUPPORTEDVIDEOMODE])


TangoVideoLastImage = create_tg_io_class(TangoLimaCtVideo.LASTIMAGE,
                                         hidden=True)