# -*- coding: utf-8 -*-

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"


from six import string_types
from collections import Iterable, namedtuple

from .treeadapters.treeadapter import TreeAdapter
from .ioadapter import IoAdapterBase


NodeError = namedtuple('NodeError',
                       ['node_path',
                        'caller_lineno',
                        'caller_fn',
                        'exception',
                        'trace'])


def _is_list_type(value):
    return (isinstance(value, (Iterable,))
            and not isinstance(value, (string_types,))
            and not isinstance(value, (dict,)))


def _to_list(value):
    if _is_list_type(value):
        return value
    return list([value])


def _to_dict(value):
    if isinstance(value, (dict,)):
        return value
    else:
        try:
            return {i: val for i, val in enumerate(value)}
        except TypeError:
            return {1: value}


class Node(object):

    def __init__(self,
                 data=None,
                 name=None,
                 **kwargs):

        self._name = name
        self._started = False
        if isinstance(data, (IoAdapterBase,)):
            data_type = data.data_type()
            # data = data.data(cache=True)
            # print('NEW TREE', data_type, data)
        else:
            data_type = data.__class__
        self._tree_adapter = TreeAdapter(data_type, self, data, **kwargs)

    def _tree(self):
        return self._tree_adapter

    def name(self):
        return self._name

    def is_started(self):
        return self._started
    
    def _set_notify_change(self, change):
        """
        Method called when a change occurs in a cell
        """
        self._tree_adapter._set_notify_change(change)

    def _set_notify_pre_insert(self, pre_insert):
        """
        Method called prior to inserting a child
        """
        self._tree_adapter._set_notify_pre_insert(pre_insert)

    def _set_notify_post_insert(self, post_insert):
        """
        Method called after inserting a child
        """
        self._tree_adapter._set_notify_post_insert(post_insert)

    def _set_notify_pre_remove(self, pre_remove):
        """1
        Method called prior to removing a child
        """
        self._tree_adapter._set_notify_pre_remove(pre_remove)

    def _set_notify_post_remove(self, post_remove):
        """
        Method called after removing a child
        """
        self._tree_adapter._set_notify_post_remove(post_remove)

    def _set_notify_exception(self, notify_exception):
        """
        Method called to notify (something, like the QModel)
        of an exception in a cell
        """
        self._tree_adapter._set_notify_exception(notify_exception)

    def _notify_change(self):
        return self._tree_adapter._notify_change_fn()

    def _notify_pre_insert(self):
        return self._tree_adapter._notify_pre_insert_fn()

    def _notify_post_insert(self):
        return self._tree_adapter._notify_post_insert_fn()

    def _notify_pre_remove(self):
        return self._tree_adapter._notify_pre_remove_fn()

    def _notify_post_remove(self):
        return self._tree_adapter._notify_post_remove_fn()

    def _notify_exception(self):
        return self._tree_adapter._notify_exception_fn()

    def start_node(self):
        self._tree_adapter.start()
        self._started = True

    def stop_node(self, column=None):
        self._tree_adapter.stop()
        self._started = False

    def insert_node_data(self, data=None, row=-1, name=None):
        node = Node(name=name, data=data)
        self.insert_child(node, row)
        return node

    def insert_child(self, *args, **kwargs):
        return self._tree_adapter.insert_child(*args, **kwargs)

    def append_child(self, *args, **kwargs):
        return self._tree_adapter.append_child(*args, **kwargs)

    def child_count(self, *args, **kwargs):
        return self._tree_adapter.child_count(*args, **kwargs)

    def has_children(self):
        return self._tree_adapter.has_children()

    def parent(self):
        return self._tree_adapter.parent_node()

    def _set_parent(self, *args, **kwargs):
        return self._tree_adapter._set_parent(*args, **kwargs)

    def row(self):
        return self._tree_adapter.row()

    def column_count(self):
        return self._tree_adapter.cell_count()

    def child(self, index):
        return self._tree_adapter.child(index)

    def background_color(self, column):
        return self._tree_adapter.background_color(column)
 
    def tooltip(self, column):
        return self._tree_adapter.tooltip(column)
    
    def decoration(self, column):
        return self._tree_adapter.decoration(column)
    
    def foreground_color(self, column):
        return self._tree_adapter.foreground_color(column)
    
    def drag_enabled(self, column):
        return self._tree_adapter.drag_enabled(column)
    
    def drop_enabled(self, column):
        return self._tree_adapter.drop_enabled(column)
    
    def enabled(self, column):
        return self._tree_adapter.enabled(column)
    
    def editable(self, column):
        return self._tree_adapter.editable(column)
    
    def checked(self, column):
        return self._tree_adapter.checked(column)
    
    def checkable(self, column):
        return self._tree_adapter.checkable(column)
    
    def selectable(self, column):
        return self._tree_adapter.selectable(column)

    def set_background_color(self, column, background_color):
        return self._tree_adapter.set_background_color(column, background_color)

    def set_tooltip(self, column, tooltip):
        return self._tree_adapter.set_tooltip(column, tooltip)

    def set_decoration(self, column, decoration):
        return self._tree_adapter.set_decoration(column, decoration)

    def set_foreground_color(self, column, foreground_color):
        return self._tree_adapter.set_foreground_color(column, foreground_color)

    def set_drag_enabled(self, column, drag_enabled):
        return self._tree_adapter.set_drag_enabled(column, drag_enabled)

    def set_drop_enabled(self, column, drop_enabled):
        return self._tree_adapter.set_drop_enabled(column, drop_enabled)

    def set_enabled(self, column, enabled):
        return self._tree_adapter.set_enabled(column, enabled)

    def set_editable(self, column, editable):
        return self._tree_adapter.set_editable(column, editable)

    def set_checked(self, column, checked):
        return self._tree_adapter.set_checked(column, checked)

    def set_checkable(self, column, checkable):
        return self._tree_adapter.set_checkable(column, checkable)

    def set_selectable(self, column, selectable):
        return self._tree_adapter.set_selectable(column, selectable)

    def data_type(self, column):
        return self._tree_adapter.data_type(column)

    def text(self, column):
        """
        Text data to be displayed in a view.
        """
        return self._tree_adapter.text(column)

    def editor_data(self, column):
        """
        Data requested by an editor.
        """
        return self._tree_adapter.editor_data(column)

    def set_editor_data(self, column, data):
        """
        Data submitted by an editor.
        """
        self._tree_adapter.set_editor_data(column, data)

    def cell(self, column):
        return self._tree_adapter.cell(column)

    def view_adapter(self, column):
        return self._tree_adapter.view_adapter(column)

    def view_adapter_type(self, column):
        return self._tree_adapter.view_adapter_type(column)

    def view_size(self, column):
        return self._tree_adapter.view_size(column)

    def set_on_change_callback(self, column, callback):
        self._tree_adapter.set_on_change_callback(column, callback)
