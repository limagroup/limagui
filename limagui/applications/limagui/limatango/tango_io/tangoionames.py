# coding: utf-8

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"

from limagui.paramtree import IoNamesEnum


"""
In this file you can define substitutes for the LIMA Ios.
The replacement's enum names must have the same name as the ones
to be replaced.

If the new IO is not already registered (e.g in .ctcontrol.py)
it will be automatically created by the LimaTangoFactory.
"""

class LimaCCDsTango(IoNamesEnum):
    TANGO_STATUS = 'LimaCCDs@LimaCCDs:Status'
    TANGO_STATE = 'LimaCCDs@LimaCCDs:State'


class TangoLimaCCDs(IoNamesEnum):
    TANGO_PUSH_IMAGE_EVENTS = 'LimaCCDs@LimaCCDs:image_events_push_data'
    ACQ_STATUS_ERROR = 'LimaCCDs@LimaCCDs:acq_status_fault_error'


class TangoLimaCtAccumulation(IoNamesEnum):
    ACTIVE = 'LimaCCDs@LimaCCDs:acc_saturated_active'
    PIXEL_THRESHOLD_VALUE = 'LimaCCDs@LimaCCDs:acc_saturated_threshold'


class TangoLimaCtAcquisition(IoNamesEnum):
    LATENCYTIME = 'LimaCCDs@LimaCCDs:latency_time'
    ACQNBFRAMES = 'LimaCCDs@LimaCCDs:acq_nb_frames'
    CONCATNBFRAMES = 'LimaCCDs@LimaCCDs:concat_nb_frames'
    ACCEXPOTIME = 'LimaCCDs@LimaCCDs:acq_expo_time'
    ACCNBFRAMES = 'LimaCCDs@LimaCCDs:acc_nb_frames'
    ACCMAXEXPOTIME = 'LimaCCDs@LimaCCDs:acc_max_expo_time'
    ACCTIMEMODE = 'LimaCCDs@LimaCCDs:acc_time_mode'
    ACCDEADTIME = 'LimaCCDs@LimaCCDs:acc_dead_time'
    ACCLIVETIME = 'LimaCCDs@LimaCCDs:acc_live_time'
    ACQEXPOTIME = 'LimaCCDs@LimaCCDs:acq_expo_time'
    ACQMODE = 'LimaCCDs@LimaCCDs:acq_mode'
    TRIGGERMODE = 'LimaCCDs@LimaCCDs:acq_trigger_mode'
    # PARS = 'LimaCCDs@LimaCCDs:Pars'


class TangoLimaCtControl(IoNamesEnum):
    STOPACQ = 'LimaCCDs@LimaCCDs:stopAcq'
    PREPAREACQ = 'LimaCCDs@LimaCCDs:prepareAcq'
    STARTACQ = 'LimaCCDs@LimaCCDs:startAcq'
    ABORTACQ = 'LimaCCDs@LimaCCDs:abortAcq'
    RESET = 'LimaCCDs@LimaCCDs:reset'
    IMAGESTATUS = 'LimaCCDs@:__image_status'
    STATUS = 'LimaCCDs@:_Status'
    STATUS_ACQUISITIONSTATUS = 'LimaCCDs@LimaCCDs:acq_status'
    STATUS_CAMERASTATUS = 'LimaCCDs@:__limaccds_cam_status'
    STATUS_ERROR = 'LimaCCDs@LimaCCDs:acq_status_fault_error'
    # STATUS_IMAGECOUNTERS = 'LimaCCDs@LimaCCDs:Status/ImageCounters'

    READIMAGE = 'LimaCCDs@LimaCCDs:last_image'
    READBASEIMAGE = 'LimaCCDs@LimaCCDs:baseImage'

    # IMAGESTATUSCALLBACK = 'LimaCCDs@LimaCCDs:ImageStatusCallback'


class TangoLimaCtImage(IoNamesEnum):
    BIN = 'LimaCCDs@LimaCCDs:image_bin'
    ROI = 'LimaCCDs@LimaCCDs:image_roi'
    FLIP = 'LimaCCDs@LimaCCDs:image_flip'
    ROTATION = 'LimaCCDs@LimaCCDs:image_rotation'
    

class TangoLimaCtVideo(IoNamesEnum):
    ACTIVE = 'LimaCCDs@LimaCCDs:video_active'
    STARTLIVE = 'LimaCCDs@LimaCCDs:video_live'
    STOPLIVE = 'LimaCCDs@LimaCCDs:video_live'
    LIVE = 'LimaCCDs@LimaCCDs:video_live'
    EXPOSURE = 'LimaCCDs@LimaCCDs:video_exposure'
    LASTIMAGE = 'LimaCCDs@LimaCCDs:video_last_image'
    LASTIMAGECOUNTER = 'LimaCCDs@LimaCCDs:video_last_image_counter'
    GAIN = 'LimaCCDs@LimaCCDs:video_gain'
    MODE = 'LimaCCDs@LimaCCDs:video_mode'
    VIDEOSOURCE = 'LimaCCDs@LimaCCDs:video_source'
    SUPPORTEDVIDEOMODE = 'LimaCCDs@LimaCCDs:_SupportedVideoMode'
    ROI = 'LimaCCDs@LimaCCDs:video_roi'
    BIN = 'LimaCCDs@LimaCCDs:video_bin'


class TangoLimaHwSync(IoNamesEnum):
    CAP_SYNC_VALIDRANGES = 'LimaCCDs@LimaCCDs:valid_ranges'


# WARNING! ORDER is important! It is the order of arguments
# passed to the CtControl.ImageStatus constructor.
class LimaCCDsImageStatusNames(IoNamesEnum):
    LAST_IMAGE_ACQUIRED = 'LimaCCDs@LimaCCDs:last_image_acquired'
    LAST_BASE_IMAGE_READY = 'LimaCCDs@LimaCCDs:last_base_image_ready'
    LAST_IMAGE_READY = 'LimaCCDs@LimaCCDs:last_image_ready'
    LAST_IMAGE_SAVED = 'LimaCCDs@LimaCCDs:last_image_saved'
    LAST_COUNTER_READY = 'LimaCCDs@LimaCCDs:last_counter_ready'


# class LimaCCDsMisc(IoNamesEnum):
    # ACQUISITION_STATUS = 'LimaCCDs@LimaCCDs:acq_status'
    # ACQ_STATUS_ERROR = 'LimaCCDs@LimaCCDs:acq_status_fault_error'
    # ACQ_STATUS_CAMERA_STATUS = 'LimaCCDs@LimaCCDs:acq_status_fault_error'


class TangoLimaTriggers(IoNamesEnum):
    STATUS_CALLBACK_IMG = 'LimaCCDs@LimaCCDs:last_image'
    STATUS_CALLBACK_CNT = 'LimaCCDs@LimaCCDs:last_image_ready'
    STATUS_CALLBACK = 'LimaCCDs@:_status_callback'
    STATUS_CALLBACK_STATUS = 'LimaCCDs@LimaCCDs:acq_status'
    LASTIMAGE = 'LimaCCDs@LimaCCDs:readImage'


LIMA_TANGO_IO_NAMES = [TangoLimaCCDs,
                       TangoLimaCtAccumulation,
                       TangoLimaCtAcquisition,
                       TangoLimaCtControl,
                       TangoLimaCtImage,
                       TangoLimaCtVideo,
                       TangoLimaHwSync,
                       LimaCCDsImageStatusNames,
                       LimaCCDsTango]