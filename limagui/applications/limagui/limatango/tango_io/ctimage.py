from Lima import Core
from limagui.paramtree.tree.extra import create_tg_io_class
from .tangoionames import TangoLimaCtImage
from .conversions import LIMA_TANGO_CONVERSIONS


TangoImageFlip = create_tg_io_class(TangoLimaCtImage.FLIP,
                                    data_type=Core.Flip,
                                    tango_conversion=LIMA_TANGO_CONVERSIONS[TangoLimaCtImage.FLIP])
