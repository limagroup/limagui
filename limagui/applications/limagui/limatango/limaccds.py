from .tango_io import *
from ..lima.lima_io.limaionames import LimaImages


def match_io_enum_names(sub_klass, klass_to_sub):
    sub = [(sub_io, klass_to_sub.__members__[sub_io.name])
           for sub_io in sub_klass
           if sub_io.name in klass_to_sub.__members__]
    return sub


def limaccds_add_substitutes(io_group):
    subs = (match_io_enum_names(TangoLimaCtAccumulation, CtAccumulation)
            + match_io_enum_names(TangoLimaCtControl, CtControl)
            + match_io_enum_names(TangoLimaCtVideo, CtVideo)
            + match_io_enum_names(TangoLimaCtImage, CtImage)
            + match_io_enum_names(TangoLimaCtAcquisition, CtAcquisition)
            + match_io_enum_names(TangoLimaHwSync, HwInterface))

    for (new, orig) in subs:
        io_group.set_substitute(orig, new)

    # io_group.set_substitute(LimaTriggers.STATUS_CALLBACK,
    #                         TangoLimaTriggers.STATUS_CALLBACK)

    io_group.set_substitute(LimaTriggers.STATUS_CALLBACK,
                            TangoLimaCtControl.IMAGESTATUS)

    io_group.set_substitute(LimaImages.LASTIMAGE,
                            TangoLimaTriggers.LASTIMAGE)

    # io_group.set_substitute(CtAcquisition.ACQNBFRAMES, TangoLimaCtAcquisition.ACQNBFRAMES)

def limaccds_import_bpm(io_group):
    if io_group.has_source('BpmDeviceServer'):
        from .tango_io.plugins.bpmdeviceserver import BpmDeviceServerNames
        return True
    return False


def device_list(srv_name):
    # TODO : error management
    db = tango.Database()
    if not srv_name.startswith('LimaCCDs/'):
        srv_name = 'LimaCCDs/' + srv_name
    devices = db.get_device_class_list(srv_name)
    return dict(zip(*[reversed(devices)] * 2))


def new_lima_ccds(srv_name):
    dev_dict = device_list(srv_name)

    print(f'Looking for LimaCCDs (srv_name={srv_name})')
    try:
        lima_ccds = tango.DeviceProxy(dev_dict['LimaCCDs'])
    except KeyError as ex:
        print('Error ', ex)
        raise RuntimeError('LimaCCDs Tango device not found: {0}'
                            ''.format(srv_name))
    print(f'OK. LimaCCDs device found: {lima_ccds.name()}.')

    print(f'Looking for BpmDeviceServer device.')
    bpm_device = dev_dict.get('BpmDeviceServer')
    if bpm_device:
        bpm_device = tango.DeviceProxy(bpm_device)
        print(f'OK: found a BpmDeviceServer: {bpm_device.name()}!')
    else:
        print(f'INFO: BpmDeviceServer not found for this LimaCCDs ({srv_name}).')
    return dict([('LimaCCDs', lima_ccds), ('BpmDeviceServer', bpm_device)])


def limaccds_io_tree(io_group):
    tree = {}

    ios = [io_group.io_adapter(io_name, hidden=False)
           for limaccdsenum in LIMA_TANGO_IO_NAMES
           for io_name in limaccdsenum]
    tree['LimaCCDs'] = {io.io_name.name:io for io in ios if io}
    return tree