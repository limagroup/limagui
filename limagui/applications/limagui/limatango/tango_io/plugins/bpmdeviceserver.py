# coding: utf-8
from typing import NamedTuple, Any
from limagui.paramtree.tree.ioadapter import IoAdapterBase, ioadapterdef

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


import cv2
import enum
import time
import base64
import struct
from collections import deque
from threading import Event

import numpy as np

import tango

# from limagui.paramtree.iomodel import (IOBase,
#                                        ioclassdef,
#                                        create_io_class,
#                                        IoNamesEnum,
#                                        get_io_class)

from ....lima.lima_io.limaionames import LimaTriggers, CtControl
from limagui.paramtree import IoNamesEnum, get_io_adapter_class, io_name_is_registered, Runner
from limagui.paramtree.tree.extra.tangoioadapter import TangoIoAdapter, create_tg_io_class
from limagui.paramtree.tree.extra import create_tg_io_class


class BpmDeviceServerNames(IoNamesEnum):
    START = 'BpmDeviceServer@BpmDeviceServer:Start'
    STOP = 'BpmDeviceServer@BpmDeviceServer:Stop'
    STATE = 'BpmDeviceServer@BpmDeviceServer:State'
    FWHM_X = 'BpmDeviceServer@BpmDeviceServer:fwhm_x'
    FWHM_Y = 'BpmDeviceServer@BpmDeviceServer:fwhm_y'
    COLORMAP = 'BpmDeviceServer@BpmDeviceServer:color_map'
    BVDATA_EVENT = 'BpmDeviceServer@BpmDeviceServer:bvdata'
    AUTOSCALE = 'BpmDeviceServer@BpmDeviceServer:autoscale'
    X = 'BpmDeviceServer@BpmDeviceServer:x'
    Y = 'BpmDeviceServer@BpmDeviceServer:y'
    LUT = 'BpmDeviceServer@BpmDeviceServer:lut_method'
    BVDATA = 'BpmDeviceServer@:_bvdata'


class BpmLutEnum(enum.Enum):
    LINEAR = 'LINEAR'
    LOG = 'LOG'


def get_bpm_tango_io_class():
    tango_io_names = [e for e in BpmDeviceServerNames]
    tango_io_classes = []

    for io_name in tango_io_names:
        klass = get_io_adapter_class(io_name)
        if klass is None:
            klass = create_tg_io_class(io_name)
        tango_io_classes.append(klass)

    return tango_io_classes


BpmDeviceStatus = create_tg_io_class(BpmDeviceServerNames.STATE,
                                  data_type=tango.DevState,
                                  triggers=[BpmDeviceServerNames.START,
                                           BpmDeviceServerNames.STOP],
                                  refresh_delay=500)


TangoBpmFwhmX = create_tg_io_class(BpmDeviceServerNames.FWHM_X,
                                data_type=float,
                                enable_condition=[(BpmDeviceServerNames.STATE, lambda data: data == tango.DevState.ON)])


TangoBpmFwhmY = create_tg_io_class(BpmDeviceServerNames.FWHM_Y,
                                data_type=float,
                                enable_condition=[(BpmDeviceServerNames.STATE, lambda data: data == tango.DevState.ON)])


TangoBpmColormap = create_tg_io_class(BpmDeviceServerNames.COLORMAP,
                                   data_type=bool,
                                   enable_condition=[(BpmDeviceServerNames.STATE, lambda data: data == tango.DevState.ON)],
                                   icon='colormap')


# TangoBpmBvdata = create_io_class(BpmDeviceServerNames.BVDATA,
#                                 base_class=(TangoIoMixin, IOBase,),
#                                 data_type=tango.DevState,
#                                 hidden=True,
#                                 enable_condition=[(BpmDeviceServerNames.STATE, lambda data: data == tango.DevState.ON)])


TangoBpmBvdataEvent = create_tg_io_class(BpmDeviceServerNames.BVDATA_EVENT,
                                        #  data_type=tango.DevState,
                                         hidden=True,
                                         tango_events = tango.EventType.CHANGE_EVENT,
                                         tango_event_same_thread = True)


TangoBpmAutoscale = create_tg_io_class(BpmDeviceServerNames.AUTOSCALE,
                                data_type=bool,
                                icon='plot-yauto',
                                enable_condition=[(BpmDeviceServerNames.STATE, lambda data: data == tango.DevState.ON)])


TangoBpmX = create_tg_io_class(BpmDeviceServerNames.X,
                                data_type=float,
                                enable_condition=[(BpmDeviceServerNames.STATE, lambda data: data == tango.DevState.ON)])


TangoBpmY = create_tg_io_class(BpmDeviceServerNames.Y,
                                data_type=float,
                                enable_condition=[(BpmDeviceServerNames.STATE, lambda data: data == tango.DevState.ON)])


TangoBpmLut = create_tg_io_class(BpmDeviceServerNames.LUT,
                                data_type=BpmLutEnum,
                                enable_condition=[(BpmDeviceServerNames.STATE, lambda data: data == tango.DevState.ON)])


class BvData(NamedTuple):
    counter: int = -1
    status: Any = None
    image: Any = None
    start_time: float = 0
    intensity: float = np.nan
    x: float = np.nan
    y: float = np.nan
    proj_x: Any = None
    proj_y: Any = None


@ioadapterdef(BpmDeviceServerNames.BVDATA,
              data_type=BvData,
              triggers=[BpmDeviceServerNames.BVDATA_EVENT,
                        LimaTriggers.IMG_POLL_DELAY,
                        LimaTriggers.ACQSTATUS_CHANGE],
              sink=True,
              hidden=True)
class TangoBpmBvdata(IoAdapterBase):
    # def __init__(self, *args, **kwargs):
    #     super(TangoBpmBvdata, self).__init__(*args, **kwargs)
    #     self._queue = deque(maxlen=1)
        # self._last_counter = None
        # self._bpm_data = None
        # self._last_start_time = None
        # self._last_status = None
        # self._start_timestamp = None
        # self._last_image = None
        # self._last_image_status = None
        # self._last_event_cnt = None

    def __init__(self, *args, **kwargs):
        self._status_io = None
        self._queue = deque(maxlen=2)
        self._bvdata = BvData()
        self._event = Event()
        self._img_poll_s = 0.05
        super().__init__(*args, **kwargs)

    def _on_start(self):
        self._runner = Runner(self._process_bvdata)
        self._run_loop = True
        self._runner.start()
        return super()._on_start()

    def _on_stop(self):
        self._run_loop = False
        self._event.set()
        self._runner.wait()
        self._runner = None
        return super()._on_stop()

    def _process_bvdata(self):
        while self._run_loop:
            try:
                sender, data = self._queue.popleft()
            except IndexError:
                self._event.wait()
                self._event.clear()
                continue

            if data is None:
                continue
            if sender == LimaTriggers.ACQSTATUS_CHANGE:
                bv_dict = self._bvdata._asdict()
                bv_dict['status'] = data[0]
                self._bvdata = BvData(**bv_dict)
            else:
                status = self._bvdata.status
                # if self._status_io is None:
                    # self._status_io = self.io_group().io_adapter(CtControl.STATUS)
                # status = self._status_io.data(cache=True).AcquisitionStatus

                data_struct = struct.unpack(data[0], data[1])
                counter = data_struct[1]
                x = data_struct[2]
                y = data_struct[3]
                intensity = data_struct[4]

                jpeg = base64.b64decode(data_struct[14])
                image = cv2.imdecode(np.frombuffer(jpeg, dtype=np.uint8),
                                    cv2.IMREAD_COLOR)

                try:
                    projx = np.frombuffer(data_struct[12], dtype=np.int64)
                except Exception as ex:
                    print('=== projX: ', ex)
                    projx = None

                try:
                    projy = np.frombuffer(data_struct[13], dtype=np.int64)
                except Exception as ex:
                    print('=== projY: ', ex)
                    projy = None

                if counter == 0 or self._bvdata.start_time == 0:
                    start_time = time.time()
                else:
                    start_time = self._bvdata.start_time

                self._bvdata = BvData(counter, status, image, start_time, intensity, x, y, projx, projy)

            self.set_data(self._bvdata, _internal=True)

        
    def _on_trigger(self, sender=None, data=None, **kwargs):
        if sender == LimaTriggers.IMG_POLL_DELAY:
            self._img_poll_s = data / 1000.
        else:
            self._queue.append([sender, data])
            self._event.set()



            # if data is None:
            #     continue
            # if sender is None:#LimaTriggers.ACQSTATUS_CHANGE:
            #     self._bvdata.status = data
            # else:
            #     status = self._bvdata.status
            #     # if self._status_io is None:
            #         # self._status_io = self.io_group().io_adapter(CtControl.STATUS)
            #     # status = self._status_io.data(cache=True).AcquisitionStatus

            #     data_struct = struct.unpack(data[0], data[1])
            #     counter = data_struct[1]
            #     x = data_struct[2]
            #     y = data_struct[3]
            #     intensity = data_struct[4]

            #     jpeg = base64.b64decode(data_struct[14])
            #     image = cv2.imdecode(np.frombuffer(jpeg, dtype=np.uint8),
            #                         cv2.IMREAD_COLOR)

            #     try:
            #         projx = np.frombuffer(data_struct[12], dtype=np.int64)
            #     except Exception as ex:
            #         print('=== projX: ', ex)
            #         projx = None

            #     try:
            #         projy = np.frombuffer(data_struct[13], dtype=np.int64)
            #     except Exception as ex:
            #         print('=== projY: ', ex)
            #         projy = None

            #     if counter == 0 or self._bvdata.start_time == 0:
            #         start_time = time.time()
            #     else:
            #         start_time = self._bvdata.start_time

            #     self._bvdata = BvData(counter, status, image, start_time, intensity, x, y, projx, projy)

            # self.set_data(self._bvdata, _internal=True)

    # def _trigger(self, sender=None, data=None, **kwargs):
    #     trigger = False
        
    #     if data is not None:
    #         data_struct = struct.unpack(data[0], data[1])
    #         counter = data_struct[1]
    #         x = data_struct[2]
    #         y = data_struct[3]
    #         intensity = data_struct[4]

    #         jpeg = base64.b64decode(data_struct[14])
    #         image = cv2.imdecode(np.frombuffer(jpeg, dtype=np.uint8),
    #                             cv2.IMREAD_COLOR)

    #         try:
    #             projx = np.frombuffer(data_struct[12], dtype=np.int64)
    #         except Exception as ex:
    #             print('=== projX: ', ex)
    #             projx = None

    #         try:
    #             projy = np.frombuffer(data_struct[13], dtype=np.int64)
    #         except Exception as ex:
    #             print('=== projY: ', ex)
    #             projy = None

    #         if counter == 0 or self._start_timestamp is None:
    #             self._start_timestamp = time.time()
    #             trigger = True

    #         self._queue.append((self._start_timestamp, counter,
    #                             x, y, intensity, image, projx, projy))

    #         if trigger:
    #             self.data(cache=False)

    # def io_getter(self, source):
    #     try:
    #         bpm_data = self._queue.popleft()
    #         self._bpm_data = bpm_data
    #     except IndexError:
    #         pass
    #     return self._bpm_data


# @ioadapterdef(LimaImages.LASTIMAGE,
#               sink=True,
#               triggers=[LimaTriggers.IMAGEREADY],
#               hidden=True)
# class LimaLastImage(IoAdapterBase):
#     def __init__(self, *args, **kwargs):
#         self._queue = deque(maxlen=2)
#         self._image_data = LastImage()
#         self._last_status = None
#         self._event = Event()
#         super().__init__(*args, **kwargs)

#     def _on_start(self):
#         self._runner = Runner(self._get_image)
#         self._run_loop = True
#         self._runner.start()
#         return super()._on_start()

#     def _on_stop(self):
#         self._run_loop = False
#         self._event.set()
#         self._runner.wait()
#         self._runner = None
#         return super()._on_stop()

#     def _read_image(self, counter=None):
#         image = self.source().ReadImage(counter)
#         image = lima_processlib_data_to_image(image)
#         return image

#     def _get_image(self):
#         while self._run_loop:
#             try:
#                 sender, data = self._queue.popleft()
#             except IndexError:
#                 self._event.wait()
#                 self._event.clear()
#                 continue

#             status = self._image_data.status
#             image = self._image_data.image
#             counter = self._image_data.counter
#             timestamp = self._image_data.start_time

#             new_timestamp, last_counter, status  = data

#             if last_counter >= 0:
#                 # - counter changed
#                 # - same counter but timestamp changed: different run
#                 # In both cases: get last image
#                 if last_counter != counter or new_timestamp != timestamp:
#                     try:
#                         if self._substitute:
#                             image = self._substitute_io._read_image(counter=last_counter)
#                         else:
#                             image = self._read_image(counter=last_counter)
#                     except Core.Exception as ex:
#                         print(f'WARNING in {self.__class__}: {ex}')
#                         continue
#                     counter = last_counter

#             self._image_data = LastImage(status=status,
#                                          counter=counter,
#                                          image=image,
#                                          start_time=new_timestamp)
#             self.set_data(self._image_data, _internal=True)
        
#     def _on_trigger(self, sender=None, data=None, **kwargs):
#         self._queue.append([sender, data])
#         self._event.set()


ios = [create_tg_io_class(io_name,
                          hidden=False)
       for io_name in BpmDeviceServerNames
       if not io_name_is_registered(io_name)]