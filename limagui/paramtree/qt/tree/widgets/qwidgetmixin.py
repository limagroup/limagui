__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from limagui.paramtree.utils.observer import Observer
from .... import Cell
from ....utils.utils import DefaultODict

from ... import Qt
from ...qobserver import QObserver
from ..qhelpers import nodeFromModelIndex


class _QCellWidgetMixin:
    def _cell_view_adapter(self):
        return self.cell.view_adapter()

    def _cell_set_enabled(self, enabled):
        try:
            self.setEnabled(enabled)
        except AttributeError:
            pass

    def _cell_set_active(self, enable):
        pass

    def _cell_set_editable(self, editable):
        try:
            self.setEditable(editable)
        except AttributeError:
            pass

    # def _cell(self):
    #     raise NotImplementedError()

    def _cell_editor_commit(self, data):
        """
        Widget to model
        """
        raise NotImplementedError()

    def _cell_set_data(self, data):
        """
        Model to widget
        """
        pass
        # raise NotImplementedError()

    def _cell_set_view_data(self, view_data):
        """
        Model to widget
        """
        pass
        # raise NotImplementedError()
    
    def _cell_set_color(self, color):
        pass

    def _cell_set_decoration(self, decoration):
        pass

    def _cell_set_text(self, text):
        pass

    def _cell_set_tooltip(self, tooltip):
        pass

    def _cell_set_editable(self, editable):
        try:
            self.setEditable(editable)
        except AttributeError:
            pass

    def _cell_reset(self, reset):
        self._resetup()

    def _resetup(self):
        pass

    def _data_changed(self,
                      cell,
                      data=None,
                      view_data=None,
                      enabled=None,
                      active=None,
                      decoration=None,
                      color=None,
                      text=None,
                      tooltip=None,
                      editable=None,
                      reset=None,
                      **kwargs):
        try:
            no_focus = not self.hasFocus()
        except AttributeError:
            no_focus = True

        if reset is not None:
            self._cell_reset(reset)

        if data is not None and no_focus:
            self._cell_set_data(data)

        if view_data is not None and no_focus:
            self._cell_set_view_data(view_data)

        if active is not None:
            self._cell_set_active(active)

        if enabled is not None:
            self._cell_set_enabled(enabled)

        if decoration is not None:
            self._cell_set_decoration(decoration)

        if color is not None:
            self._cell_set_color(color)

        if text is not None:
            self._cell_set_text(text)

        if tooltip is not None:
            self._cell_set_tooltip(tooltip)

        if editable is not None:
            self._cell_set_editable(editable)

    def _cell_refresh(self):
        cell = self.cell
        values = cell.get_all_data()
        self._data_changed(cell, **values)


class QCellWidgetMixin(_QCellWidgetMixin):
    cell = property(lambda self: self._cell)

    def __init__(self,
                 data=None,
                #  _own_cell=False,
                #  _is_delegate=False,
                 **kwargs):
        self._is_init = False
        parent = kwargs.pop('parent', None)
        super(QCellWidgetMixin, self).__init__(parent)
        self._observer = QObserver(callback=self._data_changed)
        self._committed_data = None
        # self._own_cell = False
        # self._observed = False
        self.setCellWidgetData(data)

    def setCellWidgetData(self, data):
        if data is None:
            self._observer.clear()
            self.setEnabled(False)
            return

        if isinstance(data, (Qt.QModelIndex, Qt.QPersistentModelIndex,)):
            cell = nodeFromModelIndex(data).cell(data.column())
            # print('IDX CELL', idx_cell)
            # io_adapter = idx_cell.io_adapter()
            # cell = Cell(data=io_adapter)
            # cell.start()
            # if not _is_delegate:
            #     # TODO : put this in showEvent
            #     self._observed = True
            #     self._observe_cell()
        elif not isinstance(data, (Cell,)):
            cell = Cell(data=data, parent=self)
            # self._own_cell = True
            cell.start()
        else:
            cell = data
            # if _own_cell:
            #     # TODO : put this in showEvent
            #     self._this_cell._set_parent(self)
            #     self._own_cell = True
            # else:
            #     # TODO : put this in showEvent
            #     self._observed = True
            #     self._observe_cell()
        cell.register(self._observer)
        self._cell = cell

        tooltip = self._cell.tooltip()
        if tooltip:
            try:
                self.setToolTip(tooltip)
            except AttributeError:
                pass
            try:
                self.setStatusTip(tooltip)
            except AttributeError:
                pass
    
        editable = self._cell.editable()
        try:
            self.setEditable(editable)
        except AttributeError:
                pass

        self._setupCellWidget()

    def _setupCellWidget(self):
        pass

    def _cell_set_tooltip(self, tooltip):
        self.setToolTip(tooltip)

    # def _observe_cell(self):
    #     if self._observed:
    #         if self._observer is None:
    #             self._observer = QObserver(callback=self._data_changed)
    #         self._this_cell.register(self._observer)
    #     elif self._own_cell:
    #         self._cell().set_parent(self)

    # def _unobserve_cell(self):
    #     self._observer.clear()
        # if self._observed:
        #     if self._observer:
        #         self._observer.clear()
        #         self._observer = None
        # elif self._own_cell:
        #     self.cell.set_parent(None)

    # def _cell(self):
    #     return self._this_cell

    def _cell_editor_commit(self, data):
        self._committed_data = data
        self.cell.set_view_data(data)
        # if self._callback:
        #     self._callback(self)

    # def _data_changed(self,
    #                   cell,
    #                   data=None,
    #                   enabled=None,
    #                   active=None,
    #                   color=None,
    #                   **kwargs):
    #     if data is not None:
    #         self._cell_set_data(data)

    #     if active is not None:
    #         self._cell_set_active(active)

    #     if enabled is not None:
    #         self._cell_set_enabled(enabled)

    def showEvent(self, *args, **kwargs):
        super(QCellWidgetMixin, self).showEvent(*args, **kwargs)
        if not self._is_init:
            self._cell_refresh()
            self._is_init = True


_registered_qcellwidgets = DefaultODict(None)


def register_qcellwidgets(data_type, editor_class):
    global _registered_qcellwidgets

    if data_type not in _registered_qcellwidgets:
        _registered_qcellwidgets[data_type] = editor_class
    else:
        raise ValueError('A QCellWidget has already been registered '
                         'for data_type [{0}] '.format(data_type))


def qcellwidgetdef(data_type,
               **kwargs):
    def inner(cls):
        register_qcellwidgets(data_type, cls)
        return cls

    return inner


def qcellwidget_for_type(data_type):
    from .widgets import QCellLabelWidget, QCellDefaultWidget
    if data_type is None:
        return None

    found_klass = None
    found_type = None
    for key, value in _registered_qcellwidgets.items():
        if issubclass(data_type, (key,)):
            if found_type:
                if issubclass(key, (found_type,)):
                    found_type = key
                    found_klass = value
            else:
                found_type = key
                found_klass = value

    return found_klass or QCellDefaultWidget


def QCellWidgetClass(data, *args, **kwargs):
    if isinstance(data, (Qt.QModelIndex, Qt.QPersistentModelIndex,)):
        cell = nodeFromModelIndex(data).cell(data.column())
    elif not isinstance(data, (Cell,)):
        cell = Cell(data=data)
    else:
        cell = data
        
    wid = qcellwidget_for_type(cell.view_adapter().adapter_type())
    return wid


def QCellWidget(data, *args, **kwargs):
    wid = QCellWidgetClass(data)
    return wid(data,
               *args,
               **kwargs)