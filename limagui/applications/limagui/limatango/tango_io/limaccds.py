# coding: utf-8

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from tango import DevState
from limagui.paramtree import IoAdapterBase, create_io_class, ioadapterdef
from limagui.paramtree.tree.extra import TangoIoAdapter, create_tg_io_class
from .tangoionames import TangoLimaCCDs, LimaCCDsTango


TangoStateColors = {
    DevState.ON: 'blue',
    DevState.RUNNING: 'green',
    DevState.OFF: 'orange',
    DevState.FAULT: 'red',
    DevState.STANDBY: 'pink',
    DevState.UNKNOWN: 'black'
    # CLOSE, OPEN, INSERT, EXTRACT, MOVING, STANDBY, INIT, ALARM, DISABLE, UNKNOWN.
}


@ioadapterdef(TangoLimaCCDs.TANGO_PUSH_IMAGE_EVENTS,
              data_type=bool)
class TangoPushImgEvents(TangoIoAdapter):
    def _on_start(self):
        rc = super()._on_start()
        if rc:
            limaccds = self.source()
            prop = limaccds.get_property('TangoEvent').get('TangoEvent')
            # if prop and prop[0]:
            #     self.set_refresh_delay(1000)
            # else:
            #     self.set_refresh_delay(0)
            #     self.set_enabled(False)
        return rc


LimaCCDsTangoState = create_tg_io_class(LimaCCDsTango.TANGO_STATE,
                                        data_type=DevState,
                                        poll_ms=2000,
                                        tango_read_devfailed=DevState.UNKNOWN)


LimaCCDsTangoStatus = create_tg_io_class(LimaCCDsTango.TANGO_STATUS,
                                        data_type=str,
                                        poll_ms=2000)