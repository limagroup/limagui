# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from . import Qt
from ..utils.observer import ObserverBase, add_observer


class _SignalHandler(Qt.QObject):
    signal = Qt.Signal(object)

    def emitSig(self, *args, **kwargs):

        self.signal.emit([args, kwargs])


class QObserver(ObserverBase):

    def __init__(self, *args, **kwargs):
        super(QObserver, self).__init__(*args, **kwargs)

        self.__handler = _SignalHandler()
        self.__handler.signal.connect(self._unpack)

    def _unpack(self, data):
        super(QObserver, self).notify(*data[0], **data[1])

    def notify(self, *args, **kwargs):
        self.__handler.emitSig(*args, **kwargs)


add_observer('QObserver', QObserver)
