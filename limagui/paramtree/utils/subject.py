# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from weakref import WeakSet, proxy
from collections import OrderedDict


_subjects = OrderedDict()


def add_subject(name, subject):
    global _subjects

    if name in _subjects:
        raise ValueError('An subject with this name has already been'
                         'registered.')

    _subjects[name] = subject


def get_subject(name=None):
    global _subjects
    if not _subjects:
        raise ValueError('No subject has been registered yet.')
    if name:
        return _subjects[name]
    return _subjects[list(_subjects.keys())[0]]


def Subject(*args, **kwargs):
    sname = kwargs.pop('sname', None)
    subj_class = get_subject(name=sname)
    return subj_class(*args, **kwargs)


class SubjectBase(object):

    uid = property(lambda self: self._uid)

    observers = property(lambda self: self._observers)

    def __init__(self, parent=None, uid=None):
        super(SubjectBase, self).__init__()
        if parent is not None:
            self._parent = proxy(parent)
        else:
            self._parent = None
        self._uid = (uid is not None and uid) or str(id(self))
        self._observers = WeakSet()

    def register(self, observer):
        if observer in self._observers:
            return
        self._observers.add(observer)
        observer.register(self)

    def is_observed(self):
        return len(self._observers) > 0

    def notify(self,
               *args,
               **kwargs):
            #    data=None,
            #    enabled=None,
            #    state=None,
            #    event=None):
        # creating a list to iterate over so we dont get an error
        # if some observers are added during notification
        # TODO : improve? deque? not sure it is really necessary
        if len(self._observers) == 0:
            return
        # if event is None:
        #     event = Event(self.uid, data=data, enabled=enabled, state=state)
        # try:
        #     # TODO : doesn't work when event is an EventQueue
        #     if event.is_empty():
        #         return
        # except:
        #     pass
        for observer in list(self._observers):
            observer.notify(*args, **kwargs)

    def unregister(self, observer):
        self._observers.discard(observer)
