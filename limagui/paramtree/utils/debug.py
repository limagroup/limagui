# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from collections import deque
from time import clock
from datetime import datetime

__depth = 1
__depthClock = deque()


def debugFn(func):
    def inner(*args, **kwargs):
        global __depth
        __depth += 1
        tab = '==' * __depth
        print('(' + str(__depth) + ') ' + tab + '> CALL IN', func.__name__)
        print('   ' + ' ' * __depth + '## args=', args)
        print('   ' + ' ' * __depth + '## kwargs=', kwargs)
        result = func(*args, **kwargs)
        print('(' + str(__depth) + ') ' + '<' + tab + ' CALL OUT',
              func.__name__, '## result=', result)
        __depth -= 1
        return result

    return inner


def timeFn(func):
    def inner(*args, **kwargs):
        global __depthClock
        tab = ' // ' + '  ' * len(__depthClock) + '|--> '
        __depthClock.append((clock(), datetime.now()))
        result = func(*args, **kwargs)
        clockIn, dateIn = __depthClock.pop()
        clockTotal = clock() - clockIn
        now = datetime.now()
        dateTotal = now - dateIn
        print(now.strftime("%H:%M:%S") + '.{0:>03}'.format(
            now.microsecond / 1000) + tab + 'CALLED ',
              func.__name__, ', CPU =', clockTotal, 'USER = ' + str(dateTotal))
        return result

    return inner
