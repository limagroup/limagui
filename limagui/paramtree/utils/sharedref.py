from .observer import SubjectObserver, Subject, Observer

class SharedRef(object):
    
    def __init__(self, data=None):
        self.__subject = Subject(parent=self)
        self.__data = data
        
    def setData(self, data):
        self.__data = data
        self.__subject.notifyChange()
        
    def subject(self):
        return self.__subject
        
    def data(self):
        return self.__data
