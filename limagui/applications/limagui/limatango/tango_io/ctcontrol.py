# coding: utf-8

from __future__ import absolute_import
from types import new_class
from limagui.applications.limagui.limatango.tango_io import triggers

from limagui.paramtree.tree.ioadapter import ioadapterdef

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from Lima import Core
from collections import OrderedDict

import tango

from limagui.paramtree import IoAdapterBase, create_io_class
from limagui.paramtree.tree.extra import TangoIoAdapter, create_tg_io_class

from .tangoionames import (TangoLimaCtControl,
                           LimaCCDsImageStatusNames)


def _str2tangoacqstatus(acqstr):
    state2string = {"Ready": Core.AcqReady,
                    "Running": Core.AcqRunning,
                    "Fault": Core.AcqFault,
                    "Configuration": Core.AcqConfig}
    # TODO : catch
    return state2string[acqstr]


TangoAcqStatus = create_tg_io_class(TangoLimaCtControl.STATUS_ACQUISITIONSTATUS,
            data_type=Core.AcqStatus,
            tango_events = tango.EventType.CHANGE_EVENT,
            hidden=True,
            tango_conversion = (_str2tangoacqstatus, None))


# class TangoImageCounter(TangoIoAdapter):



for io_name in LimaCCDsImageStatusNames:
    create_tg_io_class(io_name,
                       tango_events=tango.EventType.CHANGE_EVENT,
                       tango_event_same_thread=False)

# tt = create_tg_io_class(LimaCCDsImageStatusNames.LAST_IMAGE_ACQUIRED,
#                        tango_events=tango.EventType.CHANGE_EVENT,
#                        tango_event_same_thread=False)

    

@ioadapterdef(TangoLimaCtControl.IMAGESTATUS,
              data_type=Core.CtControl.ImageStatus,
              triggers=[io_name for io_name in LimaCCDsImageStatusNames])
class TangoImageStatus(IoAdapterBase):
    def __init__(self, *args, **kwargs):
        kwargs['io_getter'] = self._get_status
        super().__init__(*args, **kwargs)
        self._counter_ios = dict()
        self._counters = OrderedDict([(io_name, -1) for io_name in LimaCCDsImageStatusNames])
        self._img_status = Core.CtControl.ImageStatus()
        # self._io_getter = lambda src: self._img_status
    
    def _on_start(self):
        if not self.started():
            self._counter_ios = {io_name:self._io_group.io_adapter(io_name) for io_name in LimaCCDsImageStatusNames}
        super()._on_start()

    def _on_stop(self):
        self._counter_ios = dict()
        return super()._on_stop()

    def _on_trigger(self, sender=None, data=None, **kwargs):
        # print('IST', sender, data, self._counters[sender])
        if data != self._counters[sender]:
            self._counters[sender] = data
            self._img_status = Core.CtControl.ImageStatus(*self._counters.values())
            self.set_data(self._img_status, _internal=True)
        #     print('KEEP')
        # else:
        #     print('DISCARD')
        # return super()._on_trigger(sender=sender, data=data, **kwargs)

    def _get_status(self, source):
        for io in self._counter_ios.values():
            cnt = io.data(cache=False, _notify=False)
            self._counters[io.io_name] = cnt
        self._img_status = Core.CtControl.ImageStatus(*self._counters.values())
        # print(self._img_status)
        return self._img_status


def _cmp_img_status(status_a, status_b):
    return not (status_a.LastImageAcquired != status_b.LastImageAcquired or
                status_a.LastBaseImageReady != status_b.LastBaseImageReady or
                status_a.LastImageReady != status_b.LastImageReady or
                status_a.LastImageSaved != status_b.LastImageSaved or
                status_a.LastCounterReady != status_b.LastCounterReady)


@ioadapterdef(TangoLimaCtControl.STATUS,
    data_type=Core.CtControl.Status,
    triggers = [TangoLimaCtControl.STATUS_ACQUISITIONSTATUS,
                TangoLimaCtControl.STATUS_ERROR,
                TangoLimaCtControl.STATUS_CAMERASTATUS,
                TangoLimaCtControl.IMAGESTATUS])
class TangoLimaStatus(IoAdapterBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._status_dict = OrderedDict(
                [(TangoLimaCtControl.STATUS_ACQUISITIONSTATUS, Core.AcqReady),
                 (TangoLimaCtControl.STATUS_ERROR, Core.CtControl.NoError),
                 (TangoLimaCtControl.STATUS_CAMERASTATUS, Core.CtControl.NoCameraError),
                 (TangoLimaCtControl.IMAGESTATUS, Core.CtControl.ImageStatus())])
        self._status = Core.CtControl.Status()
        self._io_getter = lambda src: self._status

    def _on_start(self):
        for io_name in self._status_dict.keys():
            self._status_dict[io_name] = self.io_group().io_adapter(io_name).data(cache=True)
        self._status = Core.CtControl.Status(*self._status_dict.values())

    def _on_trigger(self, sender=None, data=None, **kwargs):
        # print('TRIGGGGG', sender, data)
        if (sender == TangoLimaCtControl.IMAGESTATUS and
            _cmp_img_status(data, self._status.ImageCounters)):
            # print('SAME COUNTERS')
            return

        if (sender == TangoLimaCtControl.STATUS_ACQUISITIONSTATUS and
            data == self._status.AcquisitionStatus):
            # print('SAME STATUS')
            return
            
        self._status_dict[sender] = data
        self._status = Core.CtControl.Status(*self._status_dict.values())
        return super()._on_trigger(sender=sender, data=data, **kwargs)


TangoCameraErrorCode = create_io_class(TangoLimaCtControl.STATUS_CAMERASTATUS,
            io_getter=lambda src: Core.CtControl.NoCameraError)


TangoReadBaseImage = create_tg_io_class(TangoLimaCtControl.READBASEIMAGE,
                                        hidden=True)


TangoReadLastImage = create_tg_io_class(TangoLimaCtControl.READIMAGE,
                                        hidden=True)