# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


if __name__ == '__main__':
    from .app import limagui_main
    limagui_main()