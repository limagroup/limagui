# -*- coding: utf-8 -*-

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"

import time
import numpy as np
from Lima import Core

from silx.gui.plot import Plot2D

from .profilewidget import ProfileWidget
from ..tango_io.plugins.bpmdeviceserver import BpmDeviceServerNames

from limagui.paramtree.qt.tree.widgets.qwidgetmixin import QCellWidgetMixin
from limagui.paramtree import Scheduler, Observer

from limagui.paramtree.qt import Qt

from ...lima.lima_io.limaionames import CtControl, CtImage, CtVideo, LimaTriggers

# from .bpmtoolbar import BpmDeviceServerToolbar


_FPS_ARRAY_LENGTH = 10

class LimaBpmWidget(Qt.QWidget):
    _IMG_LEGEND = '_LIMA_BPM_'

    def __init__(self, io_group, parent=None):
        super().__init__(parent=parent)
        self._status_io = io_group.io_adapter(LimaTriggers.ACQSTATUS_CHANGE)
        self._bvdata_io = io_group.io_adapter(BpmDeviceServerNames.BVDATA)
        # self._img_roi_io = model.get_io_instance(CtImage.ROI)
        # self._vid_roi_io = model.get_io_instance(CtVideo.ROI)

        self._io_group = io_group

        self._start_time = None

        self._poll_ival = 50
        self._poll = False

        self._plotVisible = True

        self._last_status = None
        self._last_counter = None

        self._rst_zoom = True

        self._poller = Scheduler(self._pollEvent,
                                 1000,
                                 single_shot=True)

        mainLayout = Qt.QGridLayout(self)
        mainLayout.setContentsMargins(0, 0, 0, 0)

        ############################
        # Plot
        ############################

        pw = self._pw = ProfileWidget()
        grp_name = io_group.name()
        pw.setWindowTitle(grp_name)
        self.setStatusTip(grp_name)
        self.setToolTip(grp_name)
        #pw.getYAxis().setInverted(True)
        pw.setStatusTip(grp_name)
        pw.setToolTip(grp_name)

        mainLayout.addWidget(pw, 1, 0)

        ############################
        # Bottom widgets (FPS, ...)
        ############################
        bottomWidget = Qt.QGroupBox()
        # bottomWidget.setContentsMargins(0, 0, 0, 0)
        bottomLayout = Qt.QHBoxLayout(bottomWidget)
        bottomLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.addWidget(bottomWidget, 2, 0)

        bottomLayout.addWidget(Qt.QLabel('FPS:'))
        self._fpsLabel = Qt.QLabel()
        bottomLayout.addWidget(self._fpsLabel)
        bottomLayout.addStretch(1)
        self._fps_array = [None] * _FPS_ARRAY_LENGTH
        self._fps_array_idx = 0
        self._last_fps_disp_t = 0

        bottomLayout.addStretch(100)

        ############################

        self._counter = -1
        self._status = None

        self._limaimgview_ready = True

        self._observer = Observer(parent=self,
                                    uid=f'IMG_WIN_{grp_name}',
                                    callback=self._on_trigger)

    def hideEvent(self, *args, **kwargs):
        rc = super().hideEvent(*args, **kwargs)
        self._setVisibility(False)
        return rc

    def showEvent(self, *args, **kwargs):
        rc = super().showEvent(*args, **kwargs)
        self._setVisibility(True)
        return rc

    def _on_trigger(self, sender, data, **kwargs):
        if sender == LimaTriggers.ACQSTATUS_CHANGE:
            if data[0] == Core.AcqRunning:
                self._startPolling()

    def _startPolling(self):
        self._poll = self._plotVisible
        self._nextCounter()

    def _stopPolling(self):
        self._poll = False

    def _nextCounter(self):
        if self._poll and self._plotVisible:
            self._poller.start(self._poll_ival, single_shot=True)

    def _setVisibility(self, visible):
        """
        Allows to start stop the polling when the widget is hidden.
        """
        self._plotVisible = visible
        if visible:
            ios = [LimaTriggers.ACQSTATUS_CHANGE]
            for io in ios:
                io_adapt = self._io_group.io_adapter(io, hidden=True)
                if not io_adapt:
                    raise RuntimeError(f'Io {io.name} not found.')
                io_adapt.register_observer(self._observer)
            self._startPolling()
        else:
            self._observer.clear()
            self._stopPolling()

    def _pollEvent(self):
        data = self._bvdata_io.data()
        if data is None:
            status = self._io_group.io_adapter(CtControl.STATUS).data(cache=True)
            if status.AcquisitionStatus == Core.AcqRunning:
                self._nextCounter()
            return
        self._processBvData(data)

    def _processBvData(self, data):
        if data is None:
            self._nextCounter()
            return

        counter = data.counter
        start_time = data.start_time
        status = data.status

        if status is None:
            self._nextCounter()
            return

        if counter >= 0:
            if status == Core.AcqReady:
                if counter != self._last_counter or self._start_time != start_time:
                    self._setBvata(data)
                self._stopPolling()
            elif status == Core.AcqRunning:
                if counter != self._last_counter or self._start_time != start_time:
                    self._setBvata(data)
                self._startPolling()
            else:
                print(f'In LimaBpmWidget, unhandled status {status}.')
        else:
            if status == Core.AcqRunning:
                self._nextCounter()
        self._last_status = status
        self._last_img_rdy = counter

    def _setBvata(self, data):
        try:
                # counter: int = -1
    # image: Any = None
    # start_time: float = 0
    # x: float = np.nan
    # y: float = np.nan
    # projx: Any = None
    # projy: Any = None
            if data is not None:
                counter = data.counter
                start_t = data.start_time
                image = data.image
                status = data.status
                proj_x = data.proj_x
                proj_y = data.proj_y
            else:
                counter, start_t, image, status, proj_x, proj_y = [None] * 6

            disp = False

            if data is None or image is None:
                self._clearPlot()
                self._start_time = None
                self._counter = None

                return

            if start_t != self._start_time or self._start_time is None:
                self._fps_array = [None] * _FPS_ARRAY_LENGTH
                self._fps_array_idx = 0
                self._start_time = start_t

            if counter < 0:
                pass
            elif counter == self._counter:
                if start_t != self._start_time and self._start_time is not None:
                    disp = True
                    self._start_time = start_t
            else:
                disp = True
                    
            # self._start_time = start_t
            # self._counter = last_img_rdy

            if status == Core.AcqRunning:
                self._nextCounter()

            pw = self._pw

            t0 = time.time()

            if disp:
                self._status = status
                self._counter = counter
                title = f'Frame {counter}'

                # roi = self._img_roi_io.data(cache=True)
                # topLeft = roi.getTopLeft()
                # orig = topLeft.x, topLeft.y

                pw.setProfileData(image,
                                  proj_x,
                                  proj_y,
                                  resetzoom=self._rst_zoom)
                
                # pw.addImage(image,
                #             legend=self._IMG_LEGEND,
                #             resetzoom=self._rst_zoom,
                #             # origin=orig
                #             )            
                pw.setGraphTitle(title)
                self._rst_zoom = False

                self._fps_array[self._fps_array_idx] = (t0, counter)
                last_fps_data = self._fps_array[self._fps_array_idx - _FPS_ARRAY_LENGTH + 1]

                if t0 - self._last_fps_disp_t >= 1:
                    if last_fps_data is None:
                        last_fps_data = self._fps_array[0]
                        n_img = self._fps_array_idx
                    else:
                        n_img = _FPS_ARRAY_LENGTH - 1
                    t_diff = t0 - last_fps_data[0]
                    if t_diff == 0:
                        fps = np.nan
                    else:
                        fps = n_img / t_diff#(number - last_fps_data[1]) / t_diff
                    self._fpsLabel.setText(f'{fps:3.2f}')
                    self._last_fps_disp_t = t0
                self._fps_array_idx += 1
                self._fps_array_idx %= _FPS_ARRAY_LENGTH
            
            # t_disp = (time() - t0) * 1000
            # self._poll_ival = max(50, t_disp + 20)
        except Exception as ex:
            print('BOOM', ex)

    def _clearPlot(self):   
        pw = self._pw
        pw.remove(self._IMG_LEGEND)
        pw.setGraphTitle('No frame')

    #     self._model = model
    #     self._poll_ival = 50
    #     self._poll = False
    #     self._start_time = None

    #     super().__init__(img_callback_io, parent=parent)

    #     self.setWindowTitle('BPM')

    #     self._rst_zoom = True
    #     self._poller = QScheduler(self._getBvData,
    #                               1,
    #                               single_shot=True)

    #     topLayout = Qt.QGridLayout(self)
    #     topLayout.setContentsMargins(0, 0, 0, 0)

    #     pw = self._pw = ProfileWidget()
    #     pw.setWindowTitle(model.name)
    #     self.setStatusTip(model.name)
    #     self.setToolTip(model.name)
    #     #pw.getYAxis().setInverted(True)
    #     pw.setStatusTip(model.name)
    #     pw.setToolTip(model.name)

    #     toolbar = BpmDeviceServerToolbar(model)
    #     toolbar.setFloatable(False)
    #     pw.addToolBar(Qt.Qt.LeftToolBarArea, toolbar)

    #     status_io = model.get_io_instance(CtControl.STATUS_ACQUISITIONSTATUS)
    #     self._status_io = status_io
    #     # bpm_state_io = model.get_io_instance(BpmDeviceServerNames.STATE)
    #     # self._bpm_state_io = bpm_state_io
    #     # # we have to do that
    #     # self._io_observer = QObserver(callback=self._on_io_event)
    #     # status_io._register_notify(self._io_observer)
    #     # bpm_state_io._register_notify(self._io_observer)

    #     # viewAct = dock.toggleViewAction()
    #     # pw.addTabbedDockWidget(dock)

    #     topLayout.addWidget(pw)

    #     self._counter = -1
    #     self._status = None

    #     self._limaimgview_ready = True

    # def _cell_set_editable(self, editable):
    #     pass

    # def _cell_set_enabled(self, enabled):
    #     pass

    # def setVisibility(self, visible):
    #     self._visible = visible
    #     if self._visible:
    #         self._observe_cell()
    #         self._startPolling()
    #     else:
    #         self._unobserve_cell()
    #         self._stopPolling()

    # def _cell_set_data(self, bvdata):
    #     self._poll = True
    #     self._setBvData(bvdata)
    
    # def _startPolling(self):
    #     if not self._poll:
    #         self._poll = True
    #         self._nextCounter()

    # def _stopPolling(self):
    #     self._poll = False

    # def _nextCounter(self):
    #     if self._poll:
    #         self._poller.start(self._poll_ival)

    # def _getBvData(self):
    #     data = self._cell().io_adapter().io_base._get_data()
    #     self._setBvData(data)

    # def _setBvData(self, data):
    #     timestamp = None
    #     counter = None
    #     image = None
    #     x = None
    #     y = None
    #     intensity = None
    #     projx = None
    #     projy = None

    #     if data:
    #         timestamp, counter, x, y, intensity, image, projx, projy = data

    #     status = self._status_io.data(cache=True)

    #     if data is None or image is None or counter is None:
    #         self._clearPlot()
    #         return

    #     if status == Core.AcqRunning:
    #         self._nextCounter()
    #     else:
    #         self._poll = False

    #     self._pw.setProfileData(image,
    #                         projx,
    #                         projy,
    #                         resetzoom=self._rst_zoom)
    #     title = ('#{0} | pos={1:.2f}, {2:.2f} | i={3}'
    #                 ''.format(counter, x, y, intensity))
    #     self._pw.setGraphTitle(title)

    #     #     number = image.number()
    #     #     if number is None:
    #     #         number = counters.LastImageReady
    #     #     self._counter = number
    #     #     title = f'Frame {number}'

    #     #     roi = self._img_roi_io.data(cache=True)
    #     #     topLeft = roi.getTopLeft()
    #     #     orig = topLeft.x, topLeft.y
            
    #     #     image_data = image.image()
    #     #     pw.addImage(image_data,
    #     #                 legend=self._IMG_LEGEND,
    #     #                 resetzoom=self._rst_zoom,
    #     #                 origin=orig)            
    #     #     pw.setGraphTitle(title)
    #     #     self._rst_zoom = False
        
    #     # t_disp = (time() - t0) * 1000
    #     # self._poll_ival = max(50, t_disp + 20)

    # def _clearPlot(self):   
    #     pw = self._pw
    #     pw.remove(self._IMG_LEGEND)
    #     pw.setGraphTitle('No frame')


# class BpmWindow(ProfileWidget):
#     _IMG_LEGEND = '_LIMA_BPM_'

#     def __init__(self, model, **kwargs):
#         super(BpmWindow, self).__init__(**kwargs)

#         self.setWindowTitle('BPM')

#         self.getYAxis().setInverted(True)


    #     self.__model = weakref.ref(model)

    #     self.__visible = False
    #     self.__polling = False
    #     self.__stopLater = False

    #     self.__lastResultTime = None
    #     self.__pollingTimeMs = None
    #     self.__lastCounter = None

    #     self.__poller = QScheduler(self.__getNextResult,
    #                                1,
    #                                single_shot=True)

    #     self.__bpmstate_obs = QObserver(callback=self.__bpmActiveChanged)
    #     self.__acqstate_obs = QObserver(callback=self.__acqStatusChanged)
    #     # self.__imgStatus_obs = QObserver(callback=self.__imgStatusChanged)
    #     self.__bvdata_obs = QObserver(callback=self.__bvDataEvent)

    #     bpmIo = model.get_io_instance(BpmDeviceServer.STATE)
    #     acqStatusIo = model.get_io_instance(
    #          CtControl.STATUS_ACQUISITIONSTATUS)

    #     self.__bvdataIo = model.get_io_instance(BpmDeviceServer.BVDATA)
    #     self.__imgStatusIo = model.get_io_instance(CtControl.IMAGESTATUS)

    #     self.__bpmstate_obs.register(bpmIo.active_chan)
    #     acqStatusIo.register_view(self.__acqstate_obs)
    #     # self.__imgStatus_obs.register(self.__imgStatusIo.data_chan)
    #     self.__bvdataIo.register_view(self.__bvdata_obs)

    #     self.__acqrunning = acqStatusIo.active
    #     self.__bpmrunning = bpmIo.active

    #     statusBar = self.statusBar()
    #     statusBar.setSizeGripEnabled(False)
    #     self.__dateLabel = Qt.QLabel()
    #     statusBar.addWidget(self.__dateLabel)

    #     self.sigVisibilityChanged.connect(self.__visibilityChanged)

    # def model(self):
    #     return self.__model()

    # def __bpmActiveChanged(self, event):
    #     self.__bpmrunning = event.data()
    #     title = 'Bpm ON' if self.__bpmrunning else 'Bpm Off'
    #     self.setWindowTitle(title)
    #     self.__updateState()

    # def __visibilityChanged(self, visible):
    #     self.__visible = visible
    #     self.__updateState()

    # def __acqStatusChanged(self, event):
    #     self.__acqrunning = event.data() == Core.AcqRunning
    #     self.__updateState()

    # def __updateState(self):
    #     polling = self.__visible and self.__acqrunning and self.__bpmrunning

    #     if polling == self.__polling:
    #         return

    #     self.__polling = polling
    #     if polling:
    #         self.__startPolling()
    #     else:
    #         later = self.__visible and not self.__acqrunning
    #         self.__stopPolling(later=later)

    # def isPolling(self):
    #     return self.__polling

    # def __startPolling(self):
    #     self.remove(self._IMG_LEGEND)
    #     self.__stopLater = False

    #     if not self.__polling:
    #         return

    #     model = self.model()
    #     if model is None:
    #         print('No model, can\'t start acquisition.')
    #         return

    #     latencyIo = model.get_io_instance(CtAcquisition.LATENCYTIME)
    #     expoTimeIo = model.get_io_instance(CtAcquisition.ACQEXPOTIME)
    #     self.__pollingTimeMs = 1000 * (latencyIo.data() + expoTimeIo.data())

    #     self.__first = True
    #     self.__lastImageTime = None
    #     self.__lastCounter = None
    #     self.__getNextResult()

    # def __stopPolling(self, later=False):
    #     if not later:
    #         self.__stopLater = None
    #         self.__poller.stop()
    #     else:
    #         self.__stopLater = True

    # def __getNextResult(self):
    #     if self.__stopLater is None:
    #         return

    #     delay_ms = 0

    #     thisTime = time() * 1000
    #     counter = self.__imgStatusIo.data().LastImageReady

    #     if counter < 0:
    #         delay_ms = 200
    #     elif self.__lastResultTime is not None:
    #         delay_ms = self.__pollingTimeMs - (thisTime - self.__lastResultTime)

    #     if delay_ms <= 0:
    #         if counter != self.__lastCounter:
    #             self.__bvdataIo.data(wait=False)
    #             return
    #         if self.__stopLater:
    #             self.__stopLater = None
    #             return
    #         delay_ms = 100
    #     self.__poller.start(delay_ms=10 + delay_ms)

    # def __bvDataEvent(self, event):
    #     thisTime = time()
    #     data = event.data()
    #     data_struct = struct.unpack(data[0], data[1])

    #     counter = data_struct[1]
    #     x = data_struct[2]
    #     y = data_struct[3]
    #     intensity = data_struct[4]

    #     same = counter == self.__lastCounter

    #     self.__lastCounter = counter

    #     self.__getNextResult()

    #     if not same:
    #         self.__lastResultTime = thisTime * 1000
    #         self.__lastCounter = counter

    #         jpeg = base64.b64decode(data_struct[14])
    #         image = cv2.imdecode(np.frombuffer(jpeg, dtype=np.uint8),
    #                              cv2.IMREAD_COLOR)

    #         try:
    #             projx = np.frombuffer(data_struct[12], dtype=np.int64)
    #         except Exception as ex:
    #             print('=== projX: ', ex)
    #             projx = None

    #         try:
    #             projy = np.frombuffer(data_struct[13], dtype=np.int64)
    #         except Exception as ex:
    #             print('=== projY: ', ex)
    #             projy = None

    #         self.setProfileData(image,
    #                             projx,
    #                             projy,
    #                             resetzoom=self.__first)
    #         title = ('#{0} | pos={1:.2f}, {2:.2f} | i={3}'
    #                  ''.format(self.__lastCounter, x, y, intensity))
    #         self.setGraphTitle(title)
    #         timestr = asctime(localtime(thisTime))
    #         self.__dateLabel.setText('Last result: {0}'.format(timestr))

    #         self.__first = False
