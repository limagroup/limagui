# coding: utf-8

from __future__ import absolute_import, print_function, division

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"

import os as _os
import logging as _logging

# Attach a do nothing logging handler for silx
_logging.getLogger(__name__).addHandler(_logging.NullHandler())

project = _os.path.basename(_os.path.dirname(_os.path.abspath(__file__)))

# try:
#     from ._version import __date__ as date  # noqa
#     from ._version import version, version_info, hexversion, \
#         strictversion  # noqa
# except ImportError:
#     raise RuntimeError("Do NOT use %s from its sources: build it and "
#                        "use the built version" % project)

from . import resources
from .utils.subject import Subject, SubjectBase
from .utils.observer import Observer, ObserverBase
from .utils.runner import Runner, RunnerBase
from .utils.scheduler import Scheduler, SchedulerBase
from .utils.utils import get_module
from .tree.node import Node
from .tree.cell import Cell
from .tree.types import Action, Actions, Progress
from .tree.ioadapter import (IoAdapter, IoAdapterBase,
                                        ioadapterdef,
                                        create_io_class,
                                        get_io_adapter_class,
                                        registered_io_names,
                                        io_name_is_registered)
from .tree.iogroup import IoGroup
from .tree.ionamesenum import IoNamesEnum, ionames_enum_def
from .tree.treeadapters.treeadapter import (TreeAdapterBase,
                                             treeadapterdef,
                                             TreeAdapter)
from .tree.treeadapters.dicttreeadapter import DictTreeAdapter
from .tree.treeadapters.sublisttreeadapter import SubListTreeAdapter
from .tree.treeadapters.vectortreeadapter import VectorBase, VectorTreeAdapter
from .tree.viewadapters.viewadapter import ViewAdapterBase, viewadapterdef
