import enum
from functools import partial
from collections import OrderedDict

from limagui.paramtree import Node, Action, Actions
from limagui.paramtree.qt import QModel, TreeView
from limagui.paramtree.tree.types import Progress
from limagui.paramtree.qt import Qt
from limagui.paramtree.qt.tree.widgets import QCellWidget
from limagui.paramtree.qt.tree.widgets import widgets as cellwids

app = Qt.QApplication([])

class myEnum(enum.Enum):
    AA = enum.auto()
    BB = enum.auto()
    CC = enum.auto()

myEnumColors = {
    myEnum.AA: 'green',
    myEnum.BB: '#FF0000',
    myEnum.CC: 'light blue',
}

data = OrderedDict(
       {'enum': myEnum.AA})

listKeys = list(data.keys())
enumIdx = listKeys.index('enum')

node = Node(name='Foo', data=data)

model = QModel()
model.startModel()
idx = model.insertNode(node)

enumIndex = model.index(enumIdx, 1, idx)

enumNode = node.child(enumIdx)

win = Qt.QMainWindow()

treeA = TreeView()
treeA.setModel(model)

treeB = TreeView()
treeB.setModel(model)

wid = Qt.QWidget()
layout =Qt.QHBoxLayout(wid)
layout.addWidget(treeA)
# layout.addWidget(treeB)

index = model.index(0, 0)
treeA.expand(index)
# treeB.expand(index)

# panel = Qt.QWidget()
# form = Qt.QFormLayout(panel)

# for row, (key, value) in enumerate(data.items()):
#     index = model.index(row, 1, idx)
#     form.addRow(key, QCellWidget(index))

# diode = cellwids.QCellDiodeWidget(enumNode.cell(1))
# diode.setColorDict(myEnumColors)
# form.addRow('MyEnum', diode)

# layout.addWidget(panel)

# tBar = win.addToolBar('ActionToolBar')
# for row, (key, value) in enumerate(data.items()):
#     index = model.index(row, 1, idx)
#     tBar.addAction(cellwids.QCellWidgetAction(index, tBar))
#     tBar.addSeparator()

win.setCentralWidget(wid)
win.show()

app.exec_()