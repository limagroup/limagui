# coding: utf-8

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from silx.gui import qt as Qt
from silx.gui.icons import getQIcon as silxGetQIcon

def getQIcon(icon):
    if isinstance(icon, (Qt.QIcon,)):
        return icon
    if isinstance(icon, (Qt.QStyle.StandardPixmap,)):
        return Qt.QApplication.style().standardIcon(icon)
    return silxGetQIcon(icon)


from .qrunner import QRunner
from .qsubject import QSubject
from .qobserver import QObserver
from .qscheduler import QScheduler
from .tree import TreeView, QModel, ParamTreeView
from .tree.widgets.widgets import *
