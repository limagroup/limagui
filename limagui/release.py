# -*- coding: utf-8 -*-

name = "limagui"
author = "ROBL"
author_email = ""
license = ""
copyright = "2018-2019 ROBL"
description = "ROBL"
url = "n/a"

_version_major = 0
_version_minor = 1
_version_patch = 0
_version_extra = ".dev0"
# _version_extra = ''  # uncomment this for full releases

# Construct full version string from these.
_ver = [_version_major, _version_minor, _version_patch]

version = ".".join(map(str, _ver))
if _version_extra:
    version += _version_extra

# used by the doc
short_version = ".".join(map(str, _ver[:2]))

version_info = _version_major, _version_minor, _version_patch, _version_extra
