# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import sys
import inspect

from setuptools.extension import Extension
from setuptools import setup, find_packages

TESTING = any(x in sys.argv for x in ["test", "pytest"])

conda_base = os.environ.get("CONDA_PREFIX")
extensions = []
sip_extensions = []

try:
    from Cython.Build import cythonize
except ImportError:
    cython = False
    cythonize = lambda ext: [ext]
else:
    cython = True


def abspath(*path):
    """A method to determine absolute path for a given relative path to the
    directory where this setup.py script is located"""
    setup_dir = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(setup_dir, *path)


def main():
    """run setup"""

    # py = sys.version_info
    # py_str = ".".join(map(str, py))
    #
    # if py < (3,):
    #     print(("Incompatible python version ({0}). Needs python 3.x ".format(py_str)))
    #     sys.exit(1)

    meta = {}
    exec(
        compile(
            open(abspath("limagui", "release.py")).read(),
            abspath("limagui", "release.py"),
            "exec",
        ),
        meta,
    )

    packages = find_packages(where=abspath(), exclude=("extensions*",))

    install_requires = [
      #  "numpy",
      #  'silx'#,
#        "py-opencv"
    ]

    tests_require = ["pytest >= 4.1.1", "pytest-cov >= 2.6.1", "scipy"]

    setup_requires = [
        #        'setuptools >= 37',
    ]

    if TESTING:
        setup_requires += ["pytest-runner"]

    package_data = {
        'limagui.resources': [
            'icons/*.png',
            'icons/*.svg',
            'icons/*.mng',
            'icons/*.gif',
            'icons/*/*.png'],
        '': ['*.png', '*.svg', '*.mng', '*.gif']
    }



    console_scripts_entry_points = [
        "limagui = limagui.applications.limagui.app:limagui_main",
    ]

    setup(
        name=meta["name"],
        author=meta["author"],
        version=meta["version"],
        description=meta["description"],
        license=meta["license"],
        url=meta["url"],
        package_dir={"limagui": "limagui"},
        packages=packages,
        package_data=package_data,
        ext_modules=extensions,
        scripts=[],
        entry_points={"console_scripts": console_scripts_entry_points},
        install_requires=install_requires,
        tests_require=tests_require,
        setup_requires=setup_requires,
    )


if __name__ == "__main__":
    main()
