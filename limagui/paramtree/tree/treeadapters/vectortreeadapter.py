# -*- coding: utf-8 -*-

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"


from .treeadapter import treeadapterdef
from .sublisttreeadapter import SubListTreeAdapter


class VectorBase:
    names = ()
    values = ()


@treeadapterdef(VectorBase)
class VectorTreeAdapter(SubListTreeAdapter):
    def __init__(self, node, data, data_type=None, **kwargs):
        kwargs['sub_data'] = data_type.values
        kwargs['sub_names'] = data_type.names
        kwargs['editable'] = False
        super(VectorTreeAdapter, self).__init__(node, data,
                                                data_type=None, **kwargs)

    # def _sub_to_root(self, root_data, row, cell_data):

    # def _sub_to_root(self, root_data, row, cell_data):
    #     bin = None
    #     if row == 0:
    #         bin = Bin(cell_data, root_data.getY())
    #     elif row == 1:
    #         bin = Bin(root_data.getX(), cell_data)
    #     return bin

    # def _root_to_sub(self, root_data):
    #     return [root_data.getX(), root_data.getY()]