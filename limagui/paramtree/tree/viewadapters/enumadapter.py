import enum
from .viewadapter import ViewAdapterBase, viewadapterdef


@viewadapterdef(enum.Enum)
@viewadapterdef(enum.EnumMeta)
class EnumAdapter(ViewAdapterBase):
    def __init__(self, cell, **kwargs):
        super(EnumAdapter, self).__init__(cell, **kwargs)
        self._enum = self.adapter_type()#data.__class__

    def model_to_view_data(self, data):
        if isinstance(data, self._enum):
            return data
        if data is not None:
            return self._enum(data)
        return None

    def view_to_model_data(self, data):
        if isinstance(data, self._enum):
            return data
        return self._enum[data]

    def to_text(self, model_data):
        if model_data is None:
            return None
        if isinstance(model_data, self._enum):
            return model_data.name
        return self._enum(model_data).name
