from importlib import import_module

from Lima import Core

from .lima_io.limaionames import (
        CtControl,
        CtAcquisition,
        CtAccumulation,
        CtImage,
        CtVideo,
        LimaTriggers,
        HwInterface,
        LimaImages)


def lima_io_tree(io_group):
    """
    Returns a "tree" dictionary of lima IoAdapters.
    Can be added to a paramtree.
    """
    tree = {}

    ios = [io_group.io_adapter(io_name, hidden=False) for io_name in CtControl]
    tree['CtControl'] = {io.io_name.name:io for io in ios if io}
    ios = [io_group.io_adapter(io_name, hidden=False) for io_name in CtAcquisition]
    tree['CtAcquisition'] = {io.io_name.name:io for io in ios if io}
    ios = [io_group.io_adapter(io_name, hidden=False) for io_name in CtAccumulation]
    tree['CtAccumulation'] =  {io.io_name.name:io for io in ios if io}
    ios = [io_group.io_adapter(io_name, hidden=False) for io_name in CtImage]
    tree['CtImage'] =  {io.io_name.name:io for io in ios if io}
    ios = [io_group.io_adapter(io_name, hidden=False) for io_name in CtVideo]
    tree['CtVideo'] =  {io.io_name.name:io for io in ios if io}

    return tree


def add_lima_sources(io_group, lima_cam):
    for name, obj in lima_cam.items():
        io_group.add_source(name, obj)


def new_lima_camera(cam_type,
                    *args,
                    **kwargs):
    """
    Initialize a Lima camera (direct access, i.e: not tango)
    Returns a dictionary with all objects ("sources") used by
        limagui ioadapters.
    """

    lima_cam = {}

    camera = None
    print('Importing Lima.{0}'.format(cam_type))
    cam_module = import_module('.{0}'.format(cam_type), 'Lima')
    if hasattr(cam_module, 'Camera'):
        if len(args) >= 1:
            camera = cam_module.Camera(*args)
        else:
            camera = cam_module.Camera()
        interface = cam_module.Interface(camera)
    else:
        if len(args) >= 1:
            interface = cam_module.Interface(*args)
        else:
            interface = cam_module.Interface()

    ctcontrol = Core.CtControl(interface)

    lima_cam['CtControl'] = ctcontrol
    lima_cam['CtVideo'] = ctcontrol.video()
    lima_cam['CtImage'] = ctcontrol.image()
    lima_cam['CtAcquisition'] = ctcontrol.acquisition()
    lima_cam['CtAccumulation'] = ctcontrol.accumulation()
    lima_cam['CtSaving'] = ctcontrol.saving()
    lima_cam['HwInterface'] = ctcontrol.hwInterface()
    lima_cam['Camera'] = camera

    return lima_cam
