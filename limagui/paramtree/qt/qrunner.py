# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"

from . import Qt

from weakref import proxy
from ..utils.runner import RunnerBase, add_runner


class _Caller(Qt.QObject):
    signal = Qt.Signal(object)

    def __init__(self, runner=None, **kwargs):
        super(_Caller, self).__init__(**kwargs)
        self.signal.connect(self._slot)

        if runner:
            self.__runner = proxy(runner)

    def call(self, data):
        self.signal.emit(data)

    def _slot(self, data=None):
        try:
            callback = self.__runner.callback
        except ReferenceError:
            return

        if callback:
            callback(data)


class QRunner(RunnerBase):

    def __init__(self, *args, **kwargs):
        super(QRunner, self).__init__(*args, **kwargs)
        self.__caller = _Caller(self)

    def _callback(self, data=None):
        self.__caller.call(data)


add_runner('Qt', QRunner)
