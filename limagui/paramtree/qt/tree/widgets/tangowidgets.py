from tango import DevState, DeviceProxy, AttributeProxy

from limagui.paramtree.qt import Qt
from limagui.paramtree.tree.ioadapter import IoAdapterBase
from limagui.paramtree.tree.extra import TangoIoAdapter
from . import QCellDiodeWidget, QCellLabelWidget


TangoStateColors = {
    DevState.ON: 'blue',
    DevState.RUNNING: 'green',
    DevState.OFF: 'orange',
    DevState.FAULT: 'red',
    DevState.STANDBY: 'pink',
    DevState.UNKNOWN: 'black'
    # CLOSE, OPEN, INSERT, EXTRACT, MOVING, STANDBY, INIT, ALARM, DISABLE, UNKNOWN.
}


def _get_state_io(data):
    if not isinstance(data, (IoAdapterBase)):
        if isinstance(data, (DeviceProxy,)):
            source = data
        else:
            source = DeviceProxy(data)
        
        data = TangoIoAdapter(tango_name='State',
                              source=source,
                              tango_read_devfailed=DevState.UNKNOWN,
                              poll_ms=2000)
        data.start()

    return data


class TangoStatusDiode(QCellDiodeWidget):
    def __init__(self, data=None, **kwargs):
        self._tg_state_io = _get_state_io(data)
        super().__init__(data=self._tg_state_io, **kwargs)
        self.setColorDict(TangoStateColors)


class TangoStatusLabel(QCellLabelWidget):
    def __init__(self, data=None, **kwargs):
        self._tg_state_io = _get_state_io(data)
        super().__init__(data=self._tg_state_io, **kwargs)

    
class TangoStatusWidget(Qt.QWidget):
    def __init__(self, data=None, **kwargs):
        super().__init__(**kwargs)

        layout = Qt.QHBoxLayout(self)

        diode = TangoStatusDiode(data=data)
        label = TangoStatusLabel(data=data)
        srvLabel = Qt.QLabel(diode.cell.io_adapter().source().name())
        srvLabel.setFrameStyle(Qt.QFrame.Panel | Qt.QFrame.Sunken)

        layout.addWidget(srvLabel)
        layout.addWidget(diode)
        layout.addWidget(label)
        layout.addStretch(1)