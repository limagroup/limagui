# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from ..editor import QEditorMixin, qeditordef
from ..widgets.widgets import QCellDoubleSpinBox


@qeditordef(data_type=float)
class DoubleSpinBox(QEditorMixin, QCellDoubleSpinBox):
    def __init__(self, *args, **kwargs):
        super(DoubleSpinBox, self).__init__(*args, **kwargs)
        self.editingFinished.disconnect()