# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"

from Lima import Core
from limagui.paramtree import IoAdapterBase, create_io_class, ioadapterdef
from .limaionames import CtAcquisition, LimaTriggers, HwInterface
from ..type import enums


LimaAcqLatencyTime = create_io_class(CtAcquisition.LATENCYTIME,
            io_getter=lambda ctacq: ctacq.getLatencyTime(),
            io_setter=lambda ctacq, data: ctacq.setLatencyTime(data),
            data_type=float,
            units='s',
            triggers=LimaTriggers.REFRESH_TRIGGER)

# class LimaAcqLatencyTime(IOBase):
#     pass
#     # def view_config(self):
#     #     view_config = super(LimaAcqLatencyTime, self).view_config()
#     #     ranges = self.model().get_io_instance(
#     #         HwInterface.CAP_SYNC_VALIDRANGES).data()
#     #     view_config['max_value'] = ranges.max_lat_time
#     #     view_config['min_value'] = ranges.min_lat_time
#     #     return view_config


LimaAcqAcqNbFrames = create_io_class(CtAcquisition.ACQNBFRAMES,
            io_getter=lambda ctacq: ctacq.getAcqNbFrames(),
            io_setter=lambda ctacq, data: ctacq.setAcqNbFrames(data),
            data_type=int,
            triggers=LimaTriggers.REFRESH_TRIGGER)


LimaAcqConcatNbFrames = create_io_class(CtAcquisition.CONCATNBFRAMES,
            io_getter=lambda ctacq: ctacq.getConcatNbFrames(),
            io_setter=lambda ctacq, data: ctacq.setConcatNbFrames(data),
            data_type=int,
            triggers=LimaTriggers.REFRESH_TRIGGER)


LimaAcqAccExpoTime = create_io_class(CtAcquisition.ACCEXPOTIME,
            io_getter=lambda ctacq: ctacq.getAccExpoTime(),
            data_type=float,
            triggers=LimaTriggers.REFRESH_TRIGGER,
            enable_conditions={CtAcquisition.ACQMODE: lambda data: data == Core.Accumulation},
            units='s',
            description='Accumulation exposition time (s)')


LimaAcqAccNbFrames = create_io_class(CtAcquisition.ACCNBFRAMES,
            io_getter=lambda ctacq: ctacq.getAccNbFrames(),
            data_type=int,
            triggers=LimaTriggers.REFRESH_TRIGGER,
            enable_conditions={CtAcquisition.ACQMODE: lambda data: data == Core.Accumulation})


LimaAcqAccMaxExpoTime = create_io_class(CtAcquisition.ACCMAXEXPOTIME,
            io_getter=lambda ctacq: ctacq.getAccMaxExpoTime(),
            io_setter=lambda ctacq, data: ctacq.setAccMaxExpoTime(data),
            data_type=float,
            units='s',
            triggers=LimaTriggers.REFRESH_TRIGGER,
            enable_conditions={CtAcquisition.ACQMODE: lambda data: data == Core.Accumulation})


LimaAcqAccTimeMode = create_io_class(CtAcquisition.ACCTIMEMODE,
            io_getter=lambda ctacq: ctacq.getAccTimeMode(),
            io_setter=lambda ctacq, data: ctacq.setAccTimeMode(data),
            data_type=enums.AccTimeMode,
            triggers=LimaTriggers.REFRESH_TRIGGER,
            enable_conditions={CtAcquisition.ACQMODE: lambda data: data == Core.Accumulation})


LimaAcqAccDeadTime = create_io_class(CtAcquisition.ACCDEADTIME,
            io_getter=lambda ctacq: ctacq.getAccDeadTime(),
            data_type=float,
            units='s',
            triggers=LimaTriggers.REFRESH_TRIGGER,
            enable_conditions={CtAcquisition.ACQMODE: lambda data: data == Core.Accumulation})


LimaAcqAccLiveTime = create_io_class(CtAcquisition.ACCLIVETIME,
            io_getter=lambda ctacq: ctacq.getAccLiveTime(),
            data_type=float,
            units='s',
            triggers=LimaTriggers.REFRESH_TRIGGER,
            enable_conditions={CtAcquisition.ACQMODE: lambda data: data == Core.Accumulation})


LimaAcqExpoTime = create_io_class(CtAcquisition.ACQEXPOTIME,
            io_getter=lambda ctacq: ctacq.getAcqExpoTime(),
            io_setter=lambda ctacq, data: ctacq.setAcqExpoTime(data),
            data_type=float,
            units='s',
            triggers=LimaTriggers.REFRESH_TRIGGER,
            description='Acquisition exposition time (s)')
# class LimaAcqAcqExpoTime(IOBase):
#     pass
#     # def view_config(self):
#     #     view_config = super(LimaAcqAcqExpoTime, self).view_config()
#     #     ranges = self.model().get_io_instance(
#     #         HwInterface.CAP_SYNC_VALIDRANGES).data()
#     #     view_config['max_value'] = ranges.max_exp_time
#     #     view_config['min_value'] = ranges.min_exp_time
#     #     return view_config


LimaAcqAcqMode = create_io_class(CtAcquisition.ACQMODE,
                        io_getter=lambda ctacq: ctacq.getAcqMode(),
                        io_setter=lambda ctacq, data: ctacq.setAcqMode(data),
                        data_type=enums.AcqMode,
                        triggers=LimaTriggers.REFRESH_TRIGGER)


LimaAcqTrigMode = create_io_class(CtAcquisition.TRIGGERMODE,
                        io_getter=lambda ctacq: ctacq.getTriggerMode(),
                        io_setter=lambda ctacq, data: ctacq.setTriggerMode(data),
                        data_type=enums.TrigMode,
                        triggers=LimaTriggers.REFRESH_TRIGGER)


# @ioclassdef(LimaCtAcquisition.PARS,
#             io_getter=lambda ctacq: ctacq.getPars(),
#             io_setter=lambda ctacq, data: ctacq.setTriggerMode(data),
#             data_type=Core.CtAcquisition.Parameters,
#             listen_to=[LimaCtControl.PREPAREACQ],
#             view_options={'editable': False})
# class LimaAcqPars(IOBase):
#     pass
