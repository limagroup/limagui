# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"

from Lima import Core

from limagui.paramtree import IoAdapterBase, create_io_class, ioadapterdef
from ..type import enums

from ..type.limaimage import LimaVideoImage, ctvideo_image_to_video_image

from .limaionames import CtVideo, CtControl, LimaTriggers

import sys
if sys.version_info > (3,):
    long = int


LimaVideoActive = create_io_class(CtVideo.ACTIVE,
                    io_setter=lambda ctvideo, data: ctvideo.setActive(data),
                    io_getter=lambda ctvideo: ctvideo.isActive(),
                    data_type=bool,
                    triggers=LimaTriggers.ACTIONTRIGGERED)


LimaVideoStartLive = create_io_class(CtVideo.STARTLIVE,
                    io_setter=lambda ctvideo, data: ctvideo.startLive(),
                    icon='lima:icons/live_start')


LimaVideoStopLive = create_io_class(CtVideo.STOPLIVE,
                    io_setter=lambda ctvideo, data: ctvideo.stopLive(),
                    icon='lima:icons/live_stop')


LimaVideoLive = create_io_class(CtVideo.LIVE,
                    io_getter=lambda ctvideo: ctvideo.getLive(),
                    data_type=bool,
                    triggers=LimaTriggers.ACTIONTRIGGERED,
                    colors={
                        False: 'light green',
                        True: 'blue'})


LimaVideoExposure = create_io_class(CtVideo.EXPOSURE,
                    io_setter=lambda ctvideo, data: ctvideo.setExposure(data),
                    io_getter=lambda ctvideo: ctvideo.getExposure(),
                    data_type=float,
                    triggers=LimaTriggers.ACTIONTRIGGERED)


LimaVideoGain = create_io_class(CtVideo.GAIN,
                    io_setter=lambda ctvideo, data: ctvideo.setGain(data),
                    io_getter=lambda ctvideo: ctvideo.getGain(),
                    data_type=float,
                    triggers=LimaTriggers.ACTIONTRIGGERED)


LimaVideoSupportedVideoMode = create_io_class(CtVideo.SUPPORTEDVIDEOMODE,
                    io_getter=lambda ctvideo: ctvideo.getSupportedVideoMode(),
                    data_type=list)
                    

@ioadapterdef(CtVideo.MODE,
              io_setter=lambda ctvideo, data: ctvideo.setMode(data),
              io_getter=lambda ctvideo: ctvideo.getMode(),
              data_type=enums.VideoMode)
class LimaVideoMode(IoAdapterBase):
    # TODO
    def enum_names(self):
        allowed = self.model().get_io_instance(CtVideo.SUPPORTEDVIDEOMODE)
        values = allowed.data()
        return [value.name for value in values]


LimaVideoVideoSource = create_io_class(CtVideo.VIDEOSOURCE,
                    io_setter=lambda ctvideo, data: ctvideo.setVideoSource(data),
                    io_getter=lambda ctvideo: ctvideo.getVideoSource(),
                    data_type=enums.VideoSource,
                    triggers=LimaTriggers.ACTIONTRIGGERED)


LimaVideoRoi = create_io_class(CtVideo.ROI,
                    io_setter=lambda ctvideo, data: ctvideo.setRoi(data),
                    io_getter=lambda ctvideo: ctvideo.getRoi(),
                    data_type=Core.Roi,
                    triggers=LimaTriggers.ACTIONTRIGGERED)


LimaVideoBin = create_io_class(CtVideo.BIN,
                    io_setter=lambda ctvideo, data: ctvideo.setBin(data),
                    io_getter=lambda ctvideo: ctvideo.getBin(),
                    data_type=Core.Bin,
                    triggers=LimaTriggers.ACTIONTRIGGERED)


LimaVideoLastImage = create_io_class(CtVideo.LASTIMAGE,
                    io_getter=lambda ctvideo: ctvideo_image_to_video_image(
                    ctvideo.getLastImage()),
                    data_type=LimaVideoImage,#Core.CtVideo.Image,
                    hidden=True)


LimaVideoLastImageCounter = create_io_class(CtVideo.LASTIMAGECOUNTER,
                    io_getter=lambda ctvideo: ctvideo.getLastImageCounter(),
                    data_type=long,
                    triggers=LimaTriggers.ACTIONTRIGGERED)
