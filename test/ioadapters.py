from limagui.paramtree import IoAdapter
from limagui.paramtree import qt, Observer


def on_event(*args, **kwargs):
    print('EVENT', args, kwargs)

obs = Observer(callback=on_event)

val = 5

def io_read(*args, **kwargs):
    print(f'io_read', args, kwargs)
    return val

def io_write(*args, **kwargs):
    global val
    print(f'io_write', args, kwargs)
    val += 1
    print(f' new {val}')
    return val

ioc = IoAdapter(int, instance=False)

io = ioc(io_write=io_write, io_read=io_read)
io.register_observer(obs)
io.start()

d = io.data()
print(d)
d = io.set_data(12)
print(d)
d = io.data()#cache=False)
print(d)
d = io.set_data(12)