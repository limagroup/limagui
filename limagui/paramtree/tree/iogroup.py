# coding: utf-8
__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from collections import OrderedDict, deque

from .ioadapter import (get_io_adapter_class,
                        parse_io_name, IoAdapter,
                        registered_io_names)
from .. import Observer


def _to_list(value):
    if value is None:
        return []
    if not isinstance(value, (tuple, list, set,)):
        return [value]
    return list(value)


class EventChain:

    def __init__(self, ioadapter, **kwargs):
        self._chain = OrderedDict()
        self._kwargs = OrderedDict()
        self.add_event(ioadapter.io_name, None)
        self.set_kwargs(ioadapter, **kwargs)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        for ioadapter, kwargs in self._kwargs.values():
            ioadapter._notify_view(**kwargs)

    def stop(self):
        self._chain.clear()
        self._kwargs.clear()

    def get_kwargs(self, io_name):
        return self._kwargs[io_name]

    def add_event(self, ioadapter, trigger):
        """
        Updates the chain.
        Returns:
        - 0 if sender was already in past senders
        - 1 if this io was already triggered, but not by this sender
        - 2 if thie io was not triggered at all
        """
        # print('   ADD EVENT', ioadapter.io_name, trigger)
        past_triggers = self._chain.get(ioadapter)
        if past_triggers is None:
            self._chain[ioadapter] = set([trigger])
            rc = 2
        elif trigger in past_triggers:
            rc = 0
        else:
            past_triggers |= set([trigger])
            rc = 1
        return rc

    def set_kwargs(self, ioadapter, **kwargs):
        # print('NEW KWARGS', ioadapter.io_name, kwargs)
        self._kwargs[ioadapter.io_name] = (ioadapter, kwargs)


class IoGroup:
    def __init__(self, name=''):
        # TODO : weakrefs?
        self._stopping = False
        self._name = name
        self._sources = {}
        self._io_adapters = {}
        self._io_infos = {}
        self._substitutes = {}
        self._event_queue = deque()
        self._current_chain = deque()
        self._current_view_data = dict()

        self._observer = Observer(parent=self, uid='IoGroup', callback=self._next_event)

    def _set_name(self, name):
        self._name = name

    def name(self):
        return self._name

    def add_source(self, name, obj):
        if name in self._sources:
            raise RuntimeError(f'There is already a source registered '
                               f'with the name {name}.')
        self._sources[name] = obj

    def source(self, name):
        return self._sources[name]

    def has_source(self, name):
        try:
            self.source(name)
        except KeyError:
            return False
        return True

    def io_info(self, name):
        return get_io_adapter_class(name)

    def io_adapter_exists(self, name):
        return name in self._io_adapters

    def registered_adapters(self):
        return registered_io_names()

    def set_substitute(self, orig, sub):
        # print(orig, sub)
        cur_sub_io = self._substitutes.get(orig)
        if cur_sub_io:
            raise RuntimeError(f'IO {orig} already has a substitute ({cur_sub_io})')
        self._substitutes[orig] = sub
        # TODO: apply sub to existing instances

    def io_adapter(self, name, start=True, hidden=True):
        try:
            io_info = self.io_info(name)
            if not io_info:
                return
            if not hidden and io_info.hidden:
                return None
            try:
                return self._io_adapters[name]
            except KeyError:
                pass
            source = None
            io_name_info = parse_io_name(name)
            sub_io = self._substitutes.get(name)
            if not sub_io is not None and io_name_info.source:
                source = self.source(io_name_info.source)
            io_adapter = IoAdapter(io_info.io_name,
                                source=source,
                                io_group=self,
                                substitute=sub_io)
            # TODO
            if not io_adapter:
                return None

            self._io_adapters[name] = io_adapter

            if start:
                io_adapter.start()
                
        except Exception as ex:
            print(f'Error while setting up IO {name}: {ex}')
            raise
        
        return io_adapter

    def stop_group(self):
        self._observer.clear()
        self._stopping = True
        for io_adapter in self._io_adapters.values():
            io_adapter.stop()

    
    def _trigger(self, ioadapter, _sender=None, _chain=None, **kwargs):
        if _chain is None:
            if _sender is not None:
                raise RuntimeError('DUH', ioadapter.io_name, _sender)
            self._event_queue.append((ioadapter, kwargs))
        else:
            self._current_chain.append([ioadapter, _chain, kwargs])
        self._observer.notify()

    def _next_event(self, *args, **kwargs):
        try:
            next_io, chain, kwargs = self._current_chain.popleft()

            if self._stopping:
                chain.stop()
                self._current_chain.clear()
                self._event_queue.clear()
                self._current_view_data.clear()
                return

            self._current_view_data[next_io.io_name] = (next_io, kwargs)
            chain.set_kwargs(next_io, **kwargs)
            next_io._trig_observers(_chain=chain, sender=next_io.io_name, **kwargs)
        except IndexError:
            try:
                if self._stopping:
                    self._current_chain.clear()
                    self._event_queue.clear()
                    self._current_view_data.clear()
                    return

                self._current_view_data = dict()
                next_io, kwargs = self._event_queue.popleft()

                self._current_view_data = {next_io.io_name: (next_io, kwargs)}
                with EventChain(next_io, **kwargs) as chain:
                    next_io._trig_observers(_chain=chain, sender=next_io.io_name, **kwargs)
            except IndexError:
                return
