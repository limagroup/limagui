# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from limagui.paramtree import IoAdapterBase, create_io_class, ioadapterdef


from .limaionames import CtAccumulation, CtControl, LimaTriggers


LimaAccActive = create_io_class(CtAccumulation.ACTIVE,
            io_getter=lambda ctaccu: ctaccu.getActive(),
            io_setter=lambda ctaccu, data: ctaccu.setActive(data),
            data_type=bool,
            triggers=LimaTriggers.REFRESH_TRIGGER)


LimaAccPixelThresholdValue = create_io_class(CtAccumulation.PIXEL_THRESHOLD_VALUE,
            io_getter=lambda ctaccu: ctaccu.getPixelThresholdValue(),
            io_setter=lambda ctaccu, data: ctaccu.setPixelThresholdValue(
                data),
            data_type=int,
            triggers=LimaTriggers.REFRESH_TRIGGER)
