# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"

from Lima import Core
from limagui.paramtree.utils.sip_utils import sipenum2python

"""
Constants.h
TODO : tests
"""

AcqStatus = sipenum2python(Core, Core.AcqStatus)
RotationMode = sipenum2python(Core, Core.RotationMode)
AcqMode = sipenum2python(Core, Core.AcqMode)
TrigMode = sipenum2python(Core, Core.TrigMode)
ImageType = sipenum2python(Core, Core.ImageType)
VideoMode = sipenum2python(Core, Core.VideoMode)
VideoSource = sipenum2python(Core.CtVideo, Core.CtVideo.VideoSource)
AccTimeMode = sipenum2python(Core.CtAcquisition, Core.CtAcquisition.AccTimeMode)

# TODO
# FIXME : temporary workaround the fact that the EventOther error code
# doesnt seem to be exported to python
ErrorCode = sipenum2python(Core.CtControl, Core.CtControl.ErrorCode)
CameraErrorCode = sipenum2python(Core.CtControl, Core.CtControl.CameraErrorCode)
SavingMode = sipenum2python(Core.CtSaving, Core.CtSaving.SavingMode)
FileFormat = sipenum2python(Core.CtSaving, Core.CtSaving.FileFormat)
OverwritePolicy = sipenum2python(Core.CtSaving, Core.CtSaving.OverwritePolicy)
ManagedMode = sipenum2python(Core.CtSaving, Core.CtSaving.ManagedMode)
DebType = sipenum2python(Core, Core.DebType)