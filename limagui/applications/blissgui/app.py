# coding: utf-8
from __future__ import absolute_import

from gevent import monkey; monkey.patch_all()   # noqa

import gevent
import sys
import argparse

from limagui.paramtree import IoGroup, IoAdapterBase
from limagui.paramtree.qt import Qt, ParamTreeView

from .bliss_io.bliss_axis import create_bliss_axis_ios


class GEventProcessing:

    """Interoperability class between Qt/gevent that allows processing gevent
    tasks during Qt idle periods."""

    def __init__(self, idle_period=0.010):
        # Limit the IDLE handler's frequency while still allow for gevent
        # to trigger a microthread anytime
        self._idle_period = idle_period
        # IDLE timer: on_idle is called whenever no Qt events left for
        # processing
        self._timer = Qt.QTimer()
        self._timer.timeout.connect(self.process_events)
        self._timer.start(0)

    def __enter__(self):
        pass

    def __exit__(self, *exc_info):
        self._timer.stop()

    def process_events(self):
        # Cooperative yield, allow gevent to monitor file handles via libevent
        gevent.sleep(self._idle_period)


def parse_args():
    parser = argparse.ArgumentParser(prog='blissgui',
                                     description='Bbblblbblississ')

    parser.add_argument('session', help='Session name',
                        type=str)
    
    parsed_args = parser.parse_args()

    return parsed_args


def blissgui_main():
    bliss_args = parse_args()

    app = Qt.QApplication([])

    win = Qt.QMainWindow()
    tree = ParamTreeView()
    win.setCentralWidget(tree)

    session_name = bliss_args.session
    io_group = IoGroup(name=session_name)

    ios = create_bliss_axis_ios(io_group, session_name)
    # io.start()

    tree.model().insertNodeData(data=ios, name=session_name)

    tree.expandAll()

    win.show()


    with GEventProcessing():
        sys.exit(app.exec_())
