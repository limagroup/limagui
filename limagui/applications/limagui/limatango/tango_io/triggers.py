import time
from collections import deque

from Lima import Core

import tango

from limagui.paramtree import IoAdapterBase, create_io_class, ioadapterdef
from limagui.paramtree.tree.extra import TangoIoAdapter, create_tg_io_class, tangoioadapter
from .tangoionames import TangoLimaTriggers, TangoLimaCCDs, TangoLimaCtControl
from .videoutils import tango_read_image_to_lima_image
from .conversions import string2status


IMG_COUNT_POLL_TIME = 50


# LimaCCDsStatusCallback = create_tg_io_class(TangoLimaTriggers.STATUS_CALLBACK_STATUS,
#             hidden=True,
#             tango_events = tango.EventType.CHANGE_EVENT,
#             tango_event_same_thread = True,
#             tango_conversion = [string2status.get, None])
    

# @ioadapterdef(TangoLimaTriggers.STATUS_CALLBACK_CNT,
#             hidden=True,
#               tango_events=tango.EventType.CHANGE_EVENT,
#               tango_event_same_thread=True)
# class LimaCCDsCounterCallback(TangoIoAdapter):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self._has_events = None
#         self._status = None

    # def _on_start(self):
    #     print('START')
    #     return super()._on_start()

    # def _on_trigger(self, sender=None, data=None, _first_trig=True, **kwargs):
    #     print('trig', sender, data)
        # trig = False
        # if sender == LimaTriggers.ACTIONTRIGGERED:
        #     trig = True

        # if sender == CtControl.STATUS_ACQUISITIONSTATUS:
        #     status = kwargs.get('data')
        #     if status != self._lastStatus:
        #         trig = True
        #         self._lastStatus = status

        # if trig:
        # super()._on_trigger(sender=sender, data=data, _first_trig=_first_trig, **kwargs)

    # def _filter_trigger(self, sender=None, data=None, **kwargs):
    #     if data is None:
    #         return False
    #     if sender == TangoLimaCCDs.TANGO_PUSH_IMAGE_EVENTS:
    #         if data != self._has_events:
    #             self._has_events = data
    #             if self._status is not None:
    #                 status_io = self.model().get_io_instance(TangoLimaTriggers.STATUS_CALLBACK_STATUS)
    #                 self._status = status_io.data()
    #             if not self._has_events and self._status == Core.AcqRunning:
    #                 self.set_refresh_delay(IMG_COUNT_POLL_TIME)
    #             else:
    #                 self.set_refresh_delay(0)
    #         return True
    #     elif sender in (TangoLimaTriggers.STATUS_CALLBACK_STATUS,
    #                     TangoLimaCtControl.STATUS_ACQUISITIONSTATUS):
    #         if data != self._status:
    #             self._status = data
    #             if data == Core.AcqRunning and not self._has_events:
    #                 self.set_refresh_delay(IMG_COUNT_POLL_TIME)
    #             else:
    #                 self.set_refresh_delay(0)
    #     return False


# @ioadapterdef(TangoLimaTriggers.STATUS_CALLBACK_IMG,
#             hidden=True,
#             tango_events = tango.EventType.CHANGE_EVENT,
#     tango_event_same_thread = True)
# class LimaCCDsImgCallback(IoAdapterBase):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self._has_events = None
#         self._status = None
#         self._last_counter = None
#         self._status = self.model().get_io_instance(TangoLimaTriggers.STATUS_CALLBACK_STATUS).data(cache=True)

#     def _filter_trigger(self, sender=None, data=None, **kwargs):
#         filter = False
#         if data is None:
#             return False
#         if sender == TangoLimaCCDs.TANGO_PUSH_IMAGE_EVENTS:
#             if data != self._has_events:
#                 self._has_events = data
#             filter = True
#         elif sender == TangoLimaTriggers.STATUS_CALLBACK_STATUS:
#             # print('=====BAAM', data, self._status, self._last_counter, self._has_events)
#             if data != self._status:
#                 self._status = data
#                 if data == Core.AcqRunning:
#                     self._last_counter = None
#             filter = True
#         elif sender == TangoLimaTriggers.STATUS_CALLBACK_CNT:
#             # print('=====BOOM', data, self._last_counter, self._has_events)
#             if data >= 0:
#                 if not self._has_events:
#                     if data == self._last_counter:
#                         filter = True
#                     else:
#                         # print('READ')
#                         self.data(cache=False, wait=False)
#                 #     self._last_counter = data
#                 # else:
#                 #     self._last_counter = None
#                 # filter = True
#             else:
#                 filter = True
#             self._last_counter = data
#         return filter



# @ioadapterdef(TangoLimaTriggers.STATUS_CALLBACK,
#               triggers=TangoLimaCtControl.IMAGESTATUS,
#               hidden=True)
# class LimaCCDsImgStatusCallback(IoAdapterBase):
#     def _on_trigger(self, sender=None, data=None, **kwargs):
#         self.set_data(data, _internal=True)


@ioadapterdef(TangoLimaTriggers.LASTIMAGE,
            #   triggers=TangoLimaTriggers.STATUS_CALLBACK,
              io_getter=lambda src: None,
            #   tango_conversion=LIMA_TANGO_CONVERSIONS[TangoLimaTriggers.LASTIMAGE],
              hidden=True)
class TangoLimaLastImage(TangoIoAdapter):
    # def __init__(self, *args, **kwargs):
    #     kwargs['io_getter'] = self._readimage
    #     super().__init__(*args, **kwargs)


    def _read_image(self, counter=None):
        # print('READUY', counter)
        if counter is None:
            raise RuntimeError(f'In {self.io_name}::_read_image: expected a counter.')

        # image = self.source().ReadImage(last_counter)
        # image = lima_processlib_data_to_image(image)
        image = self._tango_io_getter(self.source(), counter=counter)
        image = tango_read_image_to_lima_image(image, counter)
        # import numpy as np
        # import os
        # np.save(os.path.expanduser(f'~/tmp/lima_img_{time.time()}.npy'), image)
        return image

    # def data(self, cache=True, **kwargs):
    #     return super().data(cache=cache, **kwargs)


# @ioadapterdef(TangoLimaTriggers.STATUS_CALLBACK,
#             hidden=True)
# class LimaCCDsImgStatusCallback(IoAdapterBase):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self._queue = deque(maxlen=2)
#         self._last_counter = None
#         self._cb_data = None
#         self._last_status = None
#         self._start_timestamp = None
#         self._last_image = None
#         self._last_image_status = None

#         self._last_event_cnt = None

#         self._in_status = False

#     def _trigger(self, sender=None, data=None, **kwargs):
#         # print('TRIG', sender.name, data.__class__, self._in_status)
#         # if sender != TangoLimaTriggers.STATUS_CALLBACK_IMG:
#         #     print(sender, data)
#         if sender==TangoLimaTriggers.STATUS_CALLBACK_STATUS and self._in_status:
#             return
#         image = None
#         status = self._last_status
#         trigger = False
#         image_status = None

#         if sender == TangoLimaTriggers.STATUS_CALLBACK_CNT:
#             self._last_event_cnt = data
#         elif sender == TangoLimaTriggers.STATUS_CALLBACK_IMG:
#             # print('###### IMG!!!!!!')
#             try:
#                 image = tango_read_image_to_lima_image(data, self._last_event_cnt)
#                 self._last_image = image
#                 self._last_counter = self._last_event_cnt
#                 trigger = True
#                 if self._last_counter is not None:
#                     image_status = Core.CtControl.ImageStatus(-1, -1, self._last_counter, -1, -1)
#                     self._last_image_status = image_status
#             except Exception as ex:
#                 print(f'ERROR in {self.io_name.name}._trigger: {ex}.')
#         else:
#             status = data #string2status[data]

#         if self._last_status is None and status is None and not self._in_status:
#             self._in_status = True
#             status = self.model().get_io_instance(TangoLimaTriggers.STATUS_CALLBACK_STATUS).data()
#             self._in_status = False
#             # status = string2status[status]
            
#         counter = self._last_event_cnt

#         if counter is not None:
#             if counter < 0:
#                 self._cb_data = None
#                 self._queue.clear()
#                 self._start_timestamp = None
#                 self._last_image = None
#                 self._last_counter = None
#                 self._last_image_status = Core.CtControl.Status()

#             if counter <= 0:
#                 # - counter is < 0 when there is a prepareacq
#                 # - forcing a trigger after the first frame (counter == 0)
#                 #   because the acquisition status is back to
#                 #   READY when the callback is called when
#                 #   acqnbframes == 1
#                 # self._last_counter = None
#                 self._last_status = None
#                 # trig status io to trig image io and trigger refresh all params

#             if counter >= 0 and self._start_timestamp is None:
#                 self._start_timestamp = time.time()

#         if status != self._last_status:
#             trigger = True

#         if image or trigger:
#             self._queue.append((self._start_timestamp, status, self._last_image_status, self._last_image))

#         self._last_status = status

#         if self._last_status != Core.AcqRunning:
#             self._start_timestamp = None

#         if trigger:
#             self.data(cache=False)

#         # print('OUT', trigger)

#     def io_getter(self, source):
#         try:
#             cb_data = self._queue.popleft()
#             self._cb_data = cb_data
#         except IndexError:
#             pass
#         return self._cb_data
        
