# -*- coding: utf-8 -*-

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"

from contextlib import contextmanager

import numpy as np

from silx.gui import qt as Qt
from silx.gui.plot.utils.axis import SyncAxes
from silx.gui.plot import PlotWidget, PlotWindow


class ProfileWidget(PlotWindow):
    """
    A rather crude widget displaying an image and some profiles on the side.
    Axes are linked and there are some scrollbars.

    Scrollbars  probably (pretty sure actually) wont work with fancy scaling and all.
    """

    PROFILE_MIN_WIDTH = 200
    IMAGE_MIN_SIZE = 200
    SCROLL_STEPS = 1000

    IMG_LEGEND = 'BPM_IMAGE'

    def __init__(self, parent=None, backend=None):
        # WARNING: position = False
        super(ProfileWidget, self).__init__(parent=parent,
                                            backend=backend,
                                            position=True)

        self.__locked = False

        self.setYAxisInverted(True)

        centralWid = Qt.QWidget()

        hProfWid = self._hProfWid = PlotWidget()
        vProfWid = self._vProfWid = PlotWidget()
        hProfWid.setGraphTitle('H profile')
        vProfWid.setGraphTitle('V profile')
        hProfWid.getXAxis().setAutoScale(False)
        vProfWid.getYAxis().setAutoScale(False)

        hProfWid.getXAxis().setLabel('X')
        vProfWid.getYAxis().setLabel('Y')
        hProfWid.getYAxis().setLabel('I')
        vProfWid.getXAxis().setLabel('I')

        hProfWid.getWidgetHandle().setMinimumHeight(
            self.PROFILE_MIN_WIDTH)
        hProfWid.getWidgetHandle().setMaximumHeight(
            self.PROFILE_MIN_WIDTH)

        vProfWid.getWidgetHandle().setMinimumWidth(
            self.PROFILE_MIN_WIDTH)
        vProfWid.getWidgetHandle().setMaximumWidth(
            self.PROFILE_MIN_WIDTH)

        hScroll = self._hScroll = Qt.QScrollBar(Qt.Qt.Horizontal)
        vScroll = self._vScroll = Qt.QScrollBar(Qt.Qt.Vertical)

        layout = Qt.QGridLayout(centralWid)

        superCentral = self.centralWidget()
        self.setCentralWidget(superCentral)

        layout.addWidget(superCentral, 0, 0)
        # layout.addWidget(self.getWidgetHandle(), 0, 0)
        layout.addWidget(hProfWid, 1, 0)
        layout.addWidget(vProfWid, 0, 1)
        layout.addWidget(hScroll, 2, 0)
        layout.addWidget(vScroll, 0, 2)

        layout.setRowMinimumHeight(0, self.IMAGE_MIN_SIZE)
        layout.setColumnMinimumWidth(0, self.IMAGE_MIN_SIZE)
        layout.setRowMinimumHeight(1, self.PROFILE_MIN_WIDTH)
        layout.setColumnMinimumWidth(1, self.PROFILE_MIN_WIDTH)
        layout.setColumnStretch(0, 1)
        layout.setRowStretch(0, 1)
        layout.setRowStretch(1, 0)
        layout.setColumnStretch(1, 0)

        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)

        self.setCentralWidget(centralWid)

        xAxis = self.getXAxis()
        yAxis = self.getYAxis()

        self._syncXAxes = SyncAxes([xAxis, hProfWid.getXAxis()])
        self._syncYAxes = SyncAxes([yAxis, vProfWid.getYAxis()])

        xLimits = xAxis.getLimits()
        yLimits = yAxis.getLimits()
        self._hRange = xLimits
        self._vRange = yLimits
        self._updateScrollbar(hScroll, xLimits)
        self._updateScrollbar(vScroll, yLimits)

        self.getXAxis().sigLimitsChanged.connect(self._onProfSignal)
        self.getYAxis().sigLimitsChanged.connect(self._onProfSignal)

        self._hScroll.valueChanged.connect(self._scrollValueChanged)
        self._vScroll.valueChanged.connect(self._scrollValueChanged)

    @contextmanager
    def _lockScrollbars(self):
        self.__locked = True
        yield
        self.__locked = False

    def _updateScrollbar(self, scrollbar, limits):
        if self.__locked:
            return

        if scrollbar == self._hScroll:
            irange = self._hRange
            invert = False
        elif scrollbar == self._vScroll:
            irange = self._vRange
            axis = self.getYAxis()
            invert = not axis.isInverted()
        else:
            print('{0}._updateScrollbar : Unknown slider.'
                  ''.format(self.__class__.__name__))
            return

        # attempt at supporting fancy axis setups (log, scale, ...)
        # prolly wont be tested
        imin, imax = irange
        iwidth = imax - imin

        minimum, maximum = limits
        width = maximum - minimum

        widthSteps = int(width * self.SCROLL_STEPS / iwidth)
        if minimum <= imin:
            posSteps = 0
        elif minimum >= imax:
            posSteps = self.SCROLL_STEPS
        else:
            posSteps = int((minimum - imin) * self.SCROLL_STEPS / iwidth)

        if invert:
            posSteps = self.SCROLL_STEPS - posSteps - widthSteps

        blocked = scrollbar.blockSignals(True)
        scrollbar.setRange(0, self.SCROLL_STEPS - widthSteps)
        scrollbar.setPageStep(widthSteps)
        scrollbar.setValue(posSteps)
        scrollbar.blockSignals(blocked)

    def _onProfSignal(self, low, high):
        sender = self.sender()
        if sender == self.getXAxis():
            scroll = self._hScroll
        elif sender == self.getYAxis():
            scroll = self._vScroll
        else:
            return

        self._updateScrollbar(scroll, [low, high])

    def _scrollValueChanged(self, value):
        scroll = self.sender()
        if scroll == self._hScroll:
            axis = self.getXAxis()
            iRange = self._hRange
            invert = False
        elif scroll == self._vScroll:
            axis = self.getYAxis()
            iRange = self._vRange
            invert = not axis.isInverted()
        else:
            print('{0}._updateScrollbar : Unknown slider.'
                  ''.format(self.__class__.__name__))
            return

        imin, imax = iRange
        iwidth = imax - imin

        minimum, maximum = axis.getLimits()
        width = maximum - minimum

        position = value * iwidth / self.SCROLL_STEPS
        if invert:
            newMaximum = imax - position
            newMinimum = newMaximum - width
        else:
            newMinimum = position
            newMaximum = newMinimum + width

        with self._lockScrollbars():
            axis.setLimits(newMinimum, newMaximum)

    def setProfileData(self, image, hProf, vProf, resetzoom=False):
        """
        Sets the image and side curves to be displayed.
        :param image:
        :param hProf:
        :param vProf:
        :param kwargs: keyword arguments passed to the
        :return:
        """
        height, width = image.shape[0:2]

        self._hRange = [0, width]
        self._vRange = [0, height]

        self.addImage(image, legend=self.IMG_LEGEND, resetzoom=resetzoom)

        with self._lockScrollbars():
            x = np.arange(width)
            if hProf is not None and hProf.shape[0] == width:
                self._hProfWid.addCurve(x, hProf)
            else:
                print('Invalid H profile.')
                self._hProfWid.clear()

            y = np.arange(height)
            if vProf is not None and vProf.shape[0] == height:
                self._vProfWid.addCurve(vProf, y)
            else:
                print('Invalid V profile.')
                self._vProfWid.clear()


if __name__ == '__main__':
    app = Qt.QApplication([])
    import numpy as np
    win = ProfileWidget()
    img = np.random.randint(0, 255, (1024, 1024, 3), dtype=np.uint8)
    profx = np.random.rand(1024)
    profy = np.random.rand(1024)
    win.setProfileData(img, profx, profy)
    win.show()
    app.exec_()
