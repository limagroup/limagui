
class Action(object):
    text = property(lambda self: self._text)
    checkable = property(lambda self: self._checkable)
    icon = property(lambda self: self._icon)
    # callbacks = property(lambda self: self._callback[:])
    radio = property(lambda self: self._radio)
    checked = property(lambda self: self._checked)
    checkbox = property(lambda self: self._checkbox)

    def __init__(self,
                 checked=False,
                 text=None,
                 icon=None,
                 radio=None,
                 checkable=None,
                 checkbox=None):
        self._checked = checked
        self._text = text
        self._checkable = checkable
        self._icon = icon
        self._radio = radio
        self._checkbox = checkbox
        

class Actions(list):
    exclusive = property(lambda self: self._exclusive)

    def __init__(self, actions, exclusive=False):
        super(Actions, self).__init__(actions)
        self._exclusive = exclusive


class CheckBox(Action):
    def __init__(self, *args, **kwargs):
        super(CheckBox, self).__init__(*args, **kwargs)
        self._checkable = True