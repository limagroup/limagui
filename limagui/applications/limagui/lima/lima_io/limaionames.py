# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from limagui.paramtree import IoNamesEnum, ionames_enum_def

_PLUGIN_NAME = 'lima'


# @ionames_enum_def(_PLUGIN_NAME)
# class LimaCommon(IoNamesEnum):
#     REFRESH = 'Lima@:Refresh'


@ionames_enum_def(_PLUGIN_NAME)
class CtAccumulation(IoNamesEnum):
    ACTIVE = 'Lima@CtAccumulation:Active'
    PIXEL_THRESHOLD_VALUE = 'Lima@CtAccumulation:PixelThresholdValue'


@ionames_enum_def(_PLUGIN_NAME)
class CtAcquisition(IoNamesEnum):
    LATENCYTIME = 'Lima@CtAcquisition:LatencyTime'
    ACQNBFRAMES = 'Lima@CtAcquisition:AcqNbFrames'
    CONCATNBFRAMES = 'Lima@CtAcquisition:ConcatNbFrames'
    ACCEXPOTIME = 'Lima@CtAcquisition:AccExpoTime'
    ACCNBFRAMES = 'Lima@CtAcquisition:AccNbFrames'
    ACCMAXEXPOTIME = 'Lima@CtAcquisition:AccMaxExpoTime'
    ACCTIMEMODE = 'Lima@CtAcquisition:AccTimeMode'
    ACCDEADTIME = 'Lima@CtAcquisition:AccDeadTime'
    ACCLIVETIME = 'Lima@CtAcquisition:AccLiveTime'
    ACQEXPOTIME = 'Lima@CtAcquisition:AcqExpoTime'
    ACQMODE = 'Lima@CtAcquisition:AcqMode'
    TRIGGERMODE = 'Lima@CtAcquisition:TriggerMode'
    # PARS = 'Lima@CtAcquisition:Pars'


@ionames_enum_def(_PLUGIN_NAME)
class CtControl(IoNamesEnum):
    STOPACQ = 'Lima@CtControl:stopAcq'
    PREPAREACQ = 'Lima@CtControl:prepareAcq'
    STARTACQ = 'Lima@CtControl:startAcq'
    ABORTACQ = 'Lima@CtControl:abortAcq'
    RESET = 'Lima@CtControl:reset'
    IMAGESTATUS = 'Lima@CtControl:ImageStatus'
    STATUS = 'Lima@CtControl:Status'
    # STATUS_ACQUISITIONSTATUS = 'Lima@:Status/AcquisitionStatus'
    # STATUS_CAMERASTATUS = 'Lima@CtControl:Status/CameraStatus'
    # STATUS_ERROR = 'Lima@CtControl:Status/Error'
    # STATUS_IMAGECOUNTERS = 'Lima@CtControl:Status/ImageCounters'
    READIMAGE = 'Lima@CtControl:ReadImage'
    READBASEIMAGE = 'Lima@CtControl:ReadBaseImage'
    # IMAGESTATUSCALLBACK = 'Lima@CtControl:ImageStatusCallback'


@ionames_enum_def(_PLUGIN_NAME)
class CtImage(IoNamesEnum):
    BIN = 'Lima@CtImage:Bin'
    ROI = 'Lima@CtImage:Roi'
    FLIP = 'Lima@CtImage:Flip'
    ROTATION = 'Lima@CtImage:Rotation'


@ionames_enum_def(_PLUGIN_NAME)
class CtVideo(IoNamesEnum):
    ACTIVE = 'Lima@CtVideo:Active'
    STARTLIVE = 'Lima@CtVideo:startLive'
    STOPLIVE = 'Lima@CtVideo:stopLive'
    LIVE = 'Lima@CtVideo:Live'
    EXPOSURE = 'Lima@CtVideo:Exposure'
    GAIN = 'Lima@CtVideo:Gain'
    MODE = 'Lima@CtVideo:Mode'
    VIDEOSOURCE = 'Lima@CtVideo:VideoSource'
    SUPPORTEDVIDEOMODE = 'Lima@CtVideo:SupportedVideoMode'
    ROI = 'Lima@CtVideo:Roi'
    BIN = 'Lima@CtVideo:Bin'
    LASTIMAGE = 'Lima@CtVideo:LastImage'
    LASTIMAGECOUNTER = 'Lima@CtVideo:LastImageCounter'


@ionames_enum_def(_PLUGIN_NAME)
class LimaTriggers(IoNamesEnum):
    ACTIONTRIGGERED = 'Lima@:_LimaActionTriggered'
    # ACQSTATUS_TRIGGER = 'Lima@:_LimaAcqStatusTrigger'
    REFRESH_TRIGGER = 'Lima@:_LimaRefreshTrigger'
    STATUS_CALLBACK = 'Lima@CtControl:_LimaStatusCallback'
    ACQSTATUS_CHANGE = 'Lima@:_LimaAcqqStatusChange'
    IMAGEREADY = 'Lima@:_LimaImageReady'
    IMG_POLL_DELAY = 'Lima@:_LimaImgPollDelay'
    

@ionames_enum_def(_PLUGIN_NAME)
class HwInterface(IoNamesEnum):
    CAP_SYNC_VALIDRANGES = 'Lima@HwInterface:Sync/ValidRanges'


@ionames_enum_def(_PLUGIN_NAME)
class LimaImages(IoNamesEnum):
    LASTIMAGE = 'Lima@CtControl:Last Image'


LIMA_IO_NAMES = [CtAccumulation,
                 CtAcquisition,
                 CtControl,
                 CtImage,
                 CtVideo,
                 LimaTriggers,
                 HwInterface]