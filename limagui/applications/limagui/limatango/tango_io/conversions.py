# coding: utf-8


from Lima import Core


from .tangoionames import (TangoLimaCtControl,
                           TangoLimaCtVideo,
                           TangoLimaHwSync,
                           TangoLimaCtImage,
                           TangoLimaCtAcquisition, TangoLimaTriggers)

from .videoutils import (tango_read_image_to_lima_image,
                         tango_base_image_to_lima_image,
                         tango_last_image_to_lima_image,
                         tango_video_last_image_to_video_image)


from ...lima.type import enums


def rot_to_str(x):
    return {Core.Rotation_0:'NONE',
            Core.Rotation_90:'90',
            Core.Rotation_180:'180',
            Core.Rotation_270:'270',
            0:'NONE',
            90:'90',
            180:'180',
            270:'270'}[x]


def str_to_rot(x):
    if x is None or x == 'NONE':
        x = 0
    rot = getattr(Core, f'Rotation_{x}')
    return rot


err_state2string = {Core.CtControl.NoError : "No error",
                Core.CtControl.SaveUnknownError : "Saving: unknown error",
                Core.CtControl.SaveOpenError : "Saving: file open error",
                Core.CtControl.SaveCloseError : "Saving: file close error",
                Core.CtControl.SaveAccessError : "Saving: access error",
                Core.CtControl.SaveOverwriteError : "Saving: overwrite error",
                Core.CtControl.SaveDiskFull : "Saving: disk full",
                Core.CtControl.SaveOverun : "Saving: overun",
                Core.CtControl.ProcessingOverun : "Processing: overun",
                Core.CtControl.CameraError : "Camera: error"}

err_string2state = {v:k for k, v in err_state2string.items()}

string2status = {"Ready": Core.AcqReady,
                 "Running": Core.AcqRunning,
                 "Fault": Core.AcqFault,
                 "Configuration": Core.AcqConfig}


def _roi_to_list(roi):
    tl = roi.getTopLeft()
    s = roi.getSize()
    return [tl.x, tl.y, s.getWidth(), s.getHeight()]

LIMA_TANGO_CONVERSIONS = {
    TangoLimaHwSync.CAP_SYNC_VALIDRANGES : (lambda data: Core.HwSyncCtrlObj.ValidRangesType(*data),
                                            lambda data: [data.min_exp_time,
                                                          data.max_exp_time,
                                                          data.min_lat_time,
                                                          data.max_lat_time]),
    TangoLimaCtControl.READIMAGE: (tango_last_image_to_lima_image, None),
    TangoLimaTriggers.LASTIMAGE: (tango_last_image_to_lima_image, None),
    TangoLimaCtControl.READBASEIMAGE: (tango_base_image_to_lima_image, None),
    TangoLimaCtControl.STATUS_ERROR: (lambda s: err_string2state[s], None),
    TangoLimaCtAcquisition.ACCTIMEMODE: (lambda s: enums.AccTimeMode[s.capitalize()].value,
                                         lambda e:enums.AccTimeMode(e).name.upper()),
    TangoLimaCtAcquisition.ACQMODE: (lambda s: enums.AcqMode[s.capitalize()].value,
                                         lambda e:enums.AcqMode(e).name.upper()),
    TangoLimaCtImage.ROTATION: (str_to_rot, rot_to_str),
    TangoLimaCtImage.BIN: (lambda x: Core.Bin(*x), lambda x: [x.getX(), x.getY()]),
    TangoLimaCtImage.ROI: (lambda x: Core.Roi(*x), _roi_to_list),
    TangoLimaCtImage.FLIP: (lambda x: Core.Flip(*x), lambda x: [x.x, x.y]),
    TangoLimaCtVideo.LASTIMAGE: (tango_video_last_image_to_video_image, None),
    TangoLimaCtVideo.STOPLIVE: (lambda x: not x, lambda x: False),
    TangoLimaCtVideo.STARTLIVE: (lambda x: x, lambda x: True),
    TangoLimaCtVideo.BIN: (lambda x: Core.Bin(*x), lambda x: [x.getX(), x.getY()]),
    TangoLimaCtVideo.ROI: (lambda x: Core.Roi(*x), _roi_to_list),
    TangoLimaCtVideo.MODE: (lambda x: enums.VideoMode[x].value, lambda x: enums.VideoMode(x).name),
    TangoLimaCtVideo.VIDEOSOURCE: (lambda x: enums.VideoSource[x].value, lambda x: enums.VideoSource(x).name),
    TangoLimaCtVideo.SUPPORTEDVIDEOMODE: (lambda x: [enums.VideoMode[m].value for m in x], None)
}
