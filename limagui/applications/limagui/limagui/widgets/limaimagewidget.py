# -*- coding: utf-8 -*-

from __future__ import absolute_import

from limagui.paramtree.qt import Qt

from limagui.paramtree.utils.observer import Observer

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"


from time import time
import numpy as np

from Lima import Core

from silx.gui.plot import Plot2D

from limagui.paramtree import Scheduler
from limagui.paramtree.qt import Qt

from ...lima.lima_io.limaionames import (CtControl, CtVideo,
                                         CtImage,
                                         LimaTriggers,
                                         LimaImages)


_FPS_ARRAY_LENGTH = 10


class LimaImageWidget(Qt.QWidget):
    _IMG_LEGEND = '_LIMA_IMAGE_'

    def __init__(self, io_group, parent=None):
        super().__init__(parent=parent)
        self._status_io = io_group.io_adapter(LimaTriggers.ACQSTATUS_CHANGE)
        self._last_img_io = io_group.io_adapter(LimaImages.LASTIMAGE)
        # self._img_roi_io = io_group.io_adapter(CtImage.ROI)
        # self._vid_roi_io = io_group.io_adapter(CtVideo.ROI)

        self._io_group = io_group

        self._start_time = None

        self._poll_ival = 50
        self._poll = False

        self._plotVisible = True

        self._last_status = None
        self._last_img_rdy = None

        self._rst_zoom = True

        self._poller = Scheduler(self._pollEvent,
                                 1000,
                                 single_shot=True)

        mainLayout = Qt.QGridLayout(self)
        mainLayout.setContentsMargins(0, 0, 0, 0)

        ############################
        # Plot
        ############################

        pw = self._pw = Plot2D()
        grp_name = io_group.name()
        pw.setWindowTitle(grp_name)
        self.setStatusTip(grp_name)
        self.setToolTip(grp_name)
        #pw.getYAxis().setInverted(True)
        pw.setStatusTip(grp_name)
        pw.setToolTip(grp_name)

        mainLayout.addWidget(pw, 1, 0)

        ############################
        # Bottom widgets (FPS, ...)
        ############################
        bottomWidget = Qt.QGroupBox()
        # bottomWidget.setContentsMargins(0, 0, 0, 0)
        bottomLayout = Qt.QHBoxLayout(bottomWidget)
        bottomLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.addWidget(bottomWidget, 2, 0)

        bottomLayout.addWidget(Qt.QLabel('FPS:'))
        self._fpsLabel = Qt.QLabel()
        bottomLayout.addWidget(self._fpsLabel)
        bottomLayout.addStretch(1)
        self._fps_array = [None] * _FPS_ARRAY_LENGTH
        self._fps_array_idx = 0
        self._last_fps_disp_t = 0

        bottomLayout.addStretch(100)

        ############################

        self._counter = -1
        self._status = None

        self._limaimgview_ready = True

        self._observer = Observer(parent=self,
                                  uid=f'IMG_WIN_{grp_name}',
                                  callback=self._on_trigger)

    def hideEvent(self, *args, **kwargs):
        rc = super().hideEvent(*args, **kwargs)
        self._setVisibility(False)
        return rc

    def showEvent(self, *args, **kwargs):
        rc = super().showEvent(*args, **kwargs)
        self._setVisibility(True)
        return rc

    def _on_trigger(self, sender, data, **kwargs):
        if sender == LimaTriggers.ACQSTATUS_CHANGE:
            if data[0] == Core.AcqRunning:
                self._startPolling()

    def _startPolling(self):
        self._poll = self._plotVisible
        self._nextCounter()

    def _stopPolling(self):
        self._poll = False

    def _nextCounter(self):
        if self._poll and self._plotVisible:
            self._poller.start(self._poll_ival, single_shot=True)

    def _setVisibility(self, visible):
        """
        Allows to start stop the polling when the widget is hidden.
        """
        self._plotVisible = visible
        if visible:
            ios = [LimaTriggers.ACQSTATUS_CHANGE]
            for io in ios:
                io_adapt = self._io_group.io_adapter(io, hidden=True)
                if not io_adapt:
                    raise RuntimeError(f'Io {io.name} not found.')
                io_adapt.register_observer(self._observer)
            self._startPolling()
        else:
            self._observer.clear()
            self._stopPolling()

    def _pollEvent(self):
        data = self._last_img_io.data()
        if data is None:
            status = self._io_group.io_adapter(CtControl.STATUS).data(cache=True)
            if status.AcquisitionStatus == Core.AcqRunning:
                self._nextCounter()
            return
        self._processImageData(data)

    def _processImageData(self, data):
        """
        if Ready:
            if previous was Running, check frame number for new img
            else: noop
        if Running:
            if counter > -1 and != than previous counter: display img
            elif previous was Ready: display text saying image is OLD
        TODO: display start time of current run, displayed run
        TODO: use timestamp to check if new run has been started
        """
        if data is None:
            self._nextCounter()
            return

        status, last_img_rdy, image, start_time = data
        if status is None:
            self._nextCounter()
            return

        if last_img_rdy >= 0:
            if status == Core.AcqReady:
                if last_img_rdy != self._last_img_rdy or self._start_time != start_time:
                    self._setImage(data)
                self._stopPolling()
            elif status == Core.AcqRunning:
                if last_img_rdy != self._last_img_rdy or self._start_time != start_time:
                    self._setImage(data)
                self._startPolling()
            else:
                print(f'In LimaImageView, unhandled status {status}.')
        else:
            if status == Core.AcqRunning:
                self._nextCounter()
        self._last_status = status
        self._last_img_rdy = last_img_rdy

    def _setImage(self, data):
        try:
            if data is not None:
                status, last_img_rdy, image, start_t = data
            else:
                status, last_img_rdy, image, start_t = [None] * 4

            disp = False

            if data is None or image is None or not image.is_valid():
                self._clearPlot()
                self._start_time = None
                self._counter = None

                return

            if start_t != self._start_time or self._start_time is None:
                self._fps_array = [None] * _FPS_ARRAY_LENGTH
                self._fps_array_idx = 0
                self._start_time = start_t

            if last_img_rdy < 0:
                pass
            elif last_img_rdy == self._counter:
                if start_t != self._start_time and self._start_time is not None:
                    disp = True
                    self._start_time = start_t
            else:
                disp = True
                    
            # self._start_time = start_t
            # self._counter = last_img_rdy

            if status == Core.AcqRunning:
                self._nextCounter()

            pw = self._pw

            t0 = time()

            if disp:
                self._status = status
                number = image.number()
                if number is None:
                    number = last_img_rdy
                self._counter = number
                title = f'Frame {number}'

                # roi = self._img_roi_io.data(cache=True)
                # topLeft = roi.getTopLeft()
                # orig = topLeft.x, topLeft.y
                
                image_data = image.image()
                pw.addImage(image_data,
                            legend=self._IMG_LEGEND,
                            resetzoom=self._rst_zoom,
                            # origin=orig
                            )            
                pw.setGraphTitle(title)
                self._rst_zoom = False

                self._fps_array[self._fps_array_idx] = (t0, number)
                last_fps_data = self._fps_array[self._fps_array_idx - _FPS_ARRAY_LENGTH + 1]

                if t0 - self._last_fps_disp_t >= 1:
                    if last_fps_data is None:
                        last_fps_data = self._fps_array[0]
                        n_img = self._fps_array_idx
                    else:
                        n_img = _FPS_ARRAY_LENGTH - 1
                    t_diff = t0 - last_fps_data[0]
                    if t_diff == 0:
                        fps = np.nan
                    else:
                        fps = n_img / t_diff#(number - last_fps_data[1]) / t_diff
                    self._fpsLabel.setText(f'{fps:3.2f}')
                    self._last_fps_disp_t = t0
                self._fps_array_idx += 1
                self._fps_array_idx %= _FPS_ARRAY_LENGTH
            
            # t_disp = (time() - t0) * 1000
            # self._poll_ival = max(50, t_disp + 20)
        except Exception as ex:
            print('BOOM', ex)

    def _clearPlot(self):   
        pw = self._pw
        pw.remove(self._IMG_LEGEND)
        pw.setGraphTitle('No frame')
