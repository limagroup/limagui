# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


class Color(int):
    pass


class Colors(object):
    green = Color(0x00ff00)
    red = Color(0xff0000)
    blue = Color(0x0000ff)
    black = Color(0x000000)
    white = Color(0xffffff)
