# coding: utf-8
from typing import NamedTuple, Any

from limagui.paramtree.tree.ioadapter import create_io_class

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/01/2017"

import time
from collections import deque
from threading import Event

from Lima import Core

from .limaionames import LimaTriggers, CtVideo, CtControl, LimaImages
from ..type.limaimage import lima_processlib_data_to_image
from limagui.paramtree import IoAdapterBase, ioadapterdef, Runner


@ioadapterdef(LimaTriggers.ACTIONTRIGGERED,
            triggers=[CtControl.STARTACQ,
                        CtControl.STOPACQ,
                        CtControl.PREPAREACQ,
                        CtControl.ABORTACQ,
                        CtControl.RESET,
                        CtVideo.STARTLIVE,
                        CtVideo.STOPLIVE],
            hidden=True)
class LimaActionTriggered(IoAdapterBase):
    def __init__(self, *args, **kwargs):
        self._last_command = None
        kwargs['io_getter'] = lambda src: self._last_command
        super().__init__(*args, **kwargs)
    
    def _on_trigger(self, sender=None, **kwargs):
        self._last_command = sender
        super()._on_trigger(sender=sender, **kwargs)
                    

@ioadapterdef(io_name=LimaTriggers.REFRESH_TRIGGER,
              triggers=[LimaTriggers.ACQSTATUS_CHANGE,
                        LimaTriggers.ACTIONTRIGGERED],
                        hidden=True)
class LimaRefreshTrigger(IoAdapterBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._last_status = None

    def _on_trigger(self, sender=None, data=None, **kwargs):
        trig = True
        if sender == LimaTriggers.ACQSTATUS_CHANGE:
            status = data[0]
            if status == self._last_status:
                trig = False
            else:
                self._last_status = status

        if trig:
            return super()._on_trigger(sender=sender, data=data, **kwargs)


class ImageStatusCallback(Core.CtControl.ImageStatusCallback):
    DefaultMaxEventRate = 25

    def __init__(self, callback):
        Core.CtControl.ImageStatusCallback.__init__(self)
        self._callback = callback

    def imageStatusChanged(self, image_status):
        self._callback(image_status)


@ioadapterdef(LimaTriggers.STATUS_CALLBACK,
              hidden=True)
class LimaStatusCallback(IoAdapterBase):
    def __init__(self, *args, **kwargs):
        # self._substitute = None
        super().__init__(*args, **kwargs)
        self._callback = None
        self._last_counter = None
        self._cb_data = None
        # self._last_status = None
        self._last_start_time = None
        self._start_timestamp = None

    def _on_start(self):
        if not self._substitute:
            ctcontrol = self.source()
            self._callback = ImageStatusCallback(self._on_img_status)
            ctcontrol.registerImageStatusCallback(self._callback)
        return super()._on_start()

    def _on_stop(self):
        if self._callback:
            ctcontrol = self.source()
            ctcontrol.unregisterImageStatusCallback(self._callback)
            self._callback = None
        super()._on_stop()

    def _on_img_status(self, image_status):
        self.set_data(image_status, _internal=True)


@ioadapterdef(LimaTriggers.IMAGEREADY,
              triggers=[LimaTriggers.STATUS_CALLBACK,
                        LimaTriggers.ACQSTATUS_CHANGE],
              hidden=True)
class LimaTriggersImageReady(IoAdapterBase):
    def __init__(self, *args, **kwargs):
        # self._substitute = None
        super().__init__(*args, **kwargs)    
        self._last_counter = -1
        self._last_status = None
        self._last_start_time = None
        self._last_timestamp = None

    def _on_trigger(self, sender=None, data=None, **kwargs):
        if data is None:
            return
        status = self._last_status
        timestamp = self._last_timestamp
        counter = self._last_counter
        new_img = False
        if sender == LimaTriggers.ACQSTATUS_CHANGE:
            new_status = data[0]
            if new_status != status:
                new_img = True
                status = new_status
            # if status == Core.AcqRunning:
                timestamp = data[1]
        else:
            counter = data.LastImageReady

        new_img |= (counter >= 0 \
                    and (counter != self._last_counter or \
                         timestamp != self._last_timestamp))

        self._last_timestamp = timestamp
        self._last_counter = counter
        self._last_status = status

        if new_img:
            if sender == LimaTriggers.ACQSTATUS_CHANGE:
                self.set_data((self._last_timestamp, -1, self._last_status))
            else:
                self.set_data((self._last_timestamp, self._last_counter, self._last_status))


class LastImage(NamedTuple):
    status: Any = None
    counter: int = -1
    image: Any = None
    # current_run: bool = False
    start_time: float = -10000


@ioadapterdef(LimaImages.LASTIMAGE,
              sink=True,
              data_type=LastImage,
              triggers=[LimaTriggers.IMAGEREADY,
                        LimaTriggers.IMG_POLL_DELAY],
              hidden=True)
class LimaLastImage(IoAdapterBase):
    def __init__(self, *args, **kwargs):
        self._queue = deque(maxlen=1)
        self._image_data = LastImage()
        self._last_status = None
        self._event = Event()
        self._img_poll_s = 0.05
        super().__init__(*args, **kwargs)

    def _on_start(self):
        self._runner = Runner(self._get_image)
        self._run_loop = True
        self._runner.start()
        return super()._on_start()

    def _on_stop(self):
        self._run_loop = False
        self._event.set()
        self._runner.wait()
        self._runner = None
        return super()._on_stop()

    def _read_image(self, counter=None):
        image = self.source().ReadImage(counter)
        image = lima_processlib_data_to_image(image)
        return image

    def _get_image(self):
        while self._run_loop:
            try:
                sender, data = self._queue.popleft()
            except IndexError:
                time.sleep(self._img_poll_s)
                self._event.wait()
                self._event.clear()
                continue

            if data is None:
                continue

            status = self._image_data.status
            image = self._image_data.image
            counter = self._image_data.counter
            timestamp = self._image_data.start_time

            new_timestamp, last_counter, status  = data

            if last_counter >= 0:
                # - counter changed
                # - same counter but timestamp changed: different run
                # In both cases: get last image
                if last_counter != counter or new_timestamp != timestamp:
                    try:
                        if self._substitute:
                            image = self._substitute_io._read_image(counter=last_counter)
                        else:
                            image = self._read_image(counter=last_counter)
                    except Core.Exception as ex:
                        print(f'WARNING in {self.__class__}: {ex}')
                        continue
                    counter = last_counter

            self._image_data = LastImage(status=status,
                                         counter=counter,
                                         image=image,
                                         start_time=new_timestamp)
            self.set_data(self._image_data, _internal=True)
        
    def _on_trigger(self, sender=None, data=None, **kwargs):
        if sender == LimaTriggers.IMG_POLL_DELAY:
            self._img_poll_s = data / 1000.
        else:
            self._queue.append([sender, data])
            self._event.set()


LimaImgPollDelay = create_io_class(LimaTriggers.IMG_POLL_DELAY,
                                   data_type=int,
                                   default_value=100)


@ioadapterdef(LimaTriggers.ACQSTATUS_CHANGE,
                    triggers=CtControl.STATUS,
                    hidden=True)
class LimaAcqStatusChange(IoAdapterBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._last_status = [None, time.time()]

    def _on_start(self):
        super()._on_start()
        self.set_data(self._last_status, _internal=True)

    def _on_trigger(self, sender=None, data=None, **kwargs):
        if data.AcquisitionStatus != self._last_status[0]:
            if data.AcquisitionStatus == Core.AcqRunning:
                evt_time = time.time()
            else:
                evt_time = self._last_status[1]
            # else:
            #     evt_time = time.time()
            self._last_status = data.AcquisitionStatus, evt_time
            self.set_data(self._last_status, _internal=True)

