# coding: utf-8
from __future__ import absolute_import

from limagui.paramtree.tree.ioadapter import IoAdapter

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from bliss.common import event
from bliss.common.axis import Axis
from bliss.config.static import get_config

from ..blissioadapter import BlissIoAdapter
from limagui.paramtree import IoAdapterBase, create_io_class


def get_bliss_object(object_name):
    config = get_config()
    obj = config.get(object_name)
    return obj

def get_bliss_axis_names(session_name):
    config = get_config()
    session = config.get(session_name)
    axis_list = [name for name in session.object_names
                 if isinstance(config.get(name), (Axis,))]
    return axis_list
            

def create_bliss_axis_ios(io_group, session_name, attribute_names=None):
    axis_list = get_bliss_axis_names(session_name)
    ios = {axis_name: create_bliss_axis_io(io_group, session_name, axis_name, attribute_names=attribute_names)
           for axis_name in axis_list}
    return ios


def create_bliss_axis_io(io_group, session_name, axis_name, attribute_names=None):
    if not attribute_names:
        attribute_names = ['position', 'state', 'velocity']
    elif not isinstance(attribute_names, (list, tuple,)):
        attribute_names = [attribute_names] 
    n_attributes = len(attribute_names)

    if not io_group.has_source(axis_name):
        axis = get_bliss_object(axis_name)
        io_group.add_source(axis_name, axis)
    else:
        axis = io_group.source(axis_name)

    io_adapters = {attr_name: BlissIoAdapter(data_type=float,
                                             bliss_chan_name=attr_name,
                                             source=axis,
                                             start=True)
                   for attr_name in attribute_names}
    if len(attribute_names) == 1:
        return io_adapters[attribute_names[0]]
    else:
        return io_adapters