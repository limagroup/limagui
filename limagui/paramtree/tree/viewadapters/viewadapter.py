# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from weakref import proxy

from ...utils.utils import DefaultODict


def ViewAdapter(data_type, cell, data=None, **kwargs):
    return view_adapter_for_type(data_type)(cell,
                                            data=data,
                                            data_type=data_type,
                                            **kwargs)


PARAM_NAMES = set(['view_data',
                       'text',
                       'color',
                       'editable',
                       'tooltip',
                       'decoration',
                       'background_color',
                       'foreground_color',
                       'selectable',
                       'drag_enabled',
                       'drop_enabled',
                       'enabled',
                       'checked',
                       'checkable'])


def setter_notify(func):
    if func.__name__.find('set_') != 0:
        raise RuntimeError()
    prop_name = func.__name__[4:]
    if prop_name not in PARAM_NAMES:
        raise RuntimeError(f'{prop_name} not allowed: {PARAM_NAMES}')
    def wrapper(self, value, _notify=True):
        retval = func(self, value)
        if _notify:
            self._cell._view_adapter_changed(**{prop_name:retval})
        return retval
    return wrapper


class ViewAdapterBase(object):
    def __init__(self,
                 cell,
                 data=None,
                 data_type=None,
                 **kwargs):
        self._cell = proxy(cell)
        if data_type is not None:
            self._adapter_type = data_type
        if data_type is None:
            self._adapter_type = data.__class__
        self._view_config = None

        self._text = ''
        self._view_data = None
        self._color = None
        self._editable = True
        self._tooltip = None
        self._decoration = None
        self._background_color = None
        self._foreground_color = None
        self._selectable = True
        self._drag_enabled = None
        self._drop_enabled = None
        self._enabled = True
        self._checked = False
        self._checkable = False

        # self._params = {key:None for key in self.PARAM_NAMES}
        self.set_params(**kwargs, _notify=False)

    def set_params(self, _notify=True, **kwargs):
        keys = set(kwargs) & PARAM_NAMES
        for key in keys:
            getattr(self, f'set_{key}')(kwargs[key], _notify=_notify)
            # self._params[key] = kwargs[key]

    # def kwargs(self):
    #     return {'editable': self._editable}

    def get_params(self, name=None):
        if name:
            return getattr(self, name)
        else:
            return {name: getattr(self, name)
            for name in PARAM_NAMES}

    def icon(self):
        return self.cell().view_config().get('icon')

    def view_config(self):
        if self._view_config is None:
            self._view_config = self.cell().view_config()
        return self._view_config

    def cell(self):
        return self._cell

    def _set_adapter_type(self, adapter_type):
        """
        Sets the view type.
        """
        self._adapter_type = adapter_type

    def adapter_type(self):
        """
        Type of data returned by this view.
        Default is the same type as the one handled by the cell.
        """
        return self._adapter_type

    def set_model_to_view(self, model_data):
        """
        Called by the cell after the model data
        has been updated.
        Default behaviour just calls to_text and to_color
        from to_text.
        """
        self._view_data = self.model_to_view_data(model_data)
        self._text = self.to_text(model_data)
        self._color = self.to_color(model_data)
        return self._view_data

    def set_view_to_model(self, view_data):
        """
        Commits editor data to the model.
        """
        model_data = self.view_to_model_data(view_data)
        self.cell().set_data(model_data)

    def model_to_view_data(self, data):
        """
        Converts data from the model to something.
        accepted by the editor.
        """
        return data

    def view_to_model_data(self, data):
        """
        Converts data from the editor to something.
        accepted by the model.
        """
        return data

    def to_text(self, model_data):
        """
        Returns data converted to text.
        Default behaviour returns str(data).
        """
        if model_data is None:
            return ''
        return str(model_data)

    def to_color(self, model_data):
        return None

    def size(self):
        """
        Size of the view (width, height) or None
        """
        return None

    def read_only(self):
        return self.cell().read_only()

    def write_only(self):
        return self.cell().write_only()

    def view_config(self):
        return self.cell().io_adapter().view_config()

    @property
    def view_data(self):
        if self._view_data is None and not self.cell().write_only():
            data = self.cell().data()
            self.set_model_to_view(data)
        return self._view_data

    @property
    def text(self):
        """
        Text displayed in the view.
        """
        return self._text

    @property
    def color(self):
        # print('color')
        return self._color

    @property
    def tooltip(self):
        # print('tooltip')
        return self._tooltip

    @property
    def background_color(self):
        return self._background_color
 
    @property
    def decoration(self):
        return self._decoration
    
    @property
    def foreground_color(self):
        return self._foreground_color

    @property
    def selectable(self):
        # print('sele')
        return self._selectable

    @property 
    def drag_enabled(self):
        return self._drag_enabled
    
    @property
    def drop_enabled(self):
        return self._drop_enabled
    
    @property
    def enabled(self):
        # print('enab')
        return self._enabled
    
    @property
    def editable(self):
        # print('edit')
        return self._editable and not self.write_only()
    
    @property
    def checked(self):
        return self._checked
    
    @property
    def checkable(self):
        return self._checkable

    @setter_notify
    def set_color(self, color):
        self._color = color
        return self._colof

    @setter_notify
    def set_background_color(self, background_color):
        self._background_color = background_color
        return self._background_color
 
    @setter_notify
    def set_decoration(self, decoration):
        self._decoration = decoration
        return self._decoration
    
    @setter_notify
    def set_foreground_color(self, foreground_color):
        self._foreground_color = foreground_color
        return self._foreground_color

    @setter_notify
    def set_selectable(self, selectable):
        self._selectable = selectable in (True, None)
        return self._selectable

    @setter_notify
    def set_drag_enabled(self, drag_enabled):
        self._drag_enabled = drag_enabled
        return self._drag_enabled
    
    @setter_notify
    def set_drop_enabled(self, drop_enabled):
        self._drop_enabled = drop_enabled
        return self._drop_enabled
    
    # @enabled.setter
    @setter_notify
    def set_enabled(self, enabled):
        self._enabled = enabled in (True, None)
        return self._enabled
    
    @setter_notify
    def set_editable(self, editable):
        self._editable = editable in (True, None)
        return self._editable
    
    @setter_notify
    def set_checked(self, checked):
        self._checked = checked is True and self._checkable
        return self._checked
    
    @setter_notify
    def set_checkable(self, checkable):
        self._checkable = checkable is True
        return self._checkable

    @setter_notify
    def set_tooltip(self, tooltip):
        self._tooltip = tooltip
        return self._tooltip


_registered_adapters = DefaultODict(ViewAdapterBase)


def register_view_adapter(data_type, adapter_class):
    global _registered_adapters

    if data_type not in _registered_adapters:
        _registered_adapters[data_type] = adapter_class
    else:
        raise ValueError('A type adapter has already been registered '
                         'for data_type [{0}] '.format(data_type))


def view_adapter_for_type(data_type):
    if data_type is None:
        return ViewAdapterBase

    found_klass = None
    found_type = None
    for key, value in _registered_adapters.items():
        if issubclass(data_type, (key,)):
            if found_type:
                if issubclass(key, (found_type,)):
                    found_type = key
                    found_klass = value
            else:
                found_type = key
                found_klass = value

    return found_klass or ViewAdapterBase


def viewadapterdef(data_type):
    def inner(cls):
        register_view_adapter(data_type, cls)
        return cls
    return inner
