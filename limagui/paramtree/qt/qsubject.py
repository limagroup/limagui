# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from ..utils.subject import SubjectBase, add_subject


class QSubject(SubjectBase):
    def __init__(self, **kwargs):
        super(QSubject, self).__init__(**kwargs)

        signal = kwargs.get('signal')
        if signal is not None:
            signal.connect(self.notify)


add_subject('QSubject', QSubject)
