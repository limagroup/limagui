# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from ....tree.types.action import Action, Actions

from ..editor import QEditorMixin, qeditordef
from ..widgets.widgets import QCellActionWidget

@qeditordef(data_type=Actions)
@qeditordef(data_type=Action)
class ActionEditor(QEditorMixin, QCellActionWidget):
    persistent = True
