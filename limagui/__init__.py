# coding: utf-8
from __future__ import absolute_import, print_function, division
# import faulthandler; faulthandler.enable()
__authors__ = ["Jérôme Kieffer"]
__license__ = ""
__date__ = "28/01/2016"

import os
project = os.path.basename(os.path.dirname(os.path.abspath(__file__)))
# try:
#     from ._version import __date__ as date  # noqa
#     from ._version import version, version_info, hexversion, strictversion
#     # noqa
# except ImportError:
#     raise RuntimeError("Do NOT use %s from its sources: "
#                        "build it and use the built version" % project)
