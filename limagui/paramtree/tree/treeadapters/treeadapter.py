
# coding: utf-8
from __future__ import absolute_import
import inspect

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


import sys
import inspect
import traceback
from weakref import ref
from collections import namedtuple
from contextlib import contextmanager

from silx.utils.weakref import WeakMethod

from ..cell import Cell
from ...utils.utils import DefaultODict


def _deref(obj):
    return obj and obj()


def TreeAdapter(data_type, node, data, **kwargs):
    return tree_adapter_for_type(data_type)(node, data,
                                            data_type=data_type, **kwargs)


NodeError = namedtuple('NodeError',
                       ['node_path',
                        'caller_lineno',
                        'caller_fn',
                        'exception',
                        'trace'])


class TreeAdapterBase(object):

    def __init__(self, node, data, data_type=None, editable=True):
        self._node = node
        self._parent = None

        self._data_type = data_type
        self._is_init = 0

        def missing_constructor(key):
            cell = Cell(self, editable=False)
            cell._set_column(key)
            return cell

        self._cells = DefaultODict(None,
                                   missing_constructor=missing_constructor)
        self._children = None
        self._children_lock = 0

        # todo : document signature
        # function to be called when a change occurs in a cell
        self._notify_change_fn = None

        # misc. functions to be called before before
        # insertion/deletion of children
        self._notify_pre_insert_fn = None
        self._notify_post_insert_fn = None
        self._notify_pre_remove_fn = None
        self._notify_post_remove_fn = None

        # function called when an exception is raised in a cell
        self._notify_exception_fn = None

        cells_data, children_data = self._sort_data(data)

        self._children_data = children_data

        for cell_data in cells_data:
            if 'editable' not in cell_data:
                cell_data['editable'] = editable
        self._cells_data = cells_data

    def node(self):
        return self._node

    @contextmanager
    def _lock_children(self):
        self._children_lock += 1
        try:
            yield
        except Exception as ex:
            print(self.__class__, ex)
        finally:
            self._children_lock -= 1

    def _set_notify_change(self, change):
        """
        Method called when a change occurs in a cell
        """
        # TODO : find why WeakMethod doesnt work anymore
        if change:
            self._notify_change_fn = change and WeakMethod(change)
        for child in self._get_children(init=False):
            child._set_notify_change(change)

    def _set_notify_pre_insert(self, pre_insert):
        """
        Method called prior to inserting a child
        """
        # TODO : find why WeakMethod doesnt work anymore
        if pre_insert:
            self._notify_pre_insert_fn = pre_insert and WeakMethod(pre_insert)
        for child in self._get_children(init=False):
            child._set_notify_pre_insert(pre_insert)

    def _set_notify_post_insert(self, post_insert):
        """
        Method called after inserting a child
        """
        # TODO : find why WeakMethod doesnt work anymore
        if post_insert:
            self._notify_post_insert_fn = post_insert and WeakMethod(post_insert)
        for child in self._get_children(init=False):
            child._set_notify_post_insert(post_insert)

    def _set_notify_pre_remove(self, pre_remove):
        """1
        Method called prior to removing a child
        """
        # TODO : find why WeakMethod doesnt work anymore
        if pre_remove:
            self._notify_pre_remove_fn = pre_remove and WeakMethod(pre_remove)
        for child in self._get_children(init=False):
            child._set_notify_pre_remove(pre_remove)

    def _set_notify_post_remove(self, post_remove):
        """
        Method called after removing a child
        """
        # TODO : find why WeakMethod doesnt work anymore
        if post_remove:
            self._notify_post_remove_fn = post_remove and WeakMethod(post_remove)
        for child in self._get_children(init=False):
            child._set_notify_post_remove(post_remove)

    def _set_notify_exception(self, notify_exception):
        """
        Method called to notify (something, like the QModel)
        of an exception in a cell
        """
        if notify_exception:
            self._notify_exception_fn = (notify_exception
                                       and WeakMethod(notify_exception))
        for child in self._get_children(init=False):
            child._set_notify_exception(notify_exception)

    def _notify_change(self):
        return _deref(self._notify_change_fn)

    def _notify_pre_insert(self):
        return _deref(self._notify_pre_insert_fn)

    def _notify_post_insert(self):
        return _deref(self._notify_post_insert_fn)

    def _notify_pre_remove(self):
        return _deref(self._notify_pre_remove_fn)

    def _notify_post_remove(self):
        return _deref(self._notify_post_remove_fn)

    def _notify_exception(self):
        return _deref(self._notify_exception_fn)

    def _set_exception(self, exception):
        raise
        previous_frame = inspect.currentframe().f_back
        (filename, line_number,
         function_name, lines, index) = inspect.getframeinfo(previous_frame)

        ex_type, ex, trace = sys.exc_info()

        if trace is not None:
            trace = traceback.format_exc(trace).split('\n')

        node_error = NodeError(node_path=self.path(),
                               caller_lineno=line_number,
                               caller_fn=function_name,
                               exception=exception,
                               trace=trace)

        notify_error = self._notify_exception()

        if notify_error:
            notify_error(node_error)
        else:
            print('DEFAULT', node_error)

    @contextmanager
    def _insert_context(self, first, last, args):
        """
        Context entered when inserting a child.
        Calls pre and post insert methods:
        """
        notify_pre = self._notify_pre_insert()
        if notify_pre:
            notify_pre(self.node(), first, last, args)

        try:
            yield
        except Exception as ex:
            print(ex)
            self._set_exception(ex)

        notify_post = self._notify_post_insert()
        if notify_post:
            notify_post(self.node(), first, last, args)

    @contextmanager
    def _remove_context(self, first, last, args):
        """
        Context entered when removing a child.
        Calls pre and post remove methods:
        """
        notify_pre = self._notify_pre_remove()
        if notify_pre:
            notify_pre(self.node(), first, last, args)
        try:
            yield
        except Exception as ex:
            print(self.__class__, ex)
            self._set_exception(ex)
        notify_post = self._notify_post_remove()
        if notify_post:
            notify_post(self.node(), first, last, args)

    def children(self):
        """
        Returns the children of this tree.
        """
        return tuple(self._get_children())

    def insert_child(self, child, row=-1, notify_args=None):
        """
        Insert a child at the given row (relative to this node).
        notify_args are passed to the insert context.
        """
        children = self._get_children()
        if row == -1:
            row = len(children)

        with self._insert_context(row, row, notify_args):
            children.insert(row, child)
        child._set_parent(self)

    def append_child(self, child):
        """
        Append child to this node's children.
        """
        self.insert_child(child, -1)

    def remove_child(self, row, notify_args=None):
        """
        Insert a child at the given row (relative to this node).
        notify_args are passed to the remove context.
        """
        children = self._get_children()
        child = children[row]
        child._set_parent(None)
        child.stop_node()
        with self._remove_context(row, row, notify_args):
            child = children.pop(row)
            
    def remove_all(self, notify_args=None):
        children = self._get_children()
        if not children:
            return
        with self._remove_context(0, len(children), notify_args):
            while children:
                child = children[-1]
                child._set_parent(None)
                child.stop_node()
                child = children.pop()

    def _set_parent(self, parent):
        """
        Set this node's parent.
        """
        if self is parent:
            raise ValueError('Node and parent are the same.')

        # for cell in self.__cells.values():
        #     cell._clear_cache()
        if parent is not None:
            self._parent = ref(parent)
            self._set_notify_pre_insert(parent._notify_pre_insert())
            self._set_notify_post_insert(parent._notify_post_insert())
            self._set_notify_pre_remove(parent._notify_pre_remove())
            self._set_notify_post_remove(parent._notify_post_remove())
            self._set_notify_change(parent._notify_change())
            self._set_notify_exception(parent._notify_exception())
            if parent.node().is_started():
                self.node().start_node()
        else:
            self._parent = None
            self._set_notify_pre_insert(None)
            self._set_notify_post_insert(None)
            self._set_notify_pre_remove(None)
            self._set_notify_post_remove(None)
            self._set_notify_change(None)
            self._set_notify_exception(None)
            self.node().stop_node()

    def parent_node(self):
        """
        Returns this tree's parent node.
        """
        return self._parent and self._parent().node()

    def parent(self):
        """
        Returns this tree's parent tree.
        """
        return self._parent and self._parent()

    def start(self):
        for cell in self._cells.values():
            cell.start()

    def stop(self):
        for cell in self._cells.values():
            cell.stop()
    
    def row(self):
        parent = self.parent()
        if parent:
            try:
                return parent.index_of(self)
            except ValueError:
                return -1
        return -1

    def cell(self, column):
        """
        Returns the cell at column.
        """
        try:
            return self._get_cells()[column]
        except IndexError:
            return Cell(self)

    def child(self, row):
        """
        Returns the child at <row>.
        """
        children = self._get_children()
        try:
            return children[row]
        except IndexError:
            return None

    def has_children(self):
        return self.child_count() > 0

    def child_count(self):
        """
        Returns the number of children.
        """
        return len(self._get_children(init=True))

    def cell_count(self):
        """
        Number of columns (cells).
        """
        cells = self._get_cells()
        return len(cells)

    def _get_children(self, init=True):
        if self._children_data is not None:
            if init:
                self._children = self._init_children(self._children_data)
                self._children_data = None
                for child in self._children:
                    child._set_parent(self)
            else:
                return []
        return self._children

    def _get_cells(self, init=True):
        if self._cells_data is not None:
            if init:
                if self._is_init == 1:
                    """
                    Already creating the cells
                    """
                    return []
                self._is_init = 1
                cells = self._init_cells(self._cells_data)
                started = self.node().is_started()
                for i_cell, cell in enumerate(cells):
                    self._cells[i_cell] = cell
                    cell._set_column(i_cell)
                    if started:
                        cell.start()
                self._cells_data = None
            else:
                return []
            for cell in self._cells.values():
                cell.set_parent(self)
        self._is_init = 0
        return self._cells

    def view_size(self, column):
        return self.cell(column).view_size()

    ##########################################
    ####                                  ####
    ##########################################

    def index_of(self, child):
        return self._get_children().index(child.node())
        
    def background_color(self, column):
        return self.cell(column).background_color()
    
    def tooltip(self, column):
        return self.cell(column).tooltip()
    
    def decoration(self, column):
        return self.cell(column).decoration()
    
    def foreground_color(self, column):
        return self.cell(column).foreground_color()

    def selectable(self, column):
        return self.cell(column).selectable()

    def drag_enabled(self, column):
        return self.cell(column).drag_enabled()
    
    def drop_enabled(self, column):
        return self.cell(column).drop_enabled()
    
    def enabled(self, column):
        return self.cell(column).enabled()
    
    def editable(self, column):
        return self.cell(column).editable()
    
    def checked(self, column):
        return self.cell(column).checked()
    
    def checkable(self, column):
        return self.cell(column).checkable()

    def set_background_color(self, column, background_color):
        return self.cell(column).set_background_color(background_color)

    def set_tooltip(self, column, tooltip):
        return self.cell(column).set_tooltip(tooltip)

    def set_decoration(self, column, decoration):
        return self.cell(column).set_decoration(decoration)

    def set_foreground_color(self, column, foreground_color):
        return self.cell(column).set_foreground_color(foreground_color)

    def set_drag_enabled(self, column, drag_enabled):
        return self.cell(column).set_drag_enabled(drag_enabled)

    def set_drop_enabled(self, column, drop_enabled):
        return self.cell(column).set_drop_enabled(drop_enabled)

    def set_enabled(self, column, enabled):
        self.cell(column).set_enabled(enabled)

    def set_editable(self, column, editable):
        return self.cell(column).set_editable(editable)

    def set_checked(self, column, checked):
        return self.cell(column).set_checked(checked)

    def set_checkable(self, column, checkable):
        return self.cell(column).set_checkable(checkable)

    def set_selectable(self, column, selectable):
        return self.cell(column).set_selectable(selectable)

    def view_adapter(self, column):
        return self.cell(column).view_adapter()

    def view_adapter_type(self, column):
        return self.view_adapter(column).adapter_type()

    def data_type(self, column):
        return self.cell(column).data_type()

    def text(self, column):
        return self.cell(column).text()

    def editor_data(self, column):
        return self.cell(column).view_data()

    def set_editor_data(self, column, data):
        return self.cell(column).set_view_data(data)

    def column(self, cell):
        return self._cells.index(cell)

    def set_on_change_callback(self, column, callback):
        self.cell(column).set_on_change_callback(callback)

    ##########################################
    ####                                  ####
    ##########################################

    def _sort_data(self, data):
        """
        Allows to sort between cell and tree data.
        Returns a list of dictionaries passed to the cells constructors,
            and a list of dictionaries the Node constructors.
        Those dictionaries are passed as kwargs to the constructors.
        Default behaviour: return data, None
        """
        if data is None:
            cell_data = [{'data':self.node().name(), 'editable':False}]
        elif data.__class__ in (list, tuple):
            cell_data = [{'data':self.node().name(), 'editable':False}]
            cell_data += [{'data': value}
                         for value in data]
        else:
            cell_data = [{'data':self.node().name(), 'editable':False},
                         {'data': data}]
        return cell_data, []

    def _init_children(self, children_data):
        """
        Initialize the children. Called only once.
        Returns a list of Node objects.
        children_data is a list, one element per Node.
        """
        # TODO : solve cyclic imports better
        from ..node import Node
        if children_data is not None:
            return [Node(**data) for data in children_data]
        return []

    def _init_cells(self, cell_data):
        """
        Initialize cells. Called only once.
        Returns a list of Cell objects.
        cell_data is a list, one element per cell.
        """
        cells = []
        if cell_data is not None:
            if len(cell_data) > 0:
                cells = [Cell(None, **data) for data in cell_data]
            else:
                cells= [Cell(None,
                             data=self.node().name(),
                             editable=False)]
            # for cell in cells:
            #     cell.data()
        return cells

    def _data_changed(self, cell, **kwargs):
        parent_tree = self.parent()
        if parent_tree:
            with self._lock_children():
                if self._children_lock <= 1:
                    parent_tree._children_changed(self.node(), cell, **kwargs)
        column = cell.column()
        notify_change = self._notify_change()
        if notify_change:
            kwargs_keys = [key for key, value in kwargs.items() if value is not None]
            notify_change(self.node(), column, kwargs_keys)

    def _children_changed(self, node, cell, **kwargs):
        return True
        

_registered_adapters = DefaultODict(TreeAdapterBase)


def register_tree_adapter(data_type, adapter_class):
    global _registered_adapters

    if data_type not in _registered_adapters:
        _registered_adapters[data_type] = adapter_class
    else:
        raise ValueError('A tree adapter has already been registered '
                         'for data_type [{0}] '.format(data_type))


def tree_adapter_for_type(data_type):
    if data_type is None:
        return TreeAdapterBase

    found_klass = None
    found_type = None
    for key, value in _registered_adapters.items():
        if issubclass(data_type, (key,)):
            if found_type:
                if issubclass(key, (found_type,)):
                    found_type = key
                    found_klass = value
            else:
                found_type = key
                found_klass = value

    return found_klass or TreeAdapterBase


def treeadapterdef(data_type):
    def inner(cls):
        register_tree_adapter(data_type, cls)
        return cls
    return inner
