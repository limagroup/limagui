# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


class InvalidModelIndexError(Exception):
    pass


class EmptyModelIndexError(Exception):
    pass


class QRoleError(Exception):
    pass


class ModelIndexSelectionError(Exception):
    pass
