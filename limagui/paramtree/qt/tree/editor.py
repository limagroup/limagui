# coding: utf-8

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from ...utils.utils import DefaultODict

from .. import Qt


class QEditorMixin:
    persistent = False

    def __init__(self, parent, option, index):
        super(QEditorMixin, self).__init__(data=index, parent=parent)

        self._callback = None
        self._committedData = None
        self._index = Qt.QPersistentModelIndex(index)
        # self.setAutoFillBackground(True)

    def _cell_refresh(self):
        pass

    def _setCommitCallback(self, callback):
        self._callback = callback

    def _persistentIndex(self):
        return self._index

    @classmethod
    def paintDelegate(cls, painter, option, index):
        return False

    def setEditorData(self, index):
        flags = index.flags()
        enabled = True if flags & Qt.Qt.ItemIsEnabled else False
        try:
            self._cell_set_enabled(enabled)
        except AttributeError:
            pass
        try:
            editor_data = index.data(Qt.Qt.EditRole)
            if editor_data is not None:
                self._cell_set_view_data(editor_data)
        except NotImplementedError:
            return False
        return True

    def editorIsPersistent(self):
        return self.persistent

    def sizeHint(self):
        size = self._index.data(Qt.Qt.SizeHintRole)
        if size is None:
            return Qt.QSize()
        return Qt.QSize(*size)

    def _cell_editor_commit(self, data):
        self._committedData = data
        self._callback(self)

    @classmethod
    def _isPersistent(cls):
        return cls.persistent

    def editorData(self):
        return self._committedData


# ====================================================================
# ====================================================================


_registered_qeditors = DefaultODict(None)


def register_qeditor(data_type, editor_class):
    global _registered_qeditors

    if data_type not in _registered_qeditors:
        _registered_qeditors[data_type] = editor_class
    else:
        raise ValueError('A QEditor has already been registered '
                         'for data_type [{0}] '.format(data_type))


# TODO : allow registering node types instead of data types
def qeditordef(data_type,
               **kwargs):
    def inner(cls):
        register_qeditor(data_type, cls)
        return cls

    return inner


def qeditor_for_type(data_type):
    if data_type is None:
        return None

    found_klass = None
    found_type = None
    for key, value in _registered_qeditors.items():
        if issubclass(data_type, (key,)):
            if found_type:
                if issubclass(key, (found_type,)):
                    found_type = key
                    found_klass = value
            else:
                found_type = key
                found_klass = value

    return found_klass