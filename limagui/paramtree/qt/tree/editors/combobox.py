# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"

import enum

from ... import Qt

from ..widgets.widgets import QCellComboBox
from ..editor import QEditorMixin, qeditordef


@qeditordef(data_type=enum.Enum)
@qeditordef(data_type=enum.EnumMeta)
class ComboBox(QEditorMixin, QCellComboBox):
    def __init__(self, *args, **kwargs):
        super(ComboBox, self).__init__(*args, **kwargs)
        try:
            self.activated.disconnect()
        except:
            pass
        
    def setEditorData(self, index):
        current = self._persistentIndex().data(Qt.Qt.EditRole)
        self._cell_set_view_data(current)
        return True

    def setEditable(self, editable):
        """
        Prevent the user from entering custom values.
        """
        super(ComboBox, self).setEditable(False)
