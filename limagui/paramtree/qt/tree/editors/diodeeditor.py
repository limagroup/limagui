# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"

from ... import Qt

from ..editor import QEditorMixin, qeditordef
from ....tree.types.color import Color
from ..widgets.qshapes import paintDiode


@qeditordef(data_type=Color)
class DiodeEditor(QEditorMixin, Qt.QWidget):

    @classmethod
    def paintDelegate(cls, painter, option, index):
        color = index.data(Qt.Qt.EditRole)

        paintDiode(Qt.QColor(color),
                   option.rect,
                   painter,
                   alignment=Qt.Qt.AlignLeft)

        return True

    # def sizeHint(self):
    #     return Qt.QSize(32, 32)
