# # -*- coding: utf-8 -*-

# from __future__ import absolute_import

# __authors__ = ["D. Naudet"]
# __license__ = ""
# __date__ = "01/10/2018"


# from Lima import Simulator

# from limagui.paramtree import (Node,
#                                ioadapterdef,
#                                IoAdapterBase,
#                                treeadapterdef,
#                                TreeAdapterBase)

# from limagui.gui.qt.paramtree.iobaseioadapter import IoBaseIoAdapter

# from ...lima.lima_io.camera import simulator


# _SX_NODE_NAME = 'sx'
# _SY_NODE_NAME = 'sy'


# _PX_NODE_NAME = 'px'
# _PY_NODE_NAME = 'py'


# # @treeadapterdef(tuple)
# # class TupleTreeAdapter(TreeAdapterBase):
# #     def __init__(self, node, data, node_names=None, **kwargs):
# #         super(TupleTreeAdapter, self).__init__(node, data, node_names=None, **kwargs)
# #         if isinstance(data, (tuple, list,)):
# #             self._data_len = len(data)
# #         else:
# #             self._data_len = None
# #         self._node_names = node_names

# #     def child_count(self):
# #         if self._data_len is not None:
# #             return self._data_len
# #         raise NotImplemented()

# #     def _sort_data(self, data):
# #         if self._node_names is None:
# #             self._node_names = [f'{i}' for i in range(self._data_len)]
# #         elif len(self._node_names) > self._data_len:
# #                 raise RuntimeError(f'In {__file__}: mismatch.')
# #         cell_data = []
            
# #         # cell_data = [{'data':'Bin', 'editable':False},
# #         #              {'data':data, 'editable':False}]
# #         # cell_data = {'data':name} for }]
# #         node_data = [{'name':name, 'data':1} for name in self._node_names]
# #         return cell_data, node_data

#     # def _data_changed(self, cell, **kwargs):
#     #     data = kwargs.get('data')
#     #     if data:
#     #         self.child(0).cell(1).set_data(data.getX())
#     #         self.child(1).cell(1).set_data(data.getY())
#     #     super(TupleTreeAdapter, self)._data_changed(cell, **kwargs)

#     # def _children_changed(self, node, cell, **kwargs):
#     #     data = kwargs.get('data')
#     #     if data is None:
#     #         return
#     #     cell = self.cell(1)
#     #     bin = cell.data()
#     #     if node.row() == 0:
#     #         bin = Bin(data, bin.getY())
#     #     elif node.row() == 1:
#     #         bin = Bin(bin.getX(), data)
#     #     cell.set_data(bin)





# # @treeadapterdef(simulator.LimaSimulatorDiffrationSpeed)
# # class LimaRotSpeedTreeAdapter(TreeAdapterBase):
# #     def __init__(self, node, data):
# #         super(LimaRotSpeedTreeAdapter, self).__init__(node, data)
# #         self.__node_sx = None
# #         self.__node_sy = None

# #     def get_children(self):
# #         data = self.node().model_data(1)
# #         view_init = self.node().view_init_data(1)
# #         child_view_init = {key: view_init.get(key)
# #                            for key in ('min_value', 'max_value')}
# #         self.__node_sx = Node(data[0], _SX_NODE_NAME, notify_parent=True)
# #         self.__node_sy = Node(data[1], _SY_NODE_NAME, notify_parent=True)
# #         self.__node_sx.set_view_init_data(1, child_view_init)
# #         self.__node_sy.set_view_init_data(1, child_view_init)
# #         return [self.__node_sx, self.__node_sy]

# #     def cells(self, node, **kwargs):
# #         kwargs['editable'] = False
# #         return super(LimaRotSpeedTreeAdapter, self).cells(node, **kwargs)

# #     def node_changed(self, column, data):

# #         # has it been initialized yet?
# #         if self.__node_sx is None:
# #             return

# #         model_data = data['model_data']
# #         if model_data is None:
# #             return
# #         self.__node_sx.set_model_data(1, model_data[0])
# #         self.__node_sy.set_model_data(1, model_data[1])

# #     def child_changed(self, child, column, data):
# #         model_data = data.get('model_data')
# #         if model_data is None:
# #             return
# #         name = child.name()
# #         sx, sy = self.node().model_data(1)
# #         if name == _SX_NODE_NAME:
# #             sx = model_data
# #         elif name == _SY_NODE_NAME:
# #             sy = model_data
# #         else:
# #             raise ValueError('Unexpected node name.')
# #         self.node().set_model_data(1, (sx, sy))


# # @treeadapterdef(simulator.LimaSimulatorDiffrationPos)
# # class LimaRotPosTreeAdapter(TreeAdapterBase):
# #     def __init__(self, node, data):
# #         super(LimaRotPosTreeAdapter, self).__init__(node, data)
# #         self.__node_px = None
# #         self.__node_py = None

# #     def get_children(self):
# #         data = self.node().model_data(1)
# #         view_init = self.node().view_init_data(1)
# #         child_view_init = {key: view_init.get(key)
# #                            for key in ('min_value', 'max_value')}

# #         self.__node_px = Node(data[0], _PX_NODE_NAME, notify_parent=True)
# #         self.__node_py = Node(data[1], _PY_NODE_NAME, notify_parent=True)
# #         self.__node_px.set_view_init_data(1, child_view_init)
# #         self.__node_py.set_view_init_data(1, child_view_init)
# #         return [self.__node_px, self.__node_py]

# #     def cells(self, node, **kwargs):
# #         kwargs['editable'] = False
# #         return super(LimaRotPosTreeAdapter, self).cells(node, **kwargs)

# #     def node_changed(self, column, data):
# #         model_data = data['model_data']
# #         if model_data is None:
# #             return

# #         # has it been initialized yet?
# #         if self.__node_px is None:
# #             return

# #         self.__node_px.set_model_data(1, model_data[0])
# #         self.__node_py.set_model_data(1, model_data[1])

# #     def child_changed(self, child, column, data):
# #         model_data = data.get('model_data')
# #         if model_data is None:
# #             return
# #         name = child.name()
# #         px, py = self.node().model_data(1)
# #         if name == _PX_NODE_NAME:
# #             px = model_data
# #         elif name == _PY_NODE_NAME:
# #             py = model_data
# #         else:
# #             raise ValueError('Unexpected node name.')
# #         self.node().set_model_data(1, (px, py))


# # @treeadapterdef(simulator.LimaSimulatorPeakAngles)
# # class LimaPeakAnglesTreeAdapter(TreeAdapterBase):

# #     def get_children(self):
# #         data = self.node().model_data(1)
# #         children = []
# #         for i_peak, peak in enumerate(data):
# #             child = Node(peak,
# #                          name='pk_{0}'.format(i_peak),
# #                          notify_parent=True)
# #             child.set_view_init_data(1, {'min_value': -360.,
# #                                          'max_value': 360.})
# #             children.append(child)
# #         return children

# #     def cells(self, node, **kwargs):
# #         kwargs['editable'] = False
# #         return super(LimaPeakAnglesTreeAdapter, self).cells(node, **kwargs)

# #     def node_changed(self, column, data):
# #         if column != 1:
# #             return
# #         model_data = data['model_data']
# #         if model_data is None:
# #             return
# #         n_peaks = len(model_data)
# #         n_children = self.node().child_count()

# #         # first, remove any extra nodes
# #         while n_peaks < n_children:
# #             self.node().remove_child(-1)
# #             n_children -= 1

# #         # then set the data for the remaining nodes
# #         i_peak = 0
# #         while i_peak < n_children:
# #             self.node().child(i_peak).set_model_data(1, model_data[i_peak])
# #             i_peak += 1

# #         # and add new nodes, if needed
# #         while i_peak < n_peaks:
# #             self.node().insert_node_data(data=model_data[i_peak],
# #                                          name='pk_{0}'.format(n_children),
# #                                          notify_parent=True)
# #             i_peak += 1

# #         for i_peak in range(n_children):
# #             self.node().child(i_peak).set_model_data(1, model_data[i_peak])

# #     def child_changed(self, child, column, data):
# #         model_data = data.get('model_data')
# #         if model_data is None:
# #             return
# #         angles = self.node().model_data(1)
# #         child_row = child.row()
# #         angles[child_row] = model_data
# #         self.node().set_model_data(1, angles)


# # @ioadapterdef(simulator.LimaSimulatorPeaks)
# # class LimaGaussPeakIoAdapter(IoBaseIoAdapter):
# #     def _to_display_data(self, data):
# #         if data:
# #             n_peaks = len(data)
# #         else:
# #             n_peaks = 0
# #         txt = '{0} peak'.format(n_peaks)
# #         if n_peaks > 1:
# #             txt += 's'
# #         return txt

# #     def editable(self):
# #         return False


# # @treeadapterdef(simulator.LimaSimulatorPeaks)
# # class LimaPeaksTreeAdapter(TreeAdapterBase):

# #     def get_children(self):
# #         data = self.node().model_data(1)

# #         children = [Node(peak, name='pk_{0}'.format(i_peak), notify_parent=True)
# #                     for i_peak, peak in enumerate(data)]
# #         return children

# #     def cells(self, node, **kwargs):
# #         kwargs['editable'] = False
# #         return super(LimaPeaksTreeAdapter, self).cells(node, **kwargs)

# #     def node_changed(self, column, data):
# #         if column != 1:
# #             return
# #         model_data = data['model_data']
# #         if model_data is None:
# #             return
# #         n_peaks = len(model_data)
# #         n_children = self.node().child_count()

# #         # first, remove any extra nodes
# #         while n_peaks < n_children:
# #             self.node().remove_child(-1)
# #             n_children -= 1

# #         # then set the data for the remaining nodes
# #         i_peak = 0
# #         while i_peak < n_children:
# #             self.node().child(i_peak).set_model_data(1, model_data[i_peak])
# #             i_peak += 1

# #         # and add new nodes, if needed
# #         while i_peak < n_peaks:
# #             self.node().insert_node_data(data=model_data[i_peak],
# #                                          name='pk {0}'.format(n_children),
# #                                          notify_parent=True)
# #             i_peak += 1

# #         for i_peak in range(n_children):
# #             self.node().child(i_peak).set_model_data(1, model_data[i_peak])

# #     def child_changed(self, child, column, data):
# #         model_data = data.get('model_data')
# #         if model_data is None:
# #             return
# #         peaks = self.node().model_data(1)
# #         child_row = child.row()
# #         peaks[child_row] = model_data
# #         self.node().set_model_data(1, peaks)


# # @ioadapterdef(Simulator.GaussPeak)
# # class LimaGaussPeakIoAdapter(IoAdapterBase):
# #     def _to_display_data(self, data):
# #         txt = 'Gauss({0}, {1}, {2}, {3})'.format(data.x0,
# #                                                  data.y0,
# #                                                  data.fwhm,
# #                                                  data.max)
# #         return txt

# #     def editable(self):
# #         return False


# # @treeadapterdef(Simulator.GaussPeak)
# # class LimaGaussPeakTreeAdapter(TreeAdapterBase):
# #     def __init__(self, node, data):
# #         super(LimaGaussPeakTreeAdapter, self).__init__(node, data)
# #         self.__node_x0 = None
# #         self.__node_y0 = None
# #         self.__node_fwhm = None
# #         self.__node_max = None

# #     def get_children(self):
# #         data = self.node().model_data(1)
# #         self.__node_x0 = Node(data.x0, name='x0', notify_parent=True)
# #         self.__node_y0 = Node(data.y0, name='y0', notify_parent=True)
# #         self.__node_fwhm = Node(data.fwhm, name='fwhm', notify_parent=True)
# #         self.__node_max = Node(data.max, name='max', notify_parent=True)

# #         self.__node_x0.set_view_init_data(1,
# #                                           {'min_value': -10000,
# #                                            'max_value': 10000})
# #         self.__node_x0.set_view_init_data(1,
# #                                           {'min_value': -10000,
# #                                            'max_value': 10000})
# #         self.__node_fwhm.set_view_init_data(1,
# #                                           {'min_value': 0,
# #                                            'max_value': 10000})
# #         self.__node_max.set_view_init_data(1,
# #                                           {'min_value': 0,
# #                                            'max_value': 1e9})

# #         self.__node_x0.set_view_init_data(1, {})

# #         return [self.__node_x0, self.__node_y0,
# #                 self.__node_fwhm, self.__node_max]

# #     def node_changed(self, column, data):
# #         if column != 1:
# #             return
# #         model_data = data.get('model_data')
# #         if model_data is None:
# #             return

# #         # has it been initialized yet?
# #         if self.__node_x0 is None:
# #             return

# #         self.__node_x0.set_model_data(1, model_data.x0)
# #         self.__node_y0.set_model_data(1, model_data.y0)
# #         self.__node_fwhm.set_model_data(1, model_data.fwhm)
# #         self.__node_max.set_model_data(1, model_data.max)
# #         txt = 'Gauss({0}, {1}, {2}, {3})'.format(model_data.x0,
# #                                                  model_data.y0,
# #                                                  model_data.fwhm,
# #                                                  model_data.max)
# #         self.node().set_display_data(1, txt)

# #     def child_changed(self, child, column, data):
# #         model_data = data.get('model_data')
# #         if model_data is None:
# #             return
# #         peak = Simulator.GaussPeak(self.__node_x0.model_data(1),
# #                                    self.__node_y0.model_data(1),
# #                                    self.__node_fwhm.model_data(1),
# #                                    self.__node_max.model_data(1))
# #         self.node().set_model_data(1, peak)
