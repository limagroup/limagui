# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from ...tree.node import Node, NodeError

from .. import Qt
from .qhelpers import (nodeFromModelIndex,
                       qData,
                       qFlags,
                       qSetData,
                       data_kwargs_to_role)
from .errors import QRoleError, InvalidModelIndexError
# from .nodeerrordialog import NodeErrorDialog


_DEFAULT_COLUMN_NAMES = ['Name', 'Value']


class RootNode(Node):
    def __init__(self, *args, **kwargs):
        super(RootNode, self).__init__(*args, **kwargs)

    def column_name(self, column):
        return self.model_data(column)

    def set_column_name(self, column, column_name):
        self.set_model_data(column, column_name)

    def path(self):
        return []


class QModel(Qt.QAbstractItemModel):
    sigNodeError = Qt.Signal(NodeError)
    sigCellReset = Qt.Signal(Qt.QModelIndex)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.__rootNode = RootNode(name=_DEFAULT_COLUMN_NAMES[0],
                                   data=_DEFAULT_COLUMN_NAMES[1:])

        self.__rootNode._set_notify_change(self._dataChanged)
        self.__rootNode._set_notify_pre_insert(self._beginInsertRows)
        self.__rootNode._set_notify_post_insert(self._endInsertRows)
        self.__rootNode._set_notify_pre_remove(self._beginRemoveRows)
        self.__rootNode._set_notify_post_remove(self._endRemoveRows)
        self.__rootNode._set_notify_exception(self._nodeException)

        self.__dialog = None

        # this has to be done this way, otherwise the slot is never called
        def stopModel():
            self.stopModel()
        self.destroyed.connect(stopModel)

    def refresh(self, columns=(1,), index=Qt.QModelIndex(), children=True):
        try:
            node = nodeFromModelIndex(index)
        except InvalidModelIndexError:
            node = self.__rootNode

        # node.refresh(columns=columns, children=children)

    def setColumnNames(self, columnNames):
        assert len(columnNames) > 0

        for colIdx, colName in enumerate(columnNames):
            self.__rootNode.set_column_name(colIdx, colName)
        self.__rootNode.set_column_count(len(columnNames))

    def columnNames(self):
        return [self.__rootNode.column_name(colIdx)
                for colIdx in range(self.__rootNode.column_count())]

    def headerData(self, section, orientation, role=Qt.Qt.DisplayRole):
        if role == Qt.Qt.DisplayRole and orientation == Qt.Qt.Horizontal:
            return self.__rootNode.text(section)

        return super(QModel, self).headerData(section,
                                              orientation,
                                              role=role)

    def startModel(self):
        self.__rootNode.start_node()

    def stopModel(self):
        self.__rootNode.stop_node()

    def insertNodeData(self,
                       data=None,
                       row=-1,
                       name=None,
                       parent=Qt.QModelIndex(),
                       **kwargs):
        # TODO : allow parent to be a Node
        try:
            node = nodeFromModelIndex(parent)
        except InvalidModelIndexError:
            node = self.__rootNode

        newNode = node.insert_node_data(data=data, row=row, name=name, **kwargs)

        return self._nodeIndex(newNode, 0)

    def insertNode(self, node, row=-1, parent=Qt.QModelIndex()):
        try:
            parentNode = nodeFromModelIndex(parent)
            if parent.column() != 0:
                parent = self.index(parent.row(), 0, parent.parent())
        except InvalidModelIndexError:
            parentNode = self.__rootNode

        parentNode.insert_child(node, row, notify_args=parent)

        return self._nodeIndex(node, 0)

    def _nodeIndex(self, node, column=0):
        row = node.row()
        if row < 0:
            return Qt.QModelIndex()
        rows = [row]

        parent = node.parent()

        while parent is not None:
            rows.append(parent.row())
            parent = parent.parent()
    
        index = Qt.QModelIndex()
        for row in reversed(rows[:-1]):
            index = self.index(row, column, index)

        return index

    def _dataChanged(self, node, column, data_keys=None):
        # TODO : optimize
        index = self._nodeIndex(node, column=column)
        roles = []
        if data_keys:
            roles = data_kwargs_to_role(data_keys)
            # print('KEYS', data_keys, roles)
        if 'reset' in data_keys:
            self.sigCellReset.emit(index)
        else:
            self.dataChanged.emit(index, index, roles)

    def _beginInsertRows(self, parent, first, last, args):
        if args is None:
            args = self._nodeIndex(parent)
        self.beginInsertRows(args, first, last)

    def _endInsertRows(self, parent, first, last, args):
        self.endInsertRows()

    def _beginRemoveRows(self, parent, first, last, args):
        if args is None:
            args = self._nodeIndex(parent)
        self.beginRemoveRows(args, first, last)

    def _endRemoveRows(self, parent, first, last, args):
        self.endRemoveRows()

    def _nodeException(self, error):
        self.sigNodeError.emit(error)

    def removeRow(self, row, parent=Qt.QModelIndex()):
        try:
            node = nodeFromModelIndex(parent)
        except InvalidModelIndexError:
            node = self.__rootNode

        if not node:
            return False

        node.remove_child(row, notify_args=parent)

        return True

    def parent(self, index):
        if not index.isValid():
            return Qt.QModelIndex()

        try:
            node = nodeFromModelIndex(index)
        except InvalidModelIndexError:
            return Qt.QModelIndex()

        if node == self.__rootNode:
            return Qt.QModelIndex()

        parent = node.parent()

        # Dirty (?) hack
        # Sometimes when removing rows the view gets kinda lost,
        # and asks for indices that have been removed.
        # closing all editors before removing a row fixed one problem.
        # but there is still an issue if the removed row was selected.
        # This seems to fix it... maybe
        if parent is None:
            return Qt.QModelIndex()

        if parent == self.__rootNode:
            return Qt.QModelIndex()

        row = parent.row()

        if row < 0:
            return Qt.QModelIndex()
        return self.createIndex(row, 0, parent)

    def flags(self, index):
        try:
            node = nodeFromModelIndex(index)
        except InvalidModelIndexError:
            return Qt.Qt.NoItemFlags
        return qFlags(node, index.column())

    def columnCount(self, parent=Qt.QModelIndex(), **kwargs):
        return self.__rootNode.column_count()

    def data(self, index, role=Qt.Qt.DisplayRole, **kwargs):
        try:
            node = nodeFromModelIndex(index)
        except InvalidModelIndexError as ex:
            print(f'IndexError: {__file__}:{ex}.')
            return None

        try:
            data = qData(node, index.column(), role)
            return data
        except QRoleError as ex:
            # print(f'RoleError: {__file__}:{ex}.')
            return None

    def setData(self, index, value, role=Qt.Qt.EditRole):
        try:
            node = nodeFromModelIndex(index)
        except InvalidModelIndexError:
            return False

        try:
            rc = qSetData(node, index.column(), value, role)
            # if rc:
            #     # TODO : support multiple changes?
            #     # self.dataChanged.emit(index, index)
            #     self._dataChanged(node, index.column())
            if rc is None:
                rc = True
        except QRoleError:
            rc = True

        return rc

    def index(self, row, column, parent=Qt.QModelIndex()):
        if not self.hasIndex(row, column, parent):
            return Qt.QModelIndex()

        try:
            node = nodeFromModelIndex(parent)
        except InvalidModelIndexError:
            node = self.__rootNode

        child = node.child(row)

        if child is not None:
            return self.createIndex(row, column, child)

        return Qt.QModelIndex()

    def rowCount(self, parent=Qt.QModelIndex(), **kwargs):
        try:
            node = nodeFromModelIndex(parent)
        except InvalidModelIndexError:
            node = self.__rootNode

        return node.child_count()

    def hasChildren(self, parent=Qt.QModelIndex()):
        try:
            node = nodeFromModelIndex(parent)
        except InvalidModelIndexError:
            node = self.__rootNode

        return node.has_children()

    def pathToIndex(self, tokens, parent=Qt.QModelIndex()):
        try:
            parent = nodeFromModelIndex(parent)
        except InvalidModelIndexError:
            parent = self.__rootNode
        node = parent.path_to_node(tokens)
        if node is not None:
            return self._nodeIndex(node, 0)
        return Qt.QModelIndex()

    def clear(self, parent=Qt.QModelIndex()):
        """
        Clears the children of the given QModelIndex.
        :param parent: the index of the node to be cleared. Defaults to the
            root node.
        :return:
        """
        try:
            node = nodeFromModelIndex(parent)
        except InvalidModelIndexError:
            node = self.__rootNode
        node.remove_all()
