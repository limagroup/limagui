# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/01/2017"


from importlib import import_module

from limagui.paramtree import get_module, get_io_adapter_class


from . import ctcontrol
from . import ctacquisition
from . import ctvideo
from . import ctaccumulation
from . import ctimage
from . import triggers
from . import hwinterface

from . import camera

from .limaionames import (LIMA_IO_NAMES,
                          CtVideo,
                          LimaTriggers,
                          HwInterface)


# def get_camera_ios(camera_type):
#     return []


# def get_task_ios(camera_type):
#     return []


# def get_lima_io_classes():
#     lima_io_names = [e for enum in LIMA_IO_NAMES for e in enum]
#     lima_io_classes = [get_io_adapter_class(name) for name in lima_io_names]
#     return lima_io_classes


# def add_lima_ct_sources(model,
#                        cam_type,
#                        *args,
#                        **kwargs):
#     from Lima import Core

#     camera = None
#     print('Importing Lima.{0}'.format(cam_type))
#     cam_module = import_module('.{0}'.format(cam_type), 'Lima')
#     if hasattr(cam_module, 'Camera'):
#         if len(args) >= 1:
#             camera = cam_module.Camera(*args)
#         else:
#             camera = cam_module.Camera()
#         interface = cam_module.Interface(camera)
#     else:
#         if len(args) >= 1:
#             interface = cam_module.Interface(*args)
#         else:
#             interface = cam_module.Interface()

#     ctcontrol = Core.CtControl(interface)

#     model.set_data_source('CtControl', ctcontrol)
#     model.set_data_source('CtVideo', ctcontrol.video())
#     model.set_data_source('CtImage', ctcontrol.image())
#     model.set_data_source('CtAcquisition', ctcontrol.acquisition())
#     model.set_data_source('CtAccumulation', ctcontrol.accumulation())
#     model.set_data_source('CtSaving', ctcontrol.saving())
#     model.set_data_source('HwInterface', ctcontrol.hwInterface())
#     model.set_data_source('Camera', camera)


# def add_lima_ios(model,
#                  cam_type,
#                  *args,
#                  **kwargs):
#     lima_classes = get_lima_io_classes()

#     plugin_name = f'..lima_io.camera.{cam_type.lower()}'
#     print(f'Lima plugin: Trying to import {plugin_name} '
#           f'camera specific module.')
#     try:
#         cam_module = get_module(plugin_name, __name__)
#     except ImportError as ex:
#         print(f'Failed to import camera module: {ex}).')
#     else:
#         print('Lima plugin: LOADED {0} module.'.format(cam_module.__name__))
#         cam_ios = cam_module.get_lima_io_classes()
#         lima_classes.extend(cam_ios)

#     for io in lima_classes:
#         model.add_io_class(io)

#     # substitute_ios = factory.io_substitutes()
#     #     triggers = factory.triggers()
#     #     enable_conditions = factory.enable_conditions()

#     # for (new, orig) in substitute_ios:
#     #     self.add_substitution(orig, new)

#     #     self._io_event_mgr.add_triggers(triggers)

#     #     self._io_event_mgr.add_enable_conditions(enable_conditions)
        
#         # except Exception as ex:
#         #     print('Error while setting up factory {0}({1}).'
#         #           ''.format(factory.__class__.__name__, factory.name()))
#         #     import traceback
#         #     import sys
#         #     traceback.print_exc(file=sys.stdout)
#         #     self.notify_error()