
# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from collections import OrderedDict


from ..node import Node
from .treeadapter import TreeAdapterBase, treeadapterdef


@treeadapterdef(dict)
class DictTreeAdapter(TreeAdapterBase):
    def _sort_data(self, data):
        node_data = []
        cell_data = []
        if not isinstance(data, (dict,)):
            cell_data, node_data = super(DictTreeAdapter, self)._sort_data(data)
        else:
            node_data = [{'name':key, 'data':value} for key, value in data.items()]
        return cell_data, node_data
