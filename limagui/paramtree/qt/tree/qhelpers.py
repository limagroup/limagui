# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from collections import OrderedDict

from six import string_types

from .. import Qt, getQIcon
from .errors import (QRoleError,
                     InvalidModelIndexError,
                     EmptyModelIndexError,
                     ModelIndexSelectionError)

IndexExceptions = (InvalidModelIndexError, EmptyModelIndexError)


def nodeFromModelIndex(index, raiseEx=True):
    try:
        if not index.isValid():
            raise InvalidModelIndexError()
        if isinstance(index.model(), (Qt.QAbstractProxyModel,)):
            index = index.model().mapToSource(index)
        node = index.internalPointer()
        if not node:
            raise EmptyModelIndexError()
    except:
        if raiseEx:
            raise
        return None
    return node


def data_kwargs_to_role(data_kwargs):
    """
    Returns an array with the roles corresponding to the
    keys that can be found in the data structure returned
    by the notify method of the io_adapter.
    If one of the key is unknown, it returns an empty array,
    to make sure that the model updates everything.
    """
    roles = {'data': Qt.Qt.EditRole,
             'text': Qt.Qt.DisplayRole,
             'view_data': Qt.Qt.EditRole,
             'background_color': Qt.Qt.BackgroundColorRole,
             'tooltip': Qt.Qt.ToolTipRole,
             'decoration':Qt.Qt.DecorationRole,
             'foreground_color': Qt.Qt.ForegroundRole,
             'checked': Qt.Qt.CheckState}

    try:
        return set(roles[key] for key in data_kwargs)
    except KeyError:
        return []


def qData(node, column, role):
    if not node:
        raise ValueError('<node> is mandatory')

    if role == Qt.Qt.EditRole:
        return node.editor_data(column)

    if role == Qt.Qt.DisplayRole:
        return node.text(column)

    if role == Qt.Qt.BackgroundColorRole:
        color = node.background_color(column)
        if color is not None:
            color = Qt.QColor(color)
        return color

    if role == Qt.Qt.ToolTipRole:
        return node.tooltip(column)

    if role == Qt.Qt.CheckStateRole:
        if not node.checkable(column):
            return None
        return (node.checked(column) and Qt.Qt.Checked) or Qt.Qt.Unchecked

    if role == Qt.Qt.DecorationRole:
        decoration = node.decoration(column)
        if decoration is not None:
            if isinstance(decoration, (string_types,)) is not None:
                try:
                    return Qt.QColor(decoration)
                except:
                    pass
                try:
                    return getQIcon(decoration)
                except:
                    pass
                try:
                    return Qt.QPixmap(decoration)
                except:
                    pass
        return decoration

    if role == Qt.Qt.SizeHintRole:
        size = node.view_size(column)
        if size is not None and any(map(lambda v: v != -1, size)):
            return Qt.QSize(*size)
        else:
            return None

    raise QRoleError('Unsupported role: {0}.'.format(role))


def qSetData(node, column, value, role):
    if not node:
        raise ValueError('<node> is mandatory')

    if role == Qt.Qt.EditRole:
        return node.set_editor_data(column, value)

    if role == Qt.Qt.DisplayRole:
        return node.set_editor_data(column, value)

    if role == Qt.Qt.BackgroundColorRole:
        return node.set_background_color(column, value)

    if role == Qt.Qt.ToolTipRole:
        return node.set_tooltip(column, value)

    if role == Qt.Qt.CheckStateRole:
        return node.set_checked(column, (value == Qt.Qt.Checked))

    if role == Qt.Qt.DecorationRole:
        return node.set_decoration(column, value)

    if role == Qt.Qt.SizeHintRole:
        return True

    raise QRoleError('Unsupported role: {0}.'.format(role))


def qFlags(node, column):
    if not node:
        raise ValueError('<node> is mandatory')

    flags = Qt.Qt.NoItemFlags

    if node.selectable(column):
        flags |= Qt.Qt.ItemIsSelectable

    if node.editable(column):
        flags |= Qt.Qt.ItemIsEditable

    if node.enabled(column):
        flags |= Qt.Qt.ItemIsEnabled

    if node.drag_enabled(column):
        flags |= Qt.Qt.ItemIsDragEnabled

    if node.drop_enabled(column):
        flags |= Qt.Qt.ItemIsDropEnabled

    if node.checkable(column):
        flags |= Qt.Qt.ItemIsUserCheckable

    return flags


def getViewSelectedNodes(view,
                         single=True,
                         allow_empty=True):

    selection = OrderedDict()

    indices = view.selectedIndexes()

    if len(indices) == 0:
        if allow_empty:
            return selection
        else:
            raise ModelIndexSelectionError('No selection.')

    for index in indices:
        node = nodeFromModelIndex(index)
        # TODO : check if the index stored has column = 0
        # useful when inserting/removing node
        selection[node] = index

    if single and len(selection) > 1:
        raise ModelIndexSelectionError('More that one node selected.')

    return selection
