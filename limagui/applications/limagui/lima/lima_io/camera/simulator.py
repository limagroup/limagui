# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from Lima import Simulator

from limagui.paramtree import VectorBase
from limagui.paramtree.utils.sip_utils import sipenum2python

from limagui.paramtree.iomodel import ioclassdef, IOBase
from ..limaionames import LimaTriggers
from limagui.paramtree.iomodel import get_io_class

from limagui.paramtree.iomodel import IoNamesEnum, ionames_enum_def

_PLUGIN_NAME = 'lima'


def get_lima_io_classes():
    simulator_io_names = [e for e in SimulatorIoNames]
    simulator_io_classes = [get_io_class(name) for name in simulator_io_names]
    return simulator_io_classes


@ionames_enum_def(_PLUGIN_NAME)
class SimulatorIoNames(IoNamesEnum):
    ROTATION_AXIS = 'Lima@Camera:Rot. axis'
    FILL_TYPE = 'Lima@Camera:Fill type'
    ROTATION_ANGLE = 'Lima@Camera:Rot. angle'
    ROTATION_SPEED = 'Lima@Camera:Rot. speed'
    DIFFRACTION_SPEED = 'Lima@Camera:Diff. speed'
    DIFFRACTION_POS = 'Lima@Camera:Diff. pos.'
    PEAKS = 'Lima@Camera:Peaks'
    PEAK_ANGLES = 'Lima@Camera:Peak angles'


RotationAxis = sipenum2python(Simulator.FrameBuilder, Simulator.FrameBuilder.RotationAxis)
FillType = sipenum2python(Simulator.FrameBuilder, Simulator.FrameBuilder.FillType)


@ioclassdef(SimulatorIoNames.ROTATION_AXIS,
            io_getter=lambda self, cam: cam.getFrameGetter().getRotationAxis(),
            io_setter=lambda self, cam, data: cam.getFrameGetter().setRotationAxis(data),
            data_type=RotationAxis,
            triggers=[LimaTriggers.REFRESH_TRIGGER])
class LimaSimulatorRotationAxis(IOBase):
    pass


@ioclassdef(SimulatorIoNames.FILL_TYPE,
            io_getter=lambda self, cam: cam.getFrameGetter().getFillType(),
            io_setter=lambda self, cam, data: cam.getFrameGetter().setFillType(data),
            data_type=FillType,
            triggers=[LimaTriggers.REFRESH_TRIGGER])
class LimaSimulatorFillType(IOBase):
    pass


@ioclassdef(SimulatorIoNames.ROTATION_ANGLE,
            io_getter=lambda self, cam: cam.getFrameGetter().getRotationAngle(),
            io_setter=lambda self, cam, data: cam.getFrameGetter().setRotationAngle(data),
            data_type=float,
            triggers=[LimaTriggers.REFRESH_TRIGGER],
            units='deg',
            view_options={'min_value': -360.,
                          'max_value': 360.})
class LimaSimulatorRotationAngle(IOBase):
    pass


@ioclassdef(SimulatorIoNames.ROTATION_SPEED,
            io_getter=lambda self, cam: cam.getFrameGetter().getRotationSpeed(),
            io_setter=lambda self, cam, data: cam.getFrameGetter().setRotationSpeed(data),
            data_type=float,
            triggers=[LimaTriggers.REFRESH_TRIGGER],
            view_options={'min_value': -10000.,
                          'max_value': 10000.})
class LimaSimulatorRotationSpeed(IOBase):
    pass

class SimulatorDiffractionSPeed(VectorBase):
    names = ('X', 'Y')
    values = (-1., -1.)


@ioclassdef(SimulatorIoNames.DIFFRACTION_SPEED,
            io_getter=lambda self, cam: cam.getFrameGetter().getDiffractionSpeed(),
            io_setter=lambda self, cam, data: cam.getFrameGetter().setDiffractionSpeed(*data),
            data_type=SimulatorDiffractionSPeed,
            description='Diffration speed',
            triggers=[LimaTriggers.REFRESH_TRIGGER],
            view_options={'min_value': 0.,
                          'max_value': 10000.})
class LimaSimulatorDiffrationSpeed(IOBase):
    pass


class SimulatorDiffractionPosition(VectorBase):
    names = ('X', 'Y')
    values = (-1., -1.)


@ioclassdef(SimulatorIoNames.DIFFRACTION_POS,
            io_getter=lambda self, cam: cam.getFrameGetter().getDiffractionPos(),
            io_setter=lambda self, cam, data: cam.getFrameGetter().setDiffractionPos(*data),
            data_type=SimulatorDiffractionPosition,
            description='Diffration position',
            triggers=[LimaTriggers.REFRESH_TRIGGER],
            view_options={'min_value': -10000.,
                          'max_value': 10000.})
class LimaSimulatorDiffrationPos(IOBase):
    pass


@ioclassdef(SimulatorIoNames.PEAKS,
            io_getter=lambda self, cam: cam.getFrameGetter().getPeaks(),
            io_setter=lambda self, cam, data: cam.getFrameGetter().setPeaks(data),
            data_type=list,
            triggers=[LimaTriggers.REFRESH_TRIGGER])
class LimaSimulatorPeaks(IOBase):
    pass


@ioclassdef(SimulatorIoNames.PEAK_ANGLES,
            io_getter=lambda self, cam: cam.getFrameGetter().getPeakAngles(),
            io_setter=lambda self, cam, data: cam.getFrameGetter().setPeakAngles(data),
            data_type=list,
            triggers=[LimaTriggers.REFRESH_TRIGGER])
class LimaSimulatorPeakAngles(IOBase):
    pass
