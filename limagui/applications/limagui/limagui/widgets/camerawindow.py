from tango.group import Group
from limagui.applications.limagui import lima
from limagui.applications.limagui.lima.lima import (new_lima_camera,
                                                    add_lima_sources,
                                                    lima_io_tree)
from limagui.paramtree import IoGroup
from limagui.applications.limagui.lima.lima_io.limaionames import CtControl, CtAcquisition
from limagui.paramtree.qt import Qt, QScheduler, TreeView, QModel, getQIcon
from .limaimagewidget import LimaImageWidget
from limagui.paramtree.qt import QCellWidget, QCellDiodeWidget, QCellLabelWidget
from .widgets import GroupBox
from .toolbars import AcquisitionToolBar, VideoToolBar, StatusToolBar, LimaOptionsToolBar


def _init_lima_camera(io_group, *args, cam_type=None, name=None, **kwargs):
    grp_name = io_group.name()
    if cam_type.lower() == 'tango':
        from ...limatango.limaccds import (new_lima_ccds,
                                           limaccds_add_substitutes,
                                           limaccds_import_bpm)
        lima_cam = new_lima_ccds(*args, **kwargs)
        if not grp_name:
            if not name:
                name = lima_cam['LimaCCDs'].name().split('/')[-1]
            io_group._set_name(name)
        limaccds_add_substitutes(io_group)
        limaccds_import_bpm(io_group)
    else:
        lima_cam = new_lima_camera(cam_type, *args, **kwargs)
    add_lima_sources(io_group, lima_cam)


class CameraTabBarWidget(Qt.QWidget):
    def __init__(self,
                 io_group,
                 *args,
                 name=None,
                 screen_io=None,
                 read_only=False,
                 extra_widgets=None,
                 **kwargs):
        super(CameraTabBarWidget, self).__init__(*args, **kwargs)

        self._io_group = io_group

        self._liveModeOnStart = False

        layout = Qt.QVBoxLayout(self)
        titleLayout = Qt.QHBoxLayout()
        bnLayout = self._bnLayout = Qt.QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        titleLayout.setContentsMargins(0, 0, 0, 0)
        bnLayout.setContentsMargins(0, 0, 0, 0)

        layout.addLayout(titleLayout)
        layout.addLayout(bnLayout)

        titleLayout.addWidget(Qt.QLabel(name or io_group.name()))

        io_group = self.io_group()

        self.acqIo = io_group.io_adapter(CtControl.STATUS)
        statusLed = QCellDiodeWidget(self.acqIo)
        bnLayout.addWidget(statusLed)

        if not read_only:
            self.nbFramesIo = io_group.io_adapter(CtAcquisition.ACQNBFRAMES)
            self.prepareIo = io_group.io_adapter(CtControl.PREPAREACQ)
            self.startIo = io_group.io_adapter(CtControl.STARTACQ)
            self.stopIo = io_group.io_adapter(CtControl.STOPACQ)

            startTb = Qt.QToolButton(self)
            icon = getQIcon(Qt.QStyle.SP_MediaPlay)
            startTb.setIcon(icon)
            startTb.clicked.connect(self._prepareAndStart)

            stopBn = QCellWidget(self.stopIo)
            icon = getQIcon(Qt.QStyle.SP_MediaStop)
            stopBn.setIcon(icon)

            bnLayout.addWidget(startTb)
            bnLayout.addWidget(stopBn)

        if not read_only and screen_io:
            from limagui.applications.limagui.limatango.widgets.bvscreenbutton import ScreenButton
            bnLayout.addWidget(ScreenButton(io_group, screen_io=screen_io))

    def io_group(self):
        return self._io_group

    def setLiveModeOnStart(self, liveMode):
        """
        If True, the AcqNbFrames will be set to zero (=live)
        when the start button is pressed.
        """
        self._liveModeOnStart = liveMode

    def _prepareAndStart(self):
        self.stopIo.set_data(data=True)
        if self._liveModeOnStart:
            self.nbFramesIo.set_data(data=0)
        self.prepareIo.set_data(data=True)
        self.startIo.set_data(data=True)


class InfoWidget(Qt.QWidget):
    def __init__(self, io_group, read_only=False, bpm_wid=False, **kwargs):
        super().__init__(**kwargs)

        topLayout = Qt.QGridLayout(self)

        if io_group.has_source('LimaCCDs'):
            from limagui.paramtree.qt.tree.widgets.tangowidgets import TangoStatusWidget
            tangoGroupBox = GroupBox('Tango')
            tangoGroupLayout = Qt.QHBoxLayout(tangoGroupBox)
            tangoWid = TangoStatusWidget(io_group.source('LimaCCDs'))
            tangoGroupLayout.addWidget(tangoWid)
            topLayout.addWidget(tangoGroupBox, 0, 0)

        topGroupBox = GroupBox('AcqStatus')
        topGroupLayout = Qt.QHBoxLayout(topGroupBox)
        statusTb = StatusToolBar(io_group)
        topGroupLayout.addWidget(statusTb)
        topLayout.addWidget(topGroupBox, 0, topLayout.columnCount())

        if bpm_wid and io_group.has_source('BpmDeviceServer'):
            from ...limatango.widgets.toolbars import BpmStateToolBar
            topGroupBox = GroupBox('Bpm State')
            topGroupLayout = Qt.QHBoxLayout(topGroupBox)
            bpmStateTb = BpmStateToolBar(io_group)
            topGroupLayout.addWidget(bpmStateTb)
            topLayout.addWidget(topGroupBox, 0, topLayout.columnCount())
            
        controlsTabWid = Qt.QTabWidget()

        if not read_only:
            acqToolBar = AcquisitionToolBar(io_group)
            controlsTabWid.addTab(acqToolBar, 'Acq.')

            videoTb = VideoToolBar(io_group)
            controlsTabWid.addTab(videoTb, 'Video')

            if bpm_wid and io_group.has_source('BpmDeviceServer'):
                from ...limatango.widgets.toolbars import BpmToolBar
                bpmToolBar = BpmToolBar(io_group)
                controlsTabWid.addTab(bpmToolBar, 'Bpm')

        optionsTb = LimaOptionsToolBar(io_group)
        idx = controlsTabWid.addTab(optionsTb, '')
        icon = getQIcon('lima:icons/gears')
        label = Qt.QLabel()
        label.setPixmap(icon.pixmap(18, 18))
        controlsTabWid.tabBar().setTabButton(idx, Qt.QTabBar.LeftSide, label)

        topLayout.addWidget(controlsTabWid, 0, topLayout.columnCount())

        if io_group.has_source('LimaCCDs'):
            tgEvent = io_group.source('LimaCCDs').get_property('TangoEvent')
            tgEvent = tgEvent.get('TangoEvent', [None])
            if len(tgEvent) == 0 or tgEvent[0] in (None, False):
                warnGroup = GroupBox('WARNING')
                warnLayout = Qt.QHBoxLayout(warnGroup)
                label = Qt.QLabel()
                icon = getQIcon(Qt.QStyle.SP_MessageBoxCritical)
                label.setPixmap(icon.pixmap(label.size()))
                warnLayout.addWidget(label)
                label = Qt.QLabel('TangoEvent property not set => no images.')
                warnLayout.addWidget(label)
                label = Qt.QLabel()
                label.setPixmap(icon.pixmap(label.size()))
                warnLayout.addWidget(label)
                topLayout.addWidget(warnGroup,
                                    topLayout.rowCount(), 
                                    0,
                                    1,
                                    topLayout.columnCount(),
                                    alignment=Qt.Qt.AlignCenter)

        topLayout.setColumnStretch(topLayout.columnCount(), 1)
    

class CameraWindow(Qt.QMainWindow):
    def __init__(self,
                 *args,
                 parent=None,
                 cam_type=None,
                 read_only=False,
                 name=None,
                 bpm_win=False,
                 cam_args=(),
                 cam_kwargs={}):
        super().__init__(parent=parent)

        self._read_only = read_only
        self._tabBarWidget = None

        io_group = IoGroup(name=name)
        _init_lima_camera(io_group, name=name, cam_type=cam_type, *cam_args, **cam_kwargs)
        self._io_group = io_group

        self._plot = None
        # self._bpm_win = None

        centralWid = Qt.QWidget()
        centralLayout = Qt.QGridLayout(centralWid)
        self.setCentralWidget(centralWid)

        self._infoWid = InfoWidget(io_group, read_only=read_only, bpm_wid=bpm_win)
        centralLayout.addWidget(self._infoWid, 0, 1)

        tabWid = Qt.QTabWidget()
        # tabWid.currentChanged.connect(self._currentIndexChanged)
        tabWid.tabBar().setVisible(False)

        #############
        # Main plot
        #############
        self._plot = plot = LimaImageWidget(io_group)
        plot.setWindowTitle('Lima')
        tabWid.addTab(plot, plot.windowTitle())
        centralLayout.addWidget(tabWid, 1, 0, 1, 2)

        centralLayout.setRowStretch(1, 100)

        #############
        # Bpm Window
        #############
        if bpm_win and io_group.has_source('BpmDeviceServer'):
            from ...limatango.widgets.limabpmwidget import LimaBpmWidget
            self._bpmWidget = LimaBpmWidget(io_group)
            tabWid.addTab(self._bpmWidget, 'BPM')
            tabWid.tabBar().setVisible(True)

        #############
        # Tree
        #############

        if not read_only:

            stylesheet = """
                    QTreeView {
                        background-color: #d2d8e0;
                        alternate-background-color: #FFFFFF;
                    }
                """

            io_tree = lima_io_tree(io_group)
            
            tree = TreeView(parent=self)
            tree.setStyleSheet(stylesheet)
            qmodel = QModel(tree)
            tree.setModel(qmodel)
            qmodel.startModel()
            tree.setAlternatingRowColors(True)

            for key, value in io_tree.items():
                qmodel.insertNodeData(data=value, name=key)

            dock = Qt.QDockWidget()
            dock.setObjectName(f'{io_group.name()}')
            dock.setWindowTitle('Lima')
            dock.setWidget(tree)
            self.addDockWidget(Qt.Qt.LeftDockWidgetArea, dock) 
            toggle = dock.toggleViewAction()
            icon = getQIcon('lima:icons/lima')
            toggle.setIcon(icon)
            toggleTb = Qt.QToolButton()
            toggleTb.setDefaultAction(toggle)
            # TODO: better size management
            sz = int(Qt.qApp.style().pixelMetric(
                        Qt.QStyle.PM_LargeIconSize) * 1.5)
            toggleTb.setIconSize(Qt.QSize(sz, sz))
            centralLayout.addWidget(toggleTb, 0, 0)
            dock.hide()

    def tabBarWidget(self, name=None, screen_io=None):
        if self._tabBarWidget is None:
            self._tabBarWidget = CameraTabBarWidget(self._io_group,
                                                    name=name,
                                                    extra_widgets=None,
                                                    screen_io=screen_io,
                                                    read_only=self._read_only)
        return self._tabBarWidget

    def stop_io_group(self):
        self._io_group.stop_group()

    def io_group(self):
        return self._io_group
