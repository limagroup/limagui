from tango import DevState

from limagui.paramtree.qt import Qt, getQIcon
from limagui.paramtree.qt.tree.widgets.widgets import (QCellDiodeWidget,
                                                       QCellLabelWidget,
                                                       QCellActionWidget,
                                                       QCellToolButtonMenu,
                                                       QCellWidgetAction,
                                                       QCellToolButton)
                                                       
from ...limatango.tango_io.tangoionames import LimaCCDsTango
from ...limatango.tango_io.plugins.bpmdeviceserver import BpmDeviceServerNames
from ...limatango.tango_io.limaccds import TangoStateColors


# class TangoToolBar(Qt.QToolBar):
#     def __init__(self, io_group, parent=None):
#         super().__init__(parent)

#         self.setWindowTitle('Tango')
#         self.setToolTip('Tango')
#         self.setStatusTip('Tango')

#         # icon = getQIcon('lima:icons/microscope')
#         icon = getQIcon('lima:icons/tango_logo')
#         self.setWindowIcon(icon)

#         statusWidget = TangoStatusWidget(io_group.source('LimaCCDs'))
#         self.addWidget(statusWidget)

#         # state_io = io_group.io_adapter(LimaCCDsTango.TANGO_STATE)
#         # stateDiode = QCellDiodeWidget(data=state_io, parent=self)
#         # stateDiode.setColorDict(TangoStateColors)

#         # self.addWidget(stateDiode)

#         # stateLabel = QCellLabelWidget(state_io)
#         # self.addWidget(stateLabel)


class BpmToolBar(Qt.QToolBar):
    def __init__(self, io_group, parent=None):
        super().__init__(parent)

        title = 'Lima BpmDeviceServer'
        self.setWindowTitle(title)
        self.setToolTip(title)
        self.setStatusTip(title)

        start = io_group.io_adapter(BpmDeviceServerNames.START)
        stop = io_group.io_adapter(BpmDeviceServerNames.STOP)
        state = io_group.io_adapter(BpmDeviceServerNames.STATE)
        lut = io_group.io_adapter(BpmDeviceServerNames.LUT)
        autoscale = io_group.io_adapter(BpmDeviceServerNames.AUTOSCALE)
        colormap = io_group.io_adapter(BpmDeviceServerNames.COLORMAP)

        colors = {DevState.ON: 'green',
                  DevState.OFF: 'red',
                  DevState.UNKNOWN: 'black'}
        stateWid = QCellDiodeWidget(data=state)
        stateWid.setColorDict(colors)
        self.addWidget(stateWid)

        startBn = QCellToolButton(data=start)
        icon = getQIcon(Qt.QStyle.SP_MediaPlay)
        startBn.setIcon(icon)
        self.addWidget(startBn)

        stopBn = QCellToolButton(data=stop)
        icon = getQIcon(Qt.QStyle.SP_MediaStop)
        stopBn.setIcon(icon)
        self.addWidget(stopBn)

        lutWid = QCellToolButtonMenu(data=lut)
        self.addWidget(lutWid)

        colormapWid = QCellWidgetAction(data=colormap, parent=self)
        # colormapWid.setIcon(icon)
        self.addAction(colormapWid)

        autoscaleWid = QCellToolButton(autoscale)
        # autoscaleWid.setIcon(icon)
        self.addWidget(autoscaleWid)


class BpmStateToolBar(Qt.QToolBar):
    def __init__(self, io_group, parent=None):
        super().__init__(parent)

        title = 'Lima Bpm State'
        self.setWindowTitle(title)
        self.setToolTip(title)
        self.setStatusTip(title)

        state = io_group.io_adapter(BpmDeviceServerNames.STATE)

        colors = {DevState.ON: 'green',
                  DevState.OFF: 'red',
                  DevState.UNKNOWN: 'black'}
        stateWid = QCellDiodeWidget(data=state)
        stateWid.setColorDict(colors)
        self.addWidget(stateWid)

        stateLabel = QCellLabelWidget(state)
        self.addWidget(stateLabel)