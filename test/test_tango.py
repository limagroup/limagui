from re import sub
import sys
import time

# from Lima.Core import CtVideo as LimaCtVideo
from limagui.paramtree.iomodel.extra.tangomixin import new_tango_io_class

from limagui.paramtree.qt import Qt
from limagui.applications.limagui.limagui.widgets.mainwindow import new_lima_model
from limagui.applications.limagui.limagui.widgets.controltoolbar import ControlToolBar
# from limagui.paramtree.qt.iomodel.qtapp import QtApp

from limagui.applications.limagui.lima.factories.limafactory import LimaFactory
from limagui.applications.limagui.limatango.limatangofactory import LimaTangoFactory
from limagui.applications.limagui.limatango.tango_io.tangoionames import TangoLimaCCDs

# from limagui.paramtree.qt.iomodel import iobaseioadapter, iobasetreeadapter


#from limagui.paramtree.qt.iomodel.qtapp import initIoModel
from limagui.paramtree.qt import TreeView
from limagui.paramtree.qt import QModel
from limagui.paramtree.iomodel.iobase import registered_io_classes, registered_io_names, IOBase, ioclassdef

# from limagui.plugins.limatango.model.tangoionames import TangoLimaCtImage
from limagui.applications.limagui.lima.lima_io.limaionames import CtImage, CtControl, CtAcquisition, CtVideo, CtAccumulation, LimaTriggers

try:
  from limagui.applications.limagui.lima.lima_io.camera.simulator import SimulatorIoNames
except:
  pass

from limagui.paramtree.qt.tree.widgets.qwidgetmixin import QCellWidget
from limagui.paramtree.qt.tree.widgets.widgets import QCellWidgetAction, QCellActionWidget, QCellDiodeWidget, QCellToolButtonMenu


from limagui.paramtree.iomodel import IoNamesEnum, ionames_enum_def


class MyIo(IoNamesEnum):
    T0 = 'Test@LimaCCDs:acq_nb_frames'
    T1 = 'Test@LimaCCDs:prepareAcq'
    T2 = 'Test@LimaCCDs:video_live'
    T3 = 'Test@LimaCCDs:video_live'
    T4 = 'Test@LimaCCDs:image_events_push_data'


app = Qt.QApplication([])

if len(sys.argv) > 1:
    model_args = (sys.argv[1],)
    mdl = new_lima_model('yolo', 'Simulator', *model_args)
else:
    model_args = ('d20-simu',)
    mdl = new_lima_model('yolo', 'LimaCCDs', *model_args)

for e in MyIo:
  TestCls = new_tango_io_class(e, refresh_delay=1000)
  mdl.add_io_class(TestCls)

# TestCls = new_tango_io_class(TangoLimaCCDs.TANGO_PUSH_IMAGE_EVENTS)#, refresh_delay=1000)
# mdl.add_io_class(TestCls)

#mdl.add_substitution(CtVideo.STARTLIVE, MyIo.T2)
# mdl.add_substitution(CtVideo.STOPLIVE, MyIo.T3)
mdl.add_substitution(CtControl.PREPAREACQ, MyIo.T1)
mdl.add_substitution(CtAcquisition.ACQNBFRAMES, MyIo.T0)

main = Qt.QMainWindow()
win = Qt.QWidget()
layout = Qt.QVBoxLayout(win)
main.setCentralWidget(win)

tree = TreeView()
qmodel = QModel(tree)
tree.setModel(qmodel)
qmodel.startModel()

if 0:
  ios = [
          #  mdl.get_io_instance(MyIo.T0),
          #  mdl.get_io_instance(MyIo.T1),
          mdl.get_io_instance(CtAcquisition.ACQNBFRAMES),
          # mdl.get_io_instance(MyIo.T2),
          # mdl.get_io_instance(MyIo.T3),
          mdl.get_io_instance(CtVideo.STARTLIVE),
          # mdl.get_io_instance(MyIo.T4),
        ]
  qmodel.insertNodeData({io.io_name.name:io for io in ios})
  rIndex = qmodel.index(0, 0)
  tree.setRootIndex(rIndex)
  layout.addWidget(tree)

if 1:
  # io = mdl.get_io_instance(TangoLimaCCDs.TANGO_PUSH_IMAGE_EVENTS)
  # io = mdl.get_io_instance(CtControl.PREPAREACQ)
  io = mdl.get_io_instance(CtAcquisition.ACQNBFRAMES)
  # eventsWid = QCellWidget(io)
  act = QCellWidgetAction(io, win)
  # iowid = QCellToolButtonMenu(io)
  tb = main.addToolBar('wesh')
  tb.addAction(act)
  # tb.addWidget(Qt.QLabel(io.io_name.name))
  # iowid = QCellActionWidget(io, parent=tb)
  # tb.addWidget(iowid)
  # sublayout = Qt.QFormLayout()
  # sublayout.addRow(io.io_name.name, wid)
  # layout.addLayout(sublayout)

main.show()
app.exec_()