# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"

from functools import partial

from .viewadapter import ViewAdapterBase, viewadapterdef
from ..types.action import Action, Actions


@viewadapterdef(Action)
@viewadapterdef(Actions)
class ActionsAdapter(ViewAdapterBase):

    def __init__(self, cell, data=None, **kwargs):
        super(ActionsAdapter, self).__init__(cell, data=None, **kwargs)
        self._actions = None

        if isinstance(data, (Action,)):
            actions = Actions([data])
        elif isinstance(data, (Actions,)):
            actions = data
        else:
            actions = Actions([Action()])

        self._actions = actions

        self._set_adapter_type(Actions)

    def icon(self):
        icon = super(ActionsAdapter, self).icon()
        if icon:
            return icon
        return [act.icon for act in self._actions]

    def actions(self):
        return self._actions

    def to_text(self, model_data):
        return None
