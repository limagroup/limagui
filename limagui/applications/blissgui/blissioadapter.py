from bliss.common import event
from limagui.paramtree import IoAdapterBase


class BlissIoAdapter(IoAdapterBase):
    def __init__(self, *args, bliss_chan_name=None, **kwargs):
        self._bliss_chan_name = bliss_chan_name
        if bliss_chan_name:
            io_getter = lambda src: getattr(src, bliss_chan_name)
        super().__init__(*args, io_getter=io_getter, **kwargs)

    def _on_start(self):
        event.connect(self.source(), self._bliss_chan_name, self._on_bliss_event)
        return super()._on_start()

    def _on_stop(self):
        event.disconnect(self.source(), self._bliss_chan_name, self._on_bliss_event)
        return super()._on_stop()

    def _on_bliss_event(self, value, signal=None, sender=None):
        self.set_data(data=value, _internal=True)