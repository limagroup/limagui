# -*- coding: utf-8 -*-


__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"

from Lima.Core import CtControl


from limagui.paramtree import SubListTreeAdapter, treeadapterdef, ViewAdapterBase, viewadapterdef
from limagui.paramtree.tree.ioadapter import IoAdapterBase


def _img_counters_to_list(counters):
    return  [counters.LastImageAcquired,
             counters.LastBaseImageReady,
             counters.LastImageReady,
             counters.LastImageSaved,
             counters.LastCounterReady]


@viewadapterdef(CtControl.ImageStatus)
class LimaImageStatusViewAdapter(ViewAdapterBase):
    def to_text(self, data):
        if data is not None:
            return ', '.join([str(v) for v in _img_counters_to_list(data)])
        return super().to_text(data)


# @treeadapterdef(CtControl.ImageStatus)
# class LimaImageStatusTreeAdapter(SubListTreeAdapter):
#     def _init_cells(self, cell_data):
#         cells = super()._init_cells(cell_data)
#         cells[1].set_view_adapter(LimaImageStatusViewAdapter)
#         return cells

#     def __init__(self, node, data, *args, **kwargs):
#         # print('INIT TREEEEE', data)
#         if isinstance(data, (CtControl.ImageStatus,)):
#             kwargs['sub_data'] = _img_counters_to_list(data)
#         elif isinstance(data, (IoAdapterBase,)):
#             kwargs['sub_data'] = _img_counters_to_list(data.data())
#         else:
#             kwargs['sub_data'] = [-2, -2, -2, -2, -2]
#         kwargs['sub_names'] = ['Img Acq.', 'Base Img', 'Img Rdy', 'Img Saved.', 'Counter Rdy']
#         kwargs['editable'] = False
#         super(LimaImageStatusTreeAdapter, self).__init__(node, data, *args, **kwargs)

#     def _root_to_sub(self, root_data):
#         # print('RTS', root_data)
#         return _img_counters_to_list(root_data)
