# -*- coding: utf-8 -*-

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"


from Lima.Core import Bin
from numpy import DataSource
from limagui.paramtree import SubListTreeAdapter, treeadapterdef


@treeadapterdef(Bin)
class LimaBinTreeAdapter(SubListTreeAdapter):
    def __init__(self, *args, **kwargs):
        kwargs['sub_names'] = ['X', 'Y']
        kwargs['editable'] = False
        super(LimaBinTreeAdapter, self).__init__(*args, **kwargs)

    def _sub_to_root(self, root_data, row, cell_data):
        bin = None
        if row == 0:
            bin = Bin(cell_data, root_data.getY())
        elif row == 1:
            bin = Bin(root_data.getX(), cell_data)
        return bin

    def _root_to_sub(self, root_data):
        return [root_data.getX(), root_data.getY()]