# -*- coding: utf-8 -*-

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"


from Lima import Core

ACQSTATUS_COLORS = {
    Core.AcqReady: 'light green',
    Core.AcqFault: 'red',
    Core.AcqRunning: 'blue',
    Core.AcqConfig: 'orange'
}
