import enum
from functools import partial
from collections import OrderedDict

from limagui.paramtree import Node, Action, Actions
from limagui.paramtree.qt import QModel, TreeView
from limagui.paramtree.tree.types import Progress
from limagui.paramtree.qt import Qt
from limagui.paramtree.qt.tree.widgets import QCellWidget
from limagui.paramtree.qt.tree.widgets import widgets as cellwids


class Worker(Qt.QObject):

    sigDoWork = Qt.pyqtSignal(float)
    sigWorkDone = Qt.pyqtSignal()
    sigProgress = Qt.pyqtSignal(int)

    def __init__(self, parent=None):
        super(Worker, self).__init__()
        self._th = Qt.QThread()
        self.moveToThread(self._th)

        self.sigDoWork.connect(self._doWork)
        self._sec = 2
        self._stop = False
        self._th.start()

    def setWorkDuration(self, sec):
        print('Set work duration to', sec)
        self._sec = sec

    def workDuration(self):
        return self._sec

    def work(self):
        print("Work now!")
        self.sigDoWork.emit(self.workDuration())

    def stop(self):
        self._stop = True

    def _doWork(self, sec):
        print('Working', sec)

        self._stop = False
        step_t = 100

        prog = 0
        sec = sec * 1000
        finished = True

        tStart = Qt.QTime.currentTime()
        elapsed = tStart.elapsed()
        while elapsed < sec:
            prog = 100. * elapsed / sec
            if self._stop:
                finished = False
                break
            self.sigProgress.emit(prog)
            Qt.QThread.msleep(step_t)
            elapsed = tStart.elapsed()

        if finished:
            self.sigProgress.emit(100)
            print('Work done!', tStart.elapsed())
        else:
            print('Work stopped!', tStart.elapsed())

        self.sigWorkDone.emit()


app = Qt.QApplication([])

worker = Worker()
worker.setWorkDuration(3)

class myEnum(enum.Enum):
    AA = enum.auto()
    BB = enum.auto()
    CC = enum.auto()

myEnumColors = {
    myEnum.AA: 'green',
    myEnum.BB: '#FF0000',
    myEnum.CC: 'light blue',
}

data = OrderedDict(
       {'int': 55,
        'float': 123.456,
        'start': Action(icon='item-object'),
        'work': Progress(),
        'checkable': Action(checkbox=True),
        'enum': myEnum.AA,
        'exclusive': [Actions([Action(text='E1', radio=True),
                      Action(text='E2', radio=True),
                      Action(text='E3', radio=True)], exclusive=True)],
        '3 actions': [Actions([Action(icon=Qt.QStyle.SP_MediaPlay),
                      Action(icon=Qt.QStyle.SP_MediaStop, ),
                      Action(icon=Qt.QStyle.SP_DriveCDIcon)])],
        'mixed action': [Actions([Action(checkable=True, checked=True), Action(checkable=True), Action(checkable=False)])]})

listKeys = list(data.keys())
startIdx = listKeys.index('start')
progIdx = listKeys.index('work')
enumIdx = listKeys.index('enum')

node = Node(name='Foo', data=data)

model = QModel()
model.startModel()
idx = model.insertNode(node)

progressIdx = model.index(progIdx, 1, idx)
startIndex = model.index(startIdx, 1, idx)
enumIndex = model.index(enumIdx, 1, idx)

startNode = node.child(startIdx)
progressNode = node.child(progIdx)
enumNode = node.child(enumIdx)

def start_work(*args, **kwargs):
    if kwargs.get('data'):
        startNode.set_enabled(1, False)
        worker.work()

def set_progress(prog):
    model.setData(progressIdx, prog, Qt.Qt.EditRole)

startNode.set_on_change_callback(1, start_work)
worker.sigWorkDone.connect(partial(startNode.set_enabled, 1, True))
worker.sigProgress.connect(set_progress)

win = Qt.QMainWindow()

treeA = TreeView()
treeA.setModel(model)

treeB = TreeView()
treeB.setModel(model)

wid = Qt.QWidget()
layout =Qt.QHBoxLayout(wid)
layout.addWidget(treeA)
layout.addWidget(treeB)

index = model.index(0, 0)
treeA.expand(index)
treeB.expand(index)

panel = Qt.QWidget()
form = Qt.QFormLayout(panel)

for row, (key, value) in enumerate(data.items()):
    index = model.index(row, 1, idx)
    form.addRow(key, QCellWidget(index))

diode = cellwids.QCellDiodeWidget(enumNode.cell(1))
diode.setColorDict(myEnumColors)
form.addRow('MyEnum', diode)

layout.addWidget(panel)

tBar = win.addToolBar('ActionToolBar')
for row, (key, value) in enumerate(data.items()):
    index = model.index(row, 1, idx)
    tBar.addAction(cellwids.QCellWidgetAction(index, tBar))
    tBar.addSeparator()

win.setCentralWidget(wid)
win.show()

app.exec_()