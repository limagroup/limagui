# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"

from functools import partial
from collections import OrderedDict

from weakref import WeakSet, proxy
from silx.utils.weakref import WeakMethod


_observers = OrderedDict()


def add_observer(name, observer):
    global _observers

    if name in _observers:
        raise ValueError('An observer with this name has already been'
                         'registered.')

    _observers[name] = observer


def get_observer(name=None):
    global _observers
    if not _observers:
        raise ValueError('No observer has been registered yet.')
    if name:
        return _observers[name]
    return _observers[list(_observers.keys())[0]]


def Observer(*args, **kwargs):
    oname = kwargs.pop('oname', None)
    obs_class = get_observer(name=oname)
    return obs_class(*args, **kwargs)


class ObserverBase(object):
    uid = property(lambda self: self._uid)

    subjects = property(lambda self: self._subjects)

    def __init__(self, *cb_args, parent=None, uid=None, callback=None, **cb_kwargs):
        super(ObserverBase, self).__init__()

        if parent is not None:
            self._parent = proxy(parent)
        else:
            self._parent = None

        self._has_cb = False
        self._uid = (uid is not None and uid) or str(id(self))
        self._subjects = WeakSet()

        self.set_callback(callback, *cb_args, **cb_kwargs)

    def _cb_del(self, *args, **kwargs):
        self.set_callback(None)

    def register(self, subject):
        if subject in self._subjects:
            return
        subject.register(self)
        self._subjects.add(subject)

    def notify(self, *args, **kwargs):
        cb = self.__callback and self.__callback()
        if cb:
            cb(*args, **kwargs)

    def unregister(self, subject):
        subject.unregister(self)
        self._subjects.discard(subject)

    def clear(self):
        # TODO : subjects is a weakset, do we need to make sure
        # that no subject is destroyed during this loop?
        for subject in self._subjects:
            subject.unregister(self)
        self._subjects.clear()

    def set_callback(self, callback, *cb_args, **cb_kwargs):
        if callback is not None:
            self._has_cb = True
            if cb_args or cb_kwargs:
                self.__callback = partial(WeakMethod(callback, callback=self._cb_del),
                                          *cb_args, **cb_kwargs)
            else:
                self.__callback = WeakMethod(callback, callback=self._cb_del)
        else:
            self.__callback = None
