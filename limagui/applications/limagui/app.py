# coding: utf-8
from __future__ import absolute_import

import sys
import argparse
from collections import namedtuple, OrderedDict
from collections import deque

from ruamel.yaml import YAML

from limagui.paramtree.tree.ionamesenum import get_io_names_enum

################################################################
# QAD workaround a bug (?) that prevents you from importing Lima
# if Qt is imported first.
from Lima import Core
################################################################


from limagui.paramtree.qt import Qt

# from .limagui.widgets.mainwindow import LimaMainWindow
from .limagui.widgets.limaguimainwindow import LimaGuiMainWindow


LimaArgs = namedtuple('LimaArgs', ['camera_type', 'camera_name',
                                   'camera_args', 'config_file',
                                   'settings', 'read_only'],
                      defaults=5 * [None])


# def list_io_enums(plugin_name):
#     # loading the plugin should load the
#     # file where the enums are defined
#     plugin = get_plugin(plugin_name)
#     io_enums_groups = registered_io_names_enums(plugin.name)
#     for group in io_enums_groups:
#         io_enum = get_io_names_enum(plugin.name, group)
#         print('#### {0}'.format(group))
#         for value in list(io_enum):
#             print('     - {0}'.format(value.name))


def load_settings(file_name):
    cams_args_list = []
    settings_dict = {}

    yaml = YAML(typ='safe')

    with open(file_name) as cfg:
        cfg_dict = yaml.load(cfg)

    for item in cfg_dict:
        cam_name = item.get('camera_name')
        cam_type = item.get('camera_type')
        cam_args = item.get('camera_args')
        read_only = item.get('read_only')
        bpm_win = item.get('bpm_win')
        screen_io = item.get('screen_io')
        settings = item.get('settings')

        if settings:
            settings_dict[cam_name] = settings

        cams_args_list.extend(['-n', cam_name])

        if read_only:
            cams_args_list.extend(['-r'])
        
        cams_args_list.extend([cam_type])

        if cam_args:
            cams_args_list.extend(cam_args)

        if bpm_win:
            cams_args_list.extend(['--bpm'])

        if screen_io:
            if screen_io is True:
                cams_args_list.extend(['--scr'])
            else:
                cams_args_list.extend(['--scr', f'{screen_io}'])

        cams_args_list.extend([','])

    return cams_args_list, settings_dict


# def _last_idx(args):
#     idx = 0
#     last_idx = len(args) - 1
#     while idx < last_idx:
#         item = args[idx]
#         if item.startswith('-'):
#             try:
#                 last_idx = args[idx+1:].index(item) + idx
#             except IndexError:
#                 pass
#             except ValueError:
#                 pass
#         idx += 1
#     return last_idx


def parse_lima_args(args=None):
    settings_dict = {}
    if args is None:
        args = sys.argv[1:]

    parser = argparse.ArgumentParser(prog='limagui',
                                     description='Limagui')

    parser.add_argument('-c', help='Config file',
                        type=str,
                        dest='config_file',
                        metavar='config_file',
                        nargs=1,
                        default=None)
    parser.add_argument('-r', help='Start in read only mode',
                        dest='read_only',
                        action='store_true',
                        default=False)
    # parser.add_argument('-e', help='List plugin settings',
    #                     type=str, dest='list_enums',
    #                     metavar='list_enums',
    #                     nargs=1,
    #                     default=None)
    parser.add_argument('-n', help='Camera name',
                        dest='camera_name', metavar='camera_name',
                        nargs='?', default=None,
                        type=str)
    # parser.add_argument('cam_type', help='cam_type', metavar='camera_type')
    # parser.add_argument('next', nargs=argparse.REMAINDER)
    sub = parser.add_subparsers(help='Camera type',
                                metavar='camera_type')

    # sub_parser = sub.add_parser('cfg', help='Config file')
    # sub_parser.add_argument('path', help='Path to file')
    # sub_parser.set_defaults(camera_type='config')

    sub_parser = sub.add_parser('simulator',
                                help='Lima Simulator')
    sub_parser.set_defaults(camera_type='Simulator')

    sub_parser = sub.add_parser('prosilica', help='Prosilica camera')
    sub_parser.add_argument('ip', help='Prosilica IP')
    sub_parser.set_defaults(camera_type='Prosilica')

    sub_parser = sub.add_parser('basler', help='Basler camera')
    sub_parser.add_argument('ip', help='Basler IP')
    sub_parser.set_defaults(camera_type='Basler')

    sub_parser = sub.add_parser('v4l2', help='V4L2 camera')
    sub_parser.add_argument('device', help='camera device')
    sub_parser.set_defaults(camera_type='V4l2')

    sub_parser = sub.add_parser('tango', help='remote LimaCCDs server')
    sub_parser.add_argument('tango_name', help='LimaCCDs tango device name')
    sub_parser.add_argument('--scr', help=('Add BPM screen button, or reads it '
                                        'from the LimaCCDs server if no value '
                                        'is provided.'),
                            nargs='?', default=False,
                            const=True,
                            dest='screen_io')
    sub_parser.add_argument('--bpm', help=('Adds a window do display the BpmServer results, '
                                           'if available.'),
                            default=False,
                            action='store_true',
                            dest='bpm_win')
    sub_parser.set_defaults(camera_type='tango')

    # WARNING!
    # if _lg_test_ in present it will consume all the remaining arguments!
    sub_parser = sub.add_parser('_lg_test_')
    sub_parser.add_argument('camera_class')
    sub_parser.add_argument('camera_args', nargs='*')
    sub_parser.set_defaults(camera_type='_lg_test_')

    # print(sub)
    # exit(0)

    if len(args) == 0:
        parser.print_help()

    camera_args_list = []

    args_list = deque([sub_args.split() for sub_args in ' '.join(args).replace(';', ',').split(',')])

    # for args in args_list:
    while args_list:
        try:
            args = args_list.popleft()
        except ValueError:
            break
        # while(args):

        print('========================')
        print(f'Parsing args:', ' '.join(args))
        print('========================')
        
        parsed_args = parser.parse_known_args(args)
        if parsed_args[0].config_file:
            print('#############################################################')
            print('#############################################################')
            print('#### Config file provided.')
            cfg_args, settings_dict = load_settings(parsed_args[0].config_file[0])
            print('#### Converted config file to command line:')
            print('#### [{0}]'.format(' '.join(args)))
            print('#############################################################')
            print('#############################################################')
            cfg_args_list = [sub_args.split() for sub_args in ' '.join(cfg_args).replace(';', ',').split(',') if sub_args]
            args_list.extend(cfg_args_list)
            continue
        try:
            if not parsed_args[0]:
                raise RuntimeError(f'Failed to parse args: {parsed_args[1]}.')
            # remainder = parsed_args[1]
            parsed_args = parsed_args[0]
            camera_args = OrderedDict([(key, value)
                                    for key, value in vars(parsed_args).items()
                                    if key not in ('camera_type',
                                                'camera_name',
                                                'config_file',
                                                'list_enums',
                                                'read_only')])
            settings = settings_dict.get(parsed_args.camera_name) if parsed_args.camera_name else None
            lima_args = LimaArgs(camera_type=parsed_args.camera_type,
                                camera_name=parsed_args.camera_name,
                                camera_args=camera_args,
                                settings=settings,
                                read_only=parsed_args.read_only)
            camera_args_list.append(lima_args)
        except AttributeError as ex:
            # QAD fix for help not being displayed when no paramters are passed when calling
            # limagui as a module
            print(ex)
            parser.print_help()
            exit(1)

        # args = remainder

    return camera_args_list


def limagui_main(args=None):

    cam_args_list = parse_lima_args(args)

    if not cam_args_list:
        exit(0)

    app = Qt.QApplication([])
    win = LimaGuiMainWindow()

    # if len(cam_args_list) == 1 and cam_args_list[0].config_file:
    #     cam_args_list, settings_dict = load_settings(cam_args_list[0].config_file)

    for cam_idx, cam_args in enumerate(cam_args_list):
        camera_type = cam_args.camera_type
        camera_args = cam_args.camera_args
        camera_name = cam_args.camera_name
        read_only = cam_args.read_only
        screen_io = False

        if camera_type == '_lg_test_':
            camera_type = cam_args.camera_args['camera_class']
            camera_args = cam_args.camera_args['camera_args']
        else:
            if camera_type == 'tango':
                screen_io = camera_args.pop('screen_io', False)
                bpm_win = camera_args.pop('bpm_win', False)
            else:
                screen_io= False
                bpm_win = False
            camera_args = camera_args.values()

        print(' #### Adding camera type={0}, args={1}.'
              ''.format(camera_type, camera_args))

        name = win.addCamera(name=camera_name, cam_type=camera_type,
                             screen_io=screen_io, cam_args=camera_args,
                             read_only=read_only, bpm_win=bpm_win)

        settings_dict = cam_args.settings
        if settings_dict:
            io_group = win.getCamera(name).io_group()
            # for group_name, params in settings_dict.items():
            group_name = 'lima'
            for enum_name, enum_value in settings_dict.items():
                print(group_name, enum_name, enum_value)
                try:
                    io_names_enum = get_io_names_enum(group_name, enum_name)
                except KeyError:
                    print(f'ERROR: enum {group_name}.{enum_name} not found.')
                    continue
                for param_name, param_value in enum_value.items():
                    io_name = io_names_enum[param_name]
                    io = io_group.io_adapter(io_name)
                    if io is None:
                        raise RuntimeError('Io not found: {0}.'
                                           ''.format(io_name.io_name))
                    data = io.data()
                    print(f'{name} setting: {io.io_name} to {param_value} (current: {data}).')
                    io.set_data(param_value)

    win.show()

    app.exec_()

  
if __name__ == '__main__':
    limagui_main()