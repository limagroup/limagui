from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"

from limagui.paramtree.qt import Qt, getQIcon

from limagui.paramtree.qt.tree.widgets.widgets import (QCellToolButtonMenu,
                                                       QCellDiodeWidget,
                                                       QCellWidgetAction,
                                                       QCellActionWidget)

from ..tango_io.plugins.bpmdeviceserver import BpmDeviceServerNames

from tango import DevState


class BpmDeviceServerToolbar(Qt.QToolBar):
    def __init__(self, ioModel, parent=None):
        super(BpmDeviceServerToolbar, self).__init__(parent)

        title = 'Lima BpmDeviceServer'
        self.setWindowTitle(title)
        self.setToolTip(title)
        self.setStatusTip(title)

        start = ioModel.get_io_instance(BpmDeviceServerNames.START)
        stop = ioModel.get_io_instance(BpmDeviceServerNames.STOP)
        state = ioModel.get_io_instance(BpmDeviceServerNames.STATE)
        lut = ioModel.get_io_instance(BpmDeviceServerNames.LUT)
        autoscale = ioModel.get_io_instance(BpmDeviceServerNames.AUTOSCALE)
        colormap = ioModel.get_io_instance(BpmDeviceServerNames.COLORMAP)

        self.addWidget(Qt.QLabel('Bpm'))
        self.addSeparator()

        colors = {DevState.ON: 'green',
                  DevState.OFF: 'red'}
        stateWid = QCellDiodeWidget(state, parent=self)
        stateWid.setColorDict(colors)
        self.addWidget(stateWid)

        startBn = QCellActionWidget(start, self)
        icon = getQIcon(Qt.QStyle.SP_MediaPlay)
        startBn.setIcon(icon)
        self.addWidget(startBn)

        stopBn = QCellActionWidget(stop, self)
        icon = getQIcon(Qt.QStyle.SP_MediaStop)
        stopBn.setIcon(icon)
        self.addWidget(stopBn)

        lutWid = QCellToolButtonMenu(lut, parent=self)
        self.addWidget(lutWid)

        colormapWid = QCellWidgetAction(colormap, self)
        colormapWid.setIcon(icon)
        self.addAction(colormapWid)

        autoscaleWid = QCellWidgetAction(autoscale, self)
        autoscaleWid.setIcon(icon)
        self.addAction(autoscaleWid)
