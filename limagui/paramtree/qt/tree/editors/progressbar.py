# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from ....tree.types.progress import Progress

from ... import Qt

from ..editor import QEditorMixin, qeditordef
from ..widgets.qwidgetmixin import QCellWidgetMixin


@qeditordef(data_type=Progress)
class ProgressBar(QEditorMixin, QCellWidgetMixin, Qt.QWidget):

    @classmethod
    def paintDelegate(cls, painter, option, index):
        progress = index.data(Qt.Qt.EditRole)
        progressBarOption = Qt.QStyleOptionProgressBar()
        progressBarOption.rect = option.rect
        progressBarOption.minimum = 0
        progressBarOption.maximum = 100
        progressBarOption.progress = progress
        progressBarOption.text = '{0}%'.format(progress)
        progressBarOption.textVisible = True
        Qt.QApplication.style().drawControl(Qt.QStyle.CE_ProgressBar,
                                            progressBarOption,
                                            painter)
        return True
