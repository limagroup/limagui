# coding: utf-8
from __future__ import absolute_import

from limagui.paramtree.utils.observer import Observer

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


import re
from typing import NamedTuple, Any
from collections import OrderedDict, namedtuple
from contextlib import contextmanager

from .ionamesenum import IoNamesEnum

from ..utils.subject import Subject
from ..utils.scheduler import Scheduler
from enum import Enum, EnumMeta

import gc

_registered_ioadapters = OrderedDict()

######################################################################
######################################################################

IoNameInfo = namedtuple('IoNameInfo', ['group', 'source', 'path'])

def parse_io_name(io_name):
    if isinstance(io_name, (IoNamesEnum,)):
        io_name = io_name.io_name
    rx = re.compile('^((?P<group>[^@:]*)@)?'
                    '((?P<source>[^@:]*):)?'
                    '((?P<path>[^@:]+))$')
    m = rx.match(io_name)
    if not m:
        raise ValueError("io name does not match the expected patten :"
                         " {0}".format(io_name))

    return IoNameInfo(**m.groupdict())


def register_io_adapter(cls):
    if cls.io_name in _registered_ioadapters:
        raise ValueError("io_name {0} is already registered."
                         "".format(cls.io_name))
    # checking the io_name pattern
    parse_io_name(cls.io_name)
    _registered_ioadapters[cls.io_name] = cls


def io_name_is_registered(io_name):
    return io_name in _registered_ioadapters

def get_io_adapter_class(io_name):
    try:
        return _registered_ioadapters[io_name]
    except KeyError:
        return None


def registered_io_adapters():
    return _registered_ioadapters.values()


def registered_io_names():
    return list(_registered_ioadapters.keys())


class IoInfo(NamedTuple):
    io_name: Any = None
    io_getter: Any = None
    io_setter: Any = None
    data_type: Any = None
    start_fn: Any = None
    stop_fn: Any = None
    triggers: Any = None
    enable_conditions: Any = None
    sink: bool = False
    hidden: Any = False
    register: Any = True
    poll_ms: Any = None
    description: Any = None
    icon: Any = None
    units: Any = None
    view_options: Any = None
    colors: Any = None
    default_value: Any = None
    cls: Any = None
    kwargs: Any = None


def create_io_class(io_name=None,
                    io_getter=None,
                    io_setter=None,
                    data_type=None,
                    start_fn=None,
                    stop_fn=None,
                    triggers=None,
                    enable_conditions=None,
                    sink=False,
                    hidden=False,
                    poll_ms=None,
                    description=None,
                    icon=None,
                    units=None,
                    view_options=None,
                    colors=None,
                    default_value=None,
                    cls=None,
                    register=True,
                    **kwargs):
    io_obj = IoInfo(io_name=io_name,
                    io_getter=io_getter,
                    io_setter=io_setter,
                    data_type=data_type,
                    start_fn=start_fn,
                    stop_fn=stop_fn,
                    triggers=triggers,
                    enable_conditions=enable_conditions,
                    sink=sink,
                    hidden=hidden,
                    poll_ms=poll_ms,
                    description=description,
                    icon=icon,
                    units=units,
                    view_options=view_options,
                    default_value= default_value,
                    colors=colors,
                    cls=cls,
                    kwargs=kwargs)
    if register:
        register_io_adapter(io_obj)
    return io_obj


def ioadapterdef(*args, **kwargs):
    def inner(cls):
        return create_io_class(*args, **kwargs, cls=cls)
    return inner


def _to_set(value):
    if value is None:
        return set()
    if not isinstance(value, (tuple, list, set,)):
        return set([value])
    return set(value)


def IoAdapter(name=None, instance=True, **kwargs):
    if name:
        io_info = get_io_adapter_class(name)
        if not instance:
            return io_info
        if io_info.kwargs:
            kwargs = dict(**io_info.kwargs, **kwargs)
        if io_info.cls:
            return io_info.cls(**io_info._asdict(), **kwargs)
        return IoAdapterBase(**io_info._asdict(), **kwargs)
    if not instance:
        return IoAdapterBase
    return IoAdapterBase(**kwargs)


def setter_notify(func):
    def wrapper(self, *args, _internal=False, _notify=True, _chain=None, **kwargs):
        if _notify is not False:
            with self._lock_notify(_internal=_internal, _chain=_chain):
                func(self, *args, _internal=_internal, **kwargs)
        else:
            func(self, *args, _internal=_internal, **kwargs)
    return wrapper


# TODO: tests
class Conditions:
    def __init__(self, conditions):
        self._conditions = OrderedDict({io_name:(idx, fn)
                            for idx, (io_name, fn) in enumerate(conditions.items())})
        self._enabled = [True] * len(conditions)

    def on_trigger(self, sender, data):
        if data is None:
            return None
        cond = self._conditions.get(sender)
        if not cond:
            return None
        enable = cond[1](data)
        self._enabled[cond[0]] = enable
        return all(self._enabled)
    
    def enabled(self):
        return all(self._enabled)

    def io_names(self):
        return list(self._conditions.keys())
        

def _pprint_chain(chain):
    for k, v in chain.items():
        print(f'- {k}: ', '\n  - '.join([str(i) for i in v]))


class IoAdapterBase:

    io_name = property(lambda self: self._io_name)

    def __init__(self,
                 data=None,
                 data_type=None,
                 editable=True,
                 checkable=False,
                 selectable=True,
                 io_getter=None,
                 io_setter=None,
                 start_fn=None,
                 stop_fn=None,
                 source=None,
                 io_name=None,
                 poll_ms=None,
                 io_group=None,
                 triggers=None,
                 enable_conditions=None,
                 sink=False,
                 view_options=None,
                 colors=None,
                 default_value=None,
                 substitute=None,
                 start=False,
                 **kwargs):

        # TODO: weak refs?
        self._substitute_io = None
        self._io_setter = io_setter
        self._io_getter = io_getter
        self._source = source
        self._io_group = io_group
        self._trigger_ios = {}

        self._sink = sink

        self._poller = None
        self._poll_ms = poll_ms

        if data is None:
            data = default_value

        self._data_type = None
        if data_type is not None:
            self._data_type = data_type
        elif data is not None:
            self._data_type = data.__class__
        
        self._lock = 0
        self._started = False

        self._io_name = io_name

        self._subject = Subject(parent=self, uid=self._io_name)
        self._observer = Observer(parent=self,
                                  callback=self._trigger_event,
                                  uid=self._io_name)
        self._view_subject = Subject(parent=self, uid=self._io_name)

        drag_enabled = False
        drop_enabled = False
        checked = False

        self._cache = {}
        self._dirty = {}

        self._io_getter = io_getter
        self._io_setter = io_setter

        self._start_fn = start_fn
        self._stop_fn = stop_fn

        data = self._init_data(data)
        self._is_init = False
        self.write(data=data,
                   drag_enabled=drag_enabled,
                   drop_enabled=drop_enabled,
                   checkable=checkable, 
                   editable=editable,
                   selectable=selectable,
                   checked=checked,
                   _internal=True)
        self._defaults = {}
        self._defaults.update(self._cache)

        if enable_conditions:
            self._enable_conditions = Conditions(enable_conditions)
        else:
            self._enable_conditions = None

        self._triggers = _to_set(triggers)

        if enable_conditions:
            self._triggers |= set(enable_conditions.keys())

        self._substitute = substitute
        if substitute:
            self._triggers.add(substitute)

        self._view_config = {
            'colors': colors,
            'options': view_options
        }
        
        self._is_init = True

        if start:
            self.start()

    def add_trigger(self, trigger):
        if not isinstance(trigger, (IoAdapterBase,)):
            trigger = self._io_group.io_adapter(trigger, hidden=True)
        if trigger:
            data = trigger.data(cache=True)
            self._trigger_event(data=data, sender=trigger.io_name)
            trigger.register_observer(self._observer)
            self._trigger_ios[trigger.io_name] = trigger

    def _trig_observers(self, **kwargs):
        self._subject.notify(**kwargs)

    def _poll_event(self, **kwargs):
        self._trigger_event(sender='POLL', **kwargs)

    def _trigger_event(self, data=None, _chain=None, sender=None, **kwargs):
        if _chain is not None:
            rc = _chain.add_event(self.io_name, sender)
        else:
            rc = 2

        if rc == 0 and not self._sink:
            return

        enabled = None
        if self._enable_conditions:
            enabled = self._enable_conditions.on_trigger(sender, data)
            if enabled == self.get_params('enabled'):
                enabled = None

        _first_trig = rc == 2

        with self._lock_notify(_chain=_chain):
            if enabled is not None:
                self.set_data(enabled=enabled)

            if sender is not None and sender == self._substitute and rc in (1, 2):
                self.set_data(data, _internal=True)
            else:
                self._on_trigger(sender=sender,
                                 _chain=_chain,
                                 _first_trig=_first_trig,
                                 data=data,
                                 **kwargs)

    def _on_trigger(self, sender=None, _first_trig=True, _chain=None, **kwargs):
        if not _first_trig:
            return
        if self._substitute_io:
            try:
                if _chain:
                    _chain.get_kwargs(self._substitute)
                else:
                    raise KeyError()
            except KeyError:
                self._substitute_io.data(cache=False, _chain=_chain)
            else:
                data = self._substitute_io.data(cache=True, _chain=_chain)
                self.set_data(data, _internal=True)
            return

        self.data(cache=False, _chain=_chain)
        self._dirty['_trig'] = None

    def io_group(self):
        return self._io_group

    def source(self):
        return self._source

    def started(self):
        return self._started

    def start(self):
        if self._started:
            return
        if self._substitute:
            self._substitute_io = self._io_group.io_adapter(self._substitute)
        self._on_start()
        self.set_data(enabled=True)
        # print('&&&')
        self.data(cache=False, _notify=False)
        # if self.editable():
        #     self.set_editable(not self.read_only())

        if self._triggers:
            for trigger in self._triggers:
                self.add_trigger(trigger)

        enabled = True
        if self._enable_conditions:
            for io_name in self._enable_conditions.io_names():
                cond_io = self._io_group.io_adapter(io_name)
                self._enable_conditions.on_trigger(io_name,
                                                   cond_io.data(cache=True))
            enabled = self._enable_conditions.enabled()
        self.set_data(enabled=enabled, _internal=True)

        if self._start_fn:
            self._start_fn(self)

        self._started = True
        self.set_poll_time(self._poll_ms)

    def stop(self):
        if not self._started:
            return
        self._substitute_io = None
        if self._poller:
            self._poller.stop()
            self._poller = None

        # self.set_enabled(False, _internal=True)

        if self._triggers:
            self._observer.clear()
        self._trigger_ios.clear()

        if self._stop_fn:
            self._stop_fn(self)
        self._on_stop()
        self._started = False

    def register_observer(self, observer):
        self._subject.register(observer)

    def register_view(self, observer):
        self._view_subject.register(observer)

    def _notify_view(self, **kwargs):
        self._view_subject.notify(**kwargs)

    def set_poll_time(self, poll_ms):
        if self._poller:
            self._poller.stop()
            self._poller = None
            self._poll_ms = None
        if poll_ms and poll_ms > 0:
            self._poller = Scheduler(self._poll_event,
                                     poll_ms,
                                     single_shot=False,
                                     cb_kwargs={'cache':False})
            self._poll_ms = poll_ms
            self._poller.start()

    @contextmanager
    def _lock_notify(self, _internal=False, _chain=None, **kwargs):
        # print('LOCK', self.io_name)
        self._lock += 1
        if self._lock == 1:
            self._dirty = {}
        try:
            yield
        except Exception as ex:
            print(f'In {self.io_name} : {ex}')
            # import traceback
            # traceback.print_stack()
            # print(__file__)
            # print(ex)
        finally:
            self._lock -= 1
            if self._lock == 0 and self._started:
                self._data_changed(_chain=_chain, **kwargs)

    def _notify(self, **kwargs):
        with self._lock_notify(**kwargs):
            pass

    def _data_changed(self, _chain=None, **kwargs):
        if self._is_init and self._started and self._dirty or kwargs:
            dirty = self._dirty
            self._dirty = {}
            if kwargs:
                dirty = {**kwargs, **dirty}
            if self._io_group:
                self._io_group._trigger(self, _chain=_chain, **dirty)
            else:
                self._subject.notify(sender=self.io_name, _chain=_chain, **dirty)
                self._view_subject.notify(**dirty)

    def refresh(self, notify=True):
        if notify:
            with self._lock_notify():
                self._dirty.update(self._cache)
        return self._cache

    def view_config(self):
        return self._view_config

    def _update_cache(self, reset=None, data=None, **kwargs):
        if reset:
            self._dirty = {'reset':True}
            self._cache = {'reset':True}
            self._dirty.update(self._defaults)
            self._cache.update(self._defaults)
        if data is not None:
            self._dirty['data'] = data
            self._cache['data'] = data
        self._dirty.update(kwargs)
        self._cache.update(kwargs)

    @setter_notify
    def write(self,
            #   data=None,
            #   tooltip=None,
            #   decoration=None,
            #   foreground_color=None,
            #   background_color=None,
            #   drop_enabled=None,
            #   drag_enabled=None,
            #   enabled=None,
            #   editable=None,
            #   checked=None,
            #   checkable=None,
            #   selectable=None,
              reset=None,
               _internal=False,
               **kwargs):
        self.set_data(**kwargs, _internal=_internal)
        # if data is not None:
        #     self.set_data(data, _internal=_internal)
        # if tooltip is not None:
        #     self.set_tooltip(tooltip, _internal=_internal)
        # if decoration is not None:
        #     self.set_decoration(decoration, _internal=_internal)
        # if foreground_color is not None:
        #     self.set_foreground_color(foreground_color, _internal=_internal)
        # if background_color is not None:
        #     self.set_background_color(background_color, _internal=_internal)
        # if drop_enabled is not None:
        #     self.set_drop_enabled(drop_enabled, _internal=_internal)
        # if drag_enabled is not None:
        #     self.set_drag_enabled(drag_enabled, _internal=_internal)
        # if enabled is not None:
        #     self.set_enabled(enabled, _internal=_internal)
        # if editable is not None:
        #     self.set_editable(editable, _internal=_internal)
        # if checked is not None:
        #     self.set_checked(checked, _internal=_internal)
        # if checkable is not None:
        #     self.set_checkable(checkable, _internal=_internal)
        # if selectable is not None:
        #     self.set_selectable(selectable, _internal=_internal)

        if reset:
            self.reset()

    # def __getattribute__(self, name):
    #     print(name)
    #     # print(self._cache)
    #     return super().__getattribute__(name)

    def get_params(self, name=None):
        if name:
            return self._cache.get(name)
        else:
            return self._cache.copy()

    @setter_notify
    def reset(self, *args, **kwargs):
        self._update_cache(reset=True)

    def data(self, cache=True, **kwargs):
        data = None
        _chain = kwargs.pop('_chain', None)
        _notify = kwargs.pop('_notify', True)
        if not cache:
            if self._substitute_io is not None:
                return self._substitute_io.data(cache=cache, **kwargs)
            if not self.write_only():
                data = self._read_data(self._source, **kwargs)
        else:
            data = self._cache.get('data') 

        if data is None and not self.write_only():
            if self._substitute_io is not None:
                return self._substitute_io.data(cache=cache, **kwargs)
            else:
                data = self._read_data(self._source, **kwargs)
        self.set_data(data, _internal=True, _notify=not cache and _notify, _chain=_chain)
        return data

    # def tooltip(self, cache=True):
    #     if cache:
    #         return self._cache.get('tooltip')
    #     return self._read_tooltip()

    # def decoration(self, cache=True):
    #     if cache:
    #         return self._cache.get('decoration')
    #     return self._read_decoration()

    # def foreground_color(self, cache=True):
    #     if cache:
    #         return self._cache.get('foreground_color')
    #     return self._read_foreground_color()

    # def background_color(self, cache=True):
    #     if cache:
    #         return self._cache.get('background_color')
    #     return self._read_background_color()

    # def drop_enabled(self, cache=True):
    #     if cache:
    #         return self._cache.get('drop_enabled')
    #     return self._read_drop_enabled()

    # def drag_enabled(self, cache=True):
    #     if cache:
    #         return self._cache.get('drag_enabled')
    #     return self._read_drag_enabled()

    # def enabled(self, cache=True):
    #     if cache:
    #         enabled = self._cache.get('enabled')
    #         if enabled is None:
    #             return self._read_enabled()
        return self._read_enabled()

    # def editable(self, cache=True):
    #     return not self.read_only()
        # if cache:
        #     return self._cache.get('editable')
        # return self._read_editable()

    # def checked(self, cache=True):
    #     if cache:
    #         return self._cache.get('checked')
    #     return self._read_checked()

    # def checkable(self, cache=True):
    #     if cache:
    #         return self._cache.get('checkable')
    #     return self._read_checkable()

    # def selectable(self, cache=True):
    #     if cache:
    #         return self._cache.get('selectable')
    #     return self._read_selectable()

    # @setter_notify
    # def set_background_color(self, background_color, _internal=False):
    #     if _internal:
    #         self._update_cache(background_color=background_color)
    #     else:
    #         self.set_background_color(background_color, _internal=True)

    # @setter_notify
    # def set_tooltip(self, tooltip, _internal=False):
    #     if _internal:
    #         self._update_cache(tooltip=tooltip)
    #     else:
    #         self.set_tooltip(tooltip, _internal=True)

    # @setter_notify
    # def set_decoration(self, decoration, _internal=False):
    #     if _internal:
    #         self._update_cache(decoration=decoration)
    #     else:
    #         self.set_decoration(decoration, _internal=True)

    # @setter_notify
    # def set_foreground_color(self, foreground_color, _internal=False):
    #     if _internal:
    #         self._update_cache(foreground_color=foreground_color)
    #     else:
    #         self.set_foreground_color(foreground_color, _internal=True)

    # @setter_notify
    # def set_drag_enabled(self, drag_enabled, _internal=False):
    #     if _internal:
    #         self._update_cache(drag_enabled=drag_enabled)
    #     else:
    #         self.set_drag_enabled(drag_enabled, _internal=True)

    # @setter_notify
    # def set_drop_enabled(self, drop_enabled, _internal=False):
    #     if _internal:
    #         self._update_cache(drop_enabled=drop_enabled)
    #     else:
    #         self.set_drop_enabled(drop_enabled, _internal=True)

    # @setter_notify
    # def set_enabled(self, enabled, _internal=False):
    #     if _internal:
    #         self._update_cache(enabled=enabled)
    #     else:
    #         self.set_enabled(enabled, _internal=True)

    # @setter_notify
    # def set_editable(self, editable, _internal=False):
    #     if _internal:
    #         self._update_cache(editable=editable)
    #     else:
    #         self.set_editable(editable, _internal=True)

    # @setter_notify
    # def set_checked(self, checked, _internal=False):
    #     if _internal:
    #         self._update_cache(checked=checked)
    #     else:
    #         self.set_checked(checked, _internal=True)

    # @setter_notify
    # def set_checkable(self, checkable, _internal=False):
    #     if _internal:
    #         self._update_cache(checkable=checkable)
    #     else:
    #         self.set_checkable(checkable, _internal=True)

    # @setter_notify
    # def set_selectable(self, selectable, _internal=False):
    #     if _internal:
    #         self._update_cache(selectable=selectable)
    #     else:
    #         self.set_selectable(selectable, _internal=True)

    def set_data_internal(self, *args, _internal=True, **kwargs):
        """
        Helper function that just calls set_data with the _internal keyword set.
        """
        print('SDI', args, kwargs)
        return self.set_data(*args, _internal=_internal, **kwargs)

    @setter_notify
    def set_data(self, data=None, _internal=False, **kwargs):
        # TODO: is this OK?
        if isinstance(data, (Enum, EnumMeta,)):
            data = data.value
        if _internal:
            self._update_cache(data=data, **kwargs)
        else:
            if data is None:
                self._update_cache(**kwargs)
            else:
                if self._substitute_io:
                    self._substitute_io.set_data(data=data, **kwargs)
                elif self._io_setter and data is not None:
                    set_data = self._io_setter(self._source, data)
                    if set_data is None:
                        if not self.write_only():
                            set_data = self.data(cache=False)
                        else:
                            self.set_data(True, _internal=True, **kwargs)
                else:
                    self.set_data(data, _internal=True, **kwargs)

    def data_type(self):
        return self._data_type

    def _read_data(self, source):
        if self._io_getter:
            return self._io_getter(source)
        return self._cache.get('data')

    # def _read_tooltip(self):
    #     return self._cache.get('tooltip')

    # def _read_decoration(self):
    #     return self._cache.get('decoration')

    # def _read_foreground_color(self):
    #     return self._cache.get('foreground_color')

    # def _read_background_color(self):
    #     return self._cache.get('background_color')

    # def _read_drop_enabled(self):
    #     return self._cache.get('drop_enabled')

    # def _read_drag_enabled(self):
    #     return self._cache.get('drag_enabled')

    # def _read_enabled(self):
    #     return self._cache.get('enabled', True)

    # def _read_editable(self):
    #     return self._cache.get('editable')

    # def _read_checked(self):
    #     return self._cache.get('checked')

    # def _read_checkable(self):
    #     return self._cache.get('checkable')

    # def _read_selectable(self):
    #     return self._cache.get('selectable')

    def _init_data(self, data):
        return data

    def _on_start(self):
        pass

    def _on_stop(self):
        pass

    def read_only(self):
        has_g = self._io_getter is not None
        has_s = self._io_setter is not None
        return has_g and not has_s

    def write_only(self):
        has_g = self._io_getter is not None
        has_s = self._io_setter is not None
        return has_s and not has_g
