# -*- coding: utf-8 -*-

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"


import numpy as np

from limagui.paramtree.tree.ioadapter import IoAdapterBase

from .treeadapter import TreeAdapterBase


class SubListTreeAdapter(TreeAdapterBase):
    def __init__(self, node, data, sub_data=None, sub_names=None, **kwargs):
        if sub_data is None:
            if isinstance(data, (tuple, list, np.ndarray,)):
                sub_data = data
            elif isinstance(data, IoAdapterBase):
                sub_data = self._root_to_sub(data.data(cache=True))
            else:
                sub_data = self._root_to_sub(data)
                # raise RuntimeError('Expected sub_data.')

        self._data_len = len(sub_data)
        self._sub_data = sub_data

        if sub_names is None:
            sub_names = [f'{i}' for i in range(self._data_len)]
        self._sub_names = sub_names

        if len(self._sub_names) != self._data_len:
            raise RuntimeError(f'In {__file__}: mismatch.')

        super(SubListTreeAdapter, self).__init__(node, data, **kwargs)

    def _sort_data(self, data):
        cell_data, node_data = super(SubListTreeAdapter, self)._sort_data(data)
        node_data = [{'name':name, 'data':data, 'editable':True} for name, data in zip(self._sub_names, self._sub_data)]
        self._sub_data = None
        return cell_data, node_data

    def _children_changed(self, node, cell, **kwargs):
        if self._children_lock >= 1:
            return
        data = kwargs.get('data')
        if data is None:
            return
        cell = self.cell(1)
        root_data = cell.data(cache=True)
        try:
            new_data = self._sub_to_root(root_data, node.row(), data)
        except Exception as ex:
            print(f'Ex. in {__file__}:{self.__class__.__name__}._sub_to_root {ex}')
            self._fill_sub_nodes(root_data)
        else:
            cell.set_data(new_data)

    def _fill_sub_nodes(self, data):
        try:
            sub_data = self._root_to_sub(data)
        except Exception as ex:
            print(f'TODO: Ex. in {__file__}:{self.__class__.__name__}._root_to_sub {ex}')
        else:
            with self._lock_children():
                if self._children_lock <= 1:
                    for i_child, sub_d in enumerate(sub_data):
                        self.child(i_child).cell(1).set_data(sub_d)
    
    def _data_changed(self, cell, **kwargs):
        # print('DC', cell.column(), kwargs)
        data = kwargs.get('data')
        if data is not None:
            self._fill_sub_nodes(data)
        super(SubListTreeAdapter, self)._data_changed(cell, **kwargs)

    def _sub_to_root(self, root_data, row, cell_data):
        if isinstance(root_data, (tuple,)):
            root_data = list(root_data)
        if isinstance(root_data, (list, np.ndarray,)):
            root_data = root_data[:]
        else:
            raise RuntimeError(f'Expected root_data to be a list.')
        root_data[row] = cell_data
        return root_data

    def _root_to_sub(self, root_data):
        if isinstance(root_data, (list, tuple, np.ndarray,)):
            return root_data
        return [root_data]