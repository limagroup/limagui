# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from limagui.paramtree.iomodel import get_io_class, IoNamesEnum
from limagui.paramtree.iomodel.extra.tangomixin import new_tango_io_class
from ..utils import match_io_enum_names

from ....lima.lima_io.camera.simulator import (SimulatorIoNames,
                                               RotationAxis,
                                               FillType)


# SIMULATOR_DEVICE_NAME = 'Simulator'
# SIMULATOR_SOURCE_NAME = 'TangoLimaSimulator'



class SimulatorTangoIoNames(IoNamesEnum):
    ROTATION_AXIS = 'Lima@TangoCamera:rotation_axis'
    FILL_TYPE = 'Lima@TangoCamera:fill_type'
    ROTATION_ANGLE = 'Lima@TangoCamera:rotation_angle'
    ROTATION_SPEED = 'Lima@TangoCamera:rotation_speed'
    DIFFRACTION_SPEED = 'Lima@TangoCamera:diffraction_speed'
    DIFFRACTION_POS = 'Lima@TangoCamera:diffraction_pos'
    PEAKS = 'Lima@TangoCamera:peaks'
    PEAK_ANGLES = 'Lima@TangoCamera:peak_angles'


def get_lima_tango_io_classes():
    tango_io_names = [e for e in SimulatorTangoIoNames]
    tango_io_classes = []

    for io_name in tango_io_names:
        klass = get_io_class(io_name)
        if klass is None:
            klass = new_tango_io_class(io_name, conversions=LIMA_TANGO_SIMULATOR_CONVERSIONS)
        tango_io_classes.append(klass)

    return tango_io_classes


def get_lima_tango_substitutes():
    return match_io_enum_names(SimulatorTangoIoNames, SimulatorIoNames)


# _LimaFillToTango = {FrameBuilder.Gauss: 'GAUSS',
#                     FrameBuilder.Diffraction: 'DIFFRACTION'}
# _TangoFillToLima = {v: k for k, v in _LimaFillToTango.items()}

# _LimaRotAxisToTango = {FrameBuilder.RotationX: 'ROTATIONX',
#                        FrameBuilder.RotationY: 'ROTATIONY'}
# _TangoRotAxisToLima = {v: k for k, v in _LimaRotAxisToTango.items()}


# def gausspeaks_to_list(peaks):
#     if peaks is None:
#         return []
#     return [val for p in peaks for val in [p.x0, p.y0, p.fwhm, p.max]]


# def list_to_gausspeaks(peaks):
#     if peaks is None:
#         return []
#     args = [iter(peaks)] * 4
#     return [GaussPeak(*values)
#             for values in  itertools.izip(*args)]
# (lambda s: RotationAxis[s.capitalize()].value,
#                                          lambda e:enums.AcqMode(e).name.upper())

def rot_axis_tango_to_enum(value):
    if value == 'ROTATIONX':
        return RotationAxis.RotationX
    elif value == 'ROTATIONY':
        return RotationAxis.RotationY
    return None

LIMA_TANGO_SIMULATOR_CONVERSIONS = {
    SimulatorTangoIoNames.ROTATION_AXIS: (rot_axis_tango_to_enum,
                                          lambda e: RotationAxis(e).name.upper()),
    SimulatorTangoIoNames.FILL_TYPE: (lambda s: FillType[s.capitalize()].value,
                                      lambda e: FillType(e).name.upper()),
    # SimulatorTangoIoNames.PEAKS: (list_to_gausspeaks, gausspeaks_to_list)
}


# # TODO : put this in a lib
# def _get_io_names(sub_klass, klass_to_sub):
#     io_names = [(value, key) for key, value in sub_klass.__dict__.items()
#                 if not key.startswith('__')]

#     return [(value, klass_to_sub.__dict__[key]) for value, key in io_names
#             if key in klass_to_sub.__dict__]


# @ioclassdef(SimulatorTangoIoNames.PEAKS)
# class TangoSimulatorPeaks(TangoIoMixin, IOBase):
#     tango_conversion = list_to_gausspeaks, gausspeaks_to_list

#     def io_getter(self, source):
#         data = super(TangoSimulatorPeaks, self).io_getter(source)
#         if data is None:
#             data = []
#         return data


# @ioclassdef(SimulatorTangoIoNames.PEAK_ANGLES)
# class TangoSimulatorPeakAngles(TangoIoMixin, IOBase):
#     def io_getter(self, source):
#         data = super(TangoSimulatorPeakAngles, self).io_getter(source)
#         if data is None:
#             data = []
#         return data


# @plugindef(name='LimaTangoSimulator')
# class LimaTangoSimulatorPlugin(IoPlugin):
#     def setup(self):
#         from limagui.plugins.lima.model.camera.simulator import SimulatorIo
#         from ...limatangofactory import new_tango_io_class
#         import sys

#         this_module = sys.modules[__name__]#__import__(__name__)
#         this_model = self.model()
#         lima_factory = this_model.factory('LimaTangoFactory')

#         if not lima_factory:
#             print('error: no LimaTangoFactory.')
#             return

#         simulator_dev = lima_factory.get_device('Simulator')

#         if not simulator_dev:
#             print('error: no Simulator device.')
#             return

#         this_model.set_data_source(SIMULATOR_SOURCE_NAME, simulator_dev)

#         cam_ios = get_module_ios(this_module)

#         for io_klass in cam_ios:
#             this_model.add_io_class(io_klass)

#         try:
#             substitutes = [(SimulatorIo, SimulatorTangoIoNames)]
#         except AttributeError:
#             print('No subs')
#         else:
#             subs_ios = []
#             for orig_io, cam_io in substitutes:
#                 subs_ios += _get_io_names(cam_io, orig_io)

#             for new, orig in subs_ios:
#                 if not this_model.has_io_class(new):
#                     orig_klass = get_io_class(orig)
#                     new_class = new_tango_io_class(new,
#                                                    conversions=LIMA_TANGO_SIMULATOR_CONVERSIONS)
#                     this_model.add_io_class(new_class)
#                     this_model.add_substitution(orig, new_class.io_name)
#                 else:
#                     this_model.add_substitution(orig, new)
