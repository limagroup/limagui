# coding: utf-8

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


import struct

import numpy as np

from ...lima.type.limaimage import (LimaImage,
                                                 LimaVideoImage)


_LimaCCDsImageType2NpType = {
                    0: np.uint8,
                    1: np.uint16,
                    2: np.uint32,
                    4: np.int8,
                    5: np.int16,
                    6: np.int32
                }


null_image = np.array([[0, 1, 1, 1, 0],
                       [1, 0, 1, 0, 1],
                       [1, 1, 0, 1, 1],
                       [1, 0, 1, 0, 1],
                       [0, 1, 1, 1, 0]])


def _unpack_limaccds_video_struct(struct_data):
    VIDEO_HEADER_FORMAT = '!IHHqiiHHHH'
    VIDEO_HEADER_SIZE = struct.calcsize(VIDEO_HEADER_FORMAT)
    VIDEO_MAGIC = 0x5644454f
    VIDEO_VERSIONS = [1]

    unpacked = struct.unpack(VIDEO_HEADER_FORMAT,
                             struct_data[:VIDEO_HEADER_SIZE])
    magic, h_version, mode, frame_nb, w, h, endian, h_size, pad0, pad1 =\
        unpacked[0:VIDEO_HEADER_SIZE]

    if magic != VIDEO_MAGIC:
        raise TypeError("Unsupported video magic '%s" % magic)

    if h_version not in VIDEO_VERSIONS:
        raise TypeError("Unsupported video version '%s" % h_version)

    return struct_data[h_size:], mode, w, h, frame_nb


def _unpack_limaccds_image_struct(struct_data):
    IMAGE_VERSIONS = [2]
    IMAGE_HEADER_FORMAT = '<IHHIIHHHHHHHHIIIIIIII'
    IMAGE_MAGIC = 0x44544159
    IMAGE_HEADER_SIZE = 64

    unpacked = struct.unpack(IMAGE_HEADER_FORMAT,
                             struct_data[:IMAGE_HEADER_SIZE])

    magic, h_version, h_size, category, data_type, endian = unpacked[0:6]
    n_dims = unpacked[6]
    dims = unpacked[7:13]
    steps = unpacked[13:19]
    _ = unpacked[19:21]

    if magic != IMAGE_MAGIC:
        raise ValueError('Unsupported magic : {0} (expected {1}).'
                         ''.format(magic, IMAGE_MAGIC))

    if h_version not in IMAGE_VERSIONS:
        raise TypeError('Unsupported video version {0}.'.format(h_version))

    img_dims = dims[0:n_dims]
    img_steps = steps[0:n_dims]

    return struct_data[IMAGE_HEADER_SIZE:], img_dims, img_steps, data_type


def tango_video_last_image_to_video_image(tango_data):
    if tango_data is not None:
        data = _unpack_limaccds_video_struct(tango_data[1])
        if data:
            imgbuf, mode, w, h, nb = data
            return LimaVideoImage(imgbuf, w, h, mode, number=nb)
    return None


def tango_last_image_to_lima_image(tango_data, number=-1):
    try:
        image_buf, dims, steps, img_type = \
            _unpack_limaccds_image_struct(tango_data[1])
    except Exception as ex:
        print(f'Error in tango_read_image_to_lima_image:', ex)
        return LimaImage(np.array([]), 0, 0, number=-1)

    dtype = _LimaCCDsImageType2NpType.get(img_type)

    if dtype is None:
        raise ValueError(
            'Unsupported data type : {0}.'.format(img_type))

    if len(dims) not in [0, 2]:
        raise ValueError(
            'Image stacks not supported  yet dims={0}.'.format(
                dims))

    image_data = np.ndarray(dims[::-1],
                            buffer=image_buf,
                            dtype=dtype)

    return LimaImage(image_data, dims[1], dims[0], number=number)


def tango_read_image_to_lima_image(tango_data, number=-1):
    if tango_data is not None:
        try:
            image_buf, dims, steps, img_type = \
                _unpack_limaccds_image_struct(tango_data[1])
        except Exception as ex:
            print(f'Error in tango_read_image_to_lima_image:', ex)
            return LimaImage(np.array([]), 0, 0, number=-1)
    else:
        return LimaImage(np.array([]), 0, 0, number=-1)

    dtype = _LimaCCDsImageType2NpType.get(img_type)

    if dtype is None:
        raise ValueError(
            'Unsupported data type : {0}.'.format(img_type))

    if len(dims) not in [0, 2]:
        raise ValueError(
            'Image stacks not supported  yet dims={0}.'.format(
                dims))

    image_data = np.ndarray(dims[::-1],
                            buffer=image_buf,
                            dtype=dtype)

    return LimaImage(image_data, dims[1], dims[0], number=number)


def tango_base_image_to_lima_image(tango_data):
    height, width = null_image.shape
    return LimaImage(null_image,
                     height,
                     width)
