# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from Lima import Core

from limagui.paramtree import IoAdapterBase, create_io_class, ioadapterdef


from .limaionames import HwInterface


LimaHwCapSyncValidRanges = create_io_class(HwInterface.CAP_SYNC_VALIDRANGES,
                io_getter=lambda self, hwiface: hwiface.getHwCtrlObj(Core.HwCap.Sync).getValidRanges(),
                data_type=Core.HwSyncCtrlObj.ValidRangesType)
