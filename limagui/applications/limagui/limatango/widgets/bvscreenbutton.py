import re
import tango

from limagui.paramtree.tree.extra import TangoIoAdapter
from limagui.paramtree.qt import Qt, getQIcon
from limagui.paramtree.qt.tree.widgets import QCellWidget, QCellAction
from limagui.paramtree.tree.ioadapter import IoAdapterBase


_SCREEN_TG_PROPERTY = 'bv_screen_wago'


def parse_screen_io(text):
    if not text:
        return None, None
    m = re.match('^(?P<device>[^:]*):(?P<attribute>[^:]*)$', text)

    if not m:
        print(f'ERROR, failed to parse screen wago channel: {text}.')
        return None, None
        # raise ValueError(f'Failed to parse the screen wago channel ({text}),'
        #                  f' invalid pattern.')

    mdict = m.groupdict()
    return mdict['device'], mdict['attribute']


# def set_screen_io(io_group, device_name, attribute_name):
#     device = tango.DeviceProxy(device_name)
#     # attribute = device.attribute_query(attribute_name)
#     if not io_group.has_source(_LIMA_BPM_WAGO_SRC_NAME_):
#         io_group.add_source(_LIMA_BPM_WAGO_SRC_NAME_, device)

#     io_name_enum = _screen_ctrl_enum(device_name, attribute_name)
#     io_name = io_name_enum.TG_SCREEN
#     io_inst = model.get_io_instance(io_name)

#     if not io_inst:
#         io_class = new_tango_io_class(io_name)
#         model.add_io_class(io_class)
#         io_inst = model.get_io_instance(io_name)
    
#     model.add_substitution(ScreenIoNames.SCREEN_IO, io_inst.__class__)
#     return io_inst


def get_screen_tg_device(device_name):
    return tango.DeviceProxy(device_name)


def save_screen_tango_property(device, property):
    device.put_property({_SCREEN_TG_PROPERTY: property})


def read_screen_tango_property(device):
    prop = device.get_property(_SCREEN_TG_PROPERTY)[_SCREEN_TG_PROPERTY]
    if prop:
        return prop[0]
    return None


# def _screen_ctrl_enum(dev_name, attr_name):
#     return IoNamesEnum('BvGuiIoNames',
#                        [('TG_SCREEN', f'WagoDS@{dev_name}:{attr_name}')])



# class TangoAttributeSelector(Qt.QWidget):
#     def __init__(self, tgClass, parent=None):
#         super().__init__(parent=parent)

#         db = tango.Database()


class ScreenButton(Qt.QToolButton):
    def __init__(self, io_group, *args, screen_io=None, **kwargs):
        super().__init__(*args, **kwargs)

        self._has_screen = False

        self._io_group = io_group

        if isinstance(screen_io, (IoAdapterBase,)):
            self.setScreenIo(screen_io)
        else:
            self._readScreenIo()

        self.setContextMenuPolicy(Qt.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(
            self._screenTbCustomContextMenu)

    def ioGroup(self):
        return self._io_group

    def _readScreenIo(self):
        io_group = self.ioGroup()
        limaccds = io_group.source('LimaCCDs')
        wago_prop = read_screen_tango_property(limaccds)
        device, attribute = parse_screen_io(wago_prop)
        self.setWagoAttribute(device, attribute)

    def setWagoAttribute(self, device_name, attribute_name):
        if not device_name:
            self.setScreenIo(None)
            return
        device = get_screen_tg_device(device_name)
        io_adapter = TangoIoAdapter(tango_name=attribute_name,
                                    source=device,
                                    poll_ms=1500)
        self.setScreenIo(io_adapter)

        # screenIo = model.get_io_instance(ScreenIoNames.SCREEN_IO)
        # if screenIo is None:y
        #     model.add_io_class(BvScreenIo)
        #     screenIo = model.get_io_instance(ScreenIoNames.SCREEN_IO)

    def setScreenIo(self, ioAdapter):
        self._io_adapter = ioAdapter
        if ioAdapter:
            # self.setIcon(None)
            ioAdapter.start()
            ioAction = QCellAction(data=ioAdapter, parent=self)
            self.setDefaultAction(ioAction)
            self.toggled.connect(self._screenTbToggled)
            self.setEnabled(True)
            self._screenTbToggled(ioAction.isChecked())
            self._has_screen = True
        else:
            self.setEnabled(False)
            self.setIcon(Qt.QIcon())
            self._has_screen = False

        # property = read_screen_tango_property(self.io_group().data_source('LimaCCDs'))
        
        # if property:
        #     try:
        #         save_screen_tango_property(device, property)
        #         # ioAction._ioItemChanged()
        #         ioAction._cell_refresh()
        #     except Exception as ex:
        #         Qt.QMessageBox.critical(self,
        #                                 f'Screen wago control: Error.',
        #                                 f'Error setting the screen control '
        #                                 f'for the camera {model.name}.'
        #                                 f'Exception:\n\n===========\n\n'
        #                                 f'{str(ex)}')


    def _screenTbToggled(self, checked):
        qa = self.defaultAction()
        if qa:
            if checked:
                qa.setIcon(getQIcon('lima:icons/foil_in'))
            else:
                qa.setIcon(getQIcon('lima:icons/foil_out'))

    def event(self, event):
        if event.type() == Qt.QEvent.MouseButtonRelease:
            if event.button() == Qt.Qt.RightButton:
                self._screenTbCustomContextMenu(event.pos())
                return True
        return super().event(event)

    def _screenTbCustomContextMenu(self, pos):
        menu = Qt.QMenu()
        action = menu.addAction('Edit command.')
        action.triggered.connect(self._screenTbEditTriggered)
        menu.exec_(self.mapToGlobal(pos))

    def _screenTbEditTriggered(self, checked=None):
        io_group = self.ioGroup()
        valid = False
        cur_text = ''
        if self._has_screen:
            da = self.defaultAction()
            device = da.cell.io_adapter().device_proxy
            attr = da.cell.io_adapter().attribute_proxy
            dev_name = device.name() if device else ''
            attr_name = attr.name() if attr else ''
            cur_text = f'{dev_name}:{attr_name}'

        while not valid:
            text, reply = Qt.QInputDialog.getText(self,
                                                  'Screen in/out wago channel',
                                                  'Wago DS and attribute '
                                                  '(e.g: ID00/wcid00/tg:screen)',
                                                  text=cur_text)
            text = text.strip()
            if reply == False:
                return
            if not text:
                ans = Qt.QMessageBox.question(self,
                                            'Warning',
                                            'Are you sure you want to '
                                            'disable this button?')
                if ans != Qt.QMessageBox.Yes:
                    # TODO
                    continue
                    # return
            else:
                cur_text = text
                device, attribute = parse_screen_io(text)
                if device:
                    self.setWagoAttribute(device, attribute)
                else:
                    Qt.QMessageBox.critical(self,
                                            'ERROR, not valid, must be of the '
                                            'form device_name:attribute_name.')
                    continue
            
            limaccds = io_group.source('LimaCCDs')
            ans = Qt.QMessageBox.question(self,
                                        'Save as property',
                                        'Save this setting as a property '
                                        'of the LimaCCDs device server?\n'
                                        f'{limaccds.name()}')
            if ans == Qt.QMessageBox.Yes:
                save_screen_tango_property(limaccds, text)
            valid = True
        # da._ioItemChanged()