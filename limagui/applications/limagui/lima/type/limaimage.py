# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from .videoutils import (limaccds_video_buffer_to_img)


def ctvideo_image_to_video_image(image):
    return LimaVideoImage(image.buffer()[:],
                          image.width(),
                          image.height(),
                          image.mode(),
                          number=image.frameNumber())


def lima_processlib_data_to_image(processlib_data):
    data_buffer = processlib_data.buffer[:]
    return LimaImage(data_buffer,
                     data_buffer.shape[1],
                     data_buffer.shape[0],
                     mode=None,
                     number=processlib_data.frameNumber)


class LimaImageBase(object):
    def __init__(self, imgbuf, w, h, mode=None, number=None):

        self._buffer = imgbuf
        self._mode = mode
        self._width = w
        self._height = h
        self._number = number

    def buffer(self):
        return self._buffer

    def width(self):
        return self._width

    def height(self):
        return self._height

    def mode(self):
        return self._mode

    def number(self):
        return self._number

    def image(self, to_gray=False):
        raise NotImplementedError('')

    def is_valid(self):
        return self._buffer is not None and len(self._buffer) > 0


class LimaImage(LimaImageBase):
    def image(self, to_gray=False):
        return self.buffer()


class LimaVideoImage(LimaImageBase):
    def image(self, to_gray=False):
        return limaccds_video_buffer_to_img(self.buffer(),
                                            self.width(),
                                            self.height(),
                                            self.mode())


class LimaAcquisitionImage(LimaImage):
    pass
