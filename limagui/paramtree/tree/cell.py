# -*- coding: utf-8 -*-

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"


from weakref import ref
from contextlib import contextmanager

from ..utils.subject import Subject
from ..utils.observer import Observer

from .ioadapter import IoAdapter, IoAdapterBase
from .viewadapters.viewadapter import ViewAdapter
from .types import Action


# def cache_return(meth):
#     # TODO
#     @wraps(meth)
#     def wrapper(inst, value):
#         res = meth(inst, value)
#         inst._update_cache({meth.__name__: res})
#         return res
#     return wrapper


def setter_notify(func):
    def wrapper(self, *args, **kwargs):
        # print('NOTIF', func.__name__, *args, **kwargs)
        with self._lock_notify():
            func(self, *args, **kwargs)
    return wrapper


class Cell(object):
    def __init__(self,
                 parent=None,
                 data=None,
                 view_adapter_cls=None,
                 **kwargs):
        """
        Parent must have a _data_changed(**kwargs) method.
        """
        self._lock = 0
        self._dirty = {}
        self._subject = None
        self._observer = Observer(parent=self, callback=self._data_changed)
        self._on_change_callback = lambda *args, **kwargs: None
        self._parent = None
        self._column = None

        if isinstance(data, (IoAdapterBase,)):
            self._io_adapter = data
        else:
            self._io_adapter = IoAdapter(data=data)
            # ,
            #                              editable=editable,
            #                              selectable=selectable,
            #                              checkable=checkable)
        self._view_adapter = None
        self._view_adapter_cls = view_adapter_cls
        self._view_kwargs = kwargs or {}
        # self._cell_init_data = data
        self._io_adapter.register_view(self._observer)

        # self.refresh()
        if parent is not None:
            self._parent = ref(parent)

    @contextmanager
    def _lock_notify(self):
        self._lock += 1
        if self._lock == 1:
            self._dirty = {}
        try:
            yield
        except Exception as ex:
            print(f'In {self.io_name} : {ex}')
            # import traceback
            # traceback.print_stack()
            # print(__file__)
            # print(ex)
        finally:
            self._lock -= 1
            if self._lock == 0:
                data = self._dirty
                self._dirty = {}
                self._notify(**data)

    def _data_changed(self, **kwargs):
        with self._lock_notify():
            self._dirty.update(kwargs)
            self.view_adapter().set_params(**kwargs)

    def _view_adapter_changed(self, **kwargs):
        with self._lock_notify():
            self._dirty.update(kwargs)
            # self.view_adapter().set_params(**kwargs)
            
    def _notify(self, **kwargs):
        reset = kwargs.pop('reset', None)
        if reset:
            self._view_adapter = None
        if 'data' in kwargs and not 'text' in kwargs:
            view_data = self.view_adapter().set_model_to_view(kwargs['data'])
            if 'text' not in kwargs:
                kwargs['text'] = self.view_adapter().text
            kwargs['view_data'] = view_data
        parent = self.parent()
        if parent:
            self.parent()._data_changed(self, reset=reset, **kwargs)
        self._on_change_callback(reset=reset, **kwargs)
        if reset is not None:
            self.subject().notify(cell=self, reset=reset, **kwargs)
        else:
            self.subject().notify(cell=self, **kwargs)

    def subject(self):
        if self._subject is None:
            self._subject = Subject()
        return self._subject

    def register(self, observer):
        self.subject().register(observer)

    def parent(self):
        """
        This cell's parent.
        """
        return self._parent and self._parent()

    def set_parent(self, parent):
        self._parent = parent and ref(parent)

    def io_adapter(self):
        """
        This cell's IOAdapter.
        """
        return self._io_adapter

    def get_view_params(self, name=None):
        return self.view_adapter().get_params(name=name)

    def get_all_data(self):
        view_params = self.get_view_params()
        result = {'data': self.data(cache=True)}
        result.update(view_params)
        return result

    def set_view_adapter(self, view_adapter_cls):
        if self._view_adapter:
            self._view_kwargs = self._view_adapter.get_params()
        self._view_adapter = None
        self._view_adapter_cls = view_adapter_cls

    def _view_adapter_init(self):
        data = self.io_adapter().data(cache=True, _notify=False)
        # print('INIT', data, self.io_adapter().data_type(), self.io_adapter().io_name)
        self._view_adapter = None
        if self._view_adapter_cls:
            vad = self._view_adapter_cls(cell=self,
                                        data=data,
                                        #  data=self._cell_init_data,
                                         data_type=self.io_adapter().data_type(),
                                         **self._view_kwargs)
        else:
            dtype = self.io_adapter().data_type()
            if dtype is None and self.io_adapter().write_only():
                dtype = Action
            elif dtype is bool and not self.write_only() and not self.read_only():
                dtype = Action
                # if self.checkable() is None:
                #     self.set_checkable(True)
                # checkable = self._view_kwargs.get('checkable')
                # if checkable is None:
                #     self._view_kwargs['checkable'] = True

            vad = ViewAdapter(dtype,
                              cell=self,
                              data=data,
                            #   data=self._cell_init_data,
                              **self._view_kwargs)
        # if self._io_adapter.io_name:
        #     self.io_adapter.get_params()
            # print(self._io_adapter.io_name, self.get_view_params())
        vad.set_params(**self.io_adapter().get_params(), _notify=False)
        self._view_adapter = vad
        return self._view_adapter

    def view_adapter(self):
        """
        This cell's ViewAdapter.
        """
        if self._view_adapter is None:
            self._view_adapter_init()
            data = self.data(cache=True)
            self._view_adapter.set_model_to_view(data)
            # print('HOLA', self.io_adapter().io_name, data)
            # self._data_changed(data=data)
        return self._view_adapter

    def read_only(self):
        return self.io_adapter().read_only()

    def write_only(self):
        return self.io_adapter().write_only()

    def column(self):
        return self._column

    def _set_column(self, column):
        self._column = column

    def data_type(self):
        return self.io_adapter().data_type()

    def text(self):
        """
        this node's data converted to text that can
        be displayed in a view.
        Default behaviour: str(data)
        """
        return self.view_adapter().text

    def view_config(self):
        return self._io_adapter.view_config()

    def view_data(self):
        """
        Data in a form suitable for an editor.
        """
        return self.view_adapter().view_data

    def set_view_data(self, view_data):
        """
        Data from an editor.
        """
        self.view_adapter().set_view_to_model(view_data)

    def data(self, cache=True):
        """
        Returns this cell model data.
        Reads it from the IO adapter.
        """
        data = self._io_adapter.data(cache=cache)
        return data

    def refresh(self, notify=True):
        self.io_adapter().refresh()


    # def get_values(self, **kwargs):
    #     if kwargs:
    #         keys = [key for key, value in kwargs.items() if value]
    #     else:
    #         keys = ['data', 'view_data',
    #                 'decoration', 'enabled',
    #                 'checkable', 'checked',
    #                 'text', 'tooltip',
    #                 'editable']

    #     data = {key:getattr(self, key)() for key in keys}
    #     return data

    def view_size(self):
        return self.view_adapter().size()

    def background_color(self):
        return self.view_adapter().background_color
 
    def tooltip(self):
        return self.view_adapter().tooltip
    
    def decoration(self):
        return self.view_adapter().color
    
    def foreground_color(self):
        return self.view_adapter().foreground_color

    def selectable(self):
        return self.view_adapter().selectable
     
    def drag_enabled(self):
        return self.view_adapter().drag_enabled
    
    def drop_enabled(self):
        return self.view_adapter().drop_enabled
    
    def enabled(self):
        return self.view_adapter().enabled
    
    def editable(self):
        return self.view_adapter().editable
    
    def checked(self):
        return self.view_adapter().checked
    
    def checkable(self):
        return self.view_adapter().checkable

    @setter_notify
    def set_data(self, data):
        self._io_adapter.set_data(data=data)

    @setter_notify
    def set_background_color(self, background_color):
        self.view_adapter().background_color = background_color

    @setter_notify
    def set_tooltip(self, tooltip):
        self.view_adapter().set_tooltip(tooltip)

    @setter_notify
    def set_decoration(self, decoration):
        self.view_adapter().set_decoration(decoration)

    @setter_notify
    def set_foreground_color(self, foreground_color):
        self.view_adapter().set_foreground_color(foreground_color)

    @setter_notify
    def set_drag_enabled(self, drag_enabled):
        self.view_adapter().set_drag_enabled(drag_enabled)

    @setter_notify
    def set_drop_enabled(self, drop_enabled):
        self.view_adapter().set_drop_enabled(drop_enabled)

    @setter_notify
    def set_enabled(self, enabled):
        self.view_adapter().set_enabled(enabled)

    @setter_notify
    def set_editable(self, editable):
        self.view_adapter().set_editable(editable)

    @setter_notify
    def set_checked(self, checked):
        self.view_adapter().set_checked(checked)

    @setter_notify
    def set_checkable(self, checkable):
        self.view_adapter().set_checkable(checkable)

    @setter_notify
    def set_selectable(self, selectable):
        self.view_adapter().set_selectable(selectable)
    
    def set_on_change_callback(self, callback):
        if callback:
            self._on_change_callback = callback
        else:
            self._on_change_callback = lambda data: None

    ##########################################
    ####                                  ####
    ##########################################
    def start(self):
        """
        Called when the cell becomes visible.
        """
        self.io_adapter().start()

    def stop(self):
        """
        Called when the cell becomes hidden.
        """
        self.io_adapter().stop()
