# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"

from ... import Qt


def paintDiode(qColor, qRect, painter=None, alignment=None):
    """
    Paints a "diode" shape.
    :param qColor: instance of Qt.QColor for the diode
    :param qRect: instance of Qt.QRect for the diode boundaries
    :param painter: instance of Qt.QPainter
    :param alignment: alignment of the diode (if qRect is not a square).
        Either Qt.AlignLeft or Qt.AlignRight. If not specified the diode will
        be centered.
    :return:
    """
    if painter is None:
        painter = Qt.QPainter()

    center = qRect.center()
    size = 0.9 * min(qRect.width(), qRect.height())
    ledRect = Qt.QRect(0, 0, size, size)
    ledRect.moveCenter(center)

    if alignment is not None:
        if alignment == Qt.Qt.AlignLeft:
            ledRect.moveLeft(qRect.left())
        elif alignment == Qt.Qt.AlignRight:
            ledRect.moveRight(qRect.right())

    painter.setRenderHint(Qt.QPainter.Antialiasing)
    gradient = Qt.QRadialGradient(ledRect.center(), size)
    gradient.setColorAt(0, qColor)
    gradient.setColorAt(1, Qt.QApplication.palette().color(
        Qt.QPalette.Window))
    brush = Qt.QBrush(gradient)
    brush.setStyle(Qt.Qt.RadialGradientPattern)
    painter.setBrush(brush)
    painter.setPen(Qt.QPen(Qt.Qt.NoPen))
    painter.drawEllipse(ledRect)

    painter.save()

    painter.setPen(Qt.QPen(Qt.QColor('black')))

    painter.drawEllipse(ledRect)

    painter.restore()
