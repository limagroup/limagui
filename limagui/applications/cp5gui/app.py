# coding: utf-8
from __future__ import absolute_import

import argparse

from limagui.paramtree.qt import Qt
from .cp5widget import Cp5Widget


def parse_args():

    parser = argparse.ArgumentParser(prog='cp5gui',
                                     description='Displays status of a cp5coolerDS')

    parser.add_argument('cp5_dev', help='cp5coolerDS device name (e.g: d20-cp5ge)',
                        type=str)

    parsed_args = parser.parse_args()

    return parsed_args


def cp5_main():
    cp5_args = parse_args()

    if not cp5_args:
        exit(0)

    app = Qt.QApplication([])

    win = Cp5Widget(cp5_dev_name=cp5_args.cp5_dev)

    win.show()

    app.exec_()