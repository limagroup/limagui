# from importlib import import_module

import gevent

# import Lima
from limagui.applications.limagui.lima.lima_io.limaionames import (
    CtControl, CtAcquisition, CtAccumulation, CtImage, CtVideo, LimaTriggers, HwInterface,
    LimaImages)
# from limagui.applications.limagui.limatango.tango_io import ctcontrol

# from limagui.paramtree import Node, IoAdapter, Observer, Subject
# from limagui.paramtree.tree.ioadapter import IoAdapterBase, create_io_class, ioadapterdef, parse_io_name
from limagui.paramtree.qt import ParamTreeView
from limagui.applications.limagui.limagui.widgets.limaguimainwindow import LimaGuiMainWindow
from limagui.paramtree.qt import Qt, tree

from limagui.applications.limagui.lima import new_lima_camera 

from limagui.paramtree import IoGroup

# import limagui.applications.limagui.limagui
# from limagui.paramtree.tree import iogroup
# from limagui.paramtree.tree.extra import TangoIoAdapter

# import tango
# from tango import AttrWriteType, AttributeProxy, EventType

# from limagui.applications.limagui.limatango.tango_io.tangoionames import (
#     TangoLimaCtVideo,
#     TangoLimaCtAcquisition,
#     TangoLimaCtImage,
#     LIMA_TANGO_IO_NAMES,
#     TangoLimaCtControl,
#     TangoLimaCtAccumulation,
#     TangoLimaHwSync,
#     TangoLimaTriggers,
#     LimaCCDsImageStatusNames)


# from limagui.applications.limagui.limagui.widgets.limaimageview import LimaImageView
from limagui.applications.limagui.limagui.widgets.limaguimainwindow import LimaGuiMainWindow


app = Qt.QApplication([])
app.setQuitOnLastWindowClosed(True)

run_gevent = True

def geventLoop():
    print('IN')
    while run_gevent:
        app.processEvents()
        while app.hasPendingEveWnts():
            app.processEvents()
            gevent.sleep()
        gevent.sleep()
    print('OUT')

# def run(self, useGevent=False, exec_=True, createMainWindow=True):
#     if createMainWindow:
#         self.mainWindow().show()

#     self._useGevent = useGevent
#     self._runGevent = True

    # if exec_:
    #     if self._useGevent:
    #         gevent.joinall([gevent.spawn(self._geventLoop)])
    #     else:
    #         self._app.exec_()

io_group = IoGroup(name='Simulator')

if 1:
    lima_cam = new_lima_camera('Simulator')
else:
    lima_cam = new_lima_ccds('d20-simu')
    limaccds_add_substitutes(io_group)
    acqnbframes = create_io_class(io_name=TangoLimaCtAcquisition.ACQNBFRAMES,
                                  cls=TangoIoAdapter)
    (io_group.add_source(name, obj) for name, obj in lima_cam.items())

for name, obj in lima_cam.items():
    io_group.add_source(name, obj)

# model = QModel()
# model.startModel()

tree = ParamTreeView()

if 0:
    io_tree = lima_io_tree(io_group)
    io_tree = limaccds_io_tree(io_group)
    node = Node(name='Lima', data=io_tree)
else:
    ios = [
        # io_group.io_adapter(CtControl.STATUS),
        # io_group.io_adapter(CtControl.STARTACQ),
        # io_group.io_adapter(CtControl.PREPAREACQ),
        # io_group.io_adapter(CtAcquisition.ACQNBFRAMES),
        # io_group.io_adapter(CtAcquisition.ACQEXPOTIME),
        # io_group.io_adapter(CtControl.IMAGESTATUS),
        # io_group.io_adapter(CtControl.STOPACQ),
        io_group.io_adapter(CtAcquisition.ACQMODE),
        io_group.io_adapter(CtAcquisition.ACCEXPOTIME),
        # io_group.io_adapter(CtVideo.ROI),
        # io_group.io_adapter(LimaTriggers.ACQSTATUS_CHANGE),
        # io_group.io_adapter(LimaTriggers.STATUS_CALLBACK),
        # io_group.io_adapter(LimaTriggers.IMAGEREADY),
        # io_group.io_adapter(LimaImages.LASTIMAGE),
        # io_group.io_adapter(TangoLimaTriggers.STATUS_CALLBACK_CNT),
        # io_group.io_adapter(TangoLimaCtControl.IMAGESTATUS),
        # io_group.io_adapter(TangoLimaCtControl.STATUS),
        # io_group.io_adapter(TangoLimaCtControl.STATUS_ACQUISITIONSTATUS),
        # io_group.io_adapter(LimaCCDsImageStatusNames.LAST_BASE_IMAGE_READY),
        # io_group.io_adapter(LimaCCDsImageStatusNames.LAST_COUNTER_READY),
        # io_group.io_adapter(LimaCCDsImageStatusNames.LAST_IMAGE_ACQUIRED),
        # io_group.io_adapter(LimaCCDsImageStatusNames.LAST_IMAGE_READY),
        # io_group.io_adapter(LimaCCDsImageStatusNames.LAST_IMAGE_SAVED),
        
        # io_group.io_adapter(TangoLimaCtAcquisition.ACQNBFRAMES),
        # io_group.io_adapter(TangoLimaCtControl.STARTACQ),
        # io_group.io_adapter(TangoLimaCtControl.PREPAREACQ)
        # io_group.io_adapter(TangoLimaCtVideo.STARTLIVE),
        # io_group.io_adapter(TangoLimaCtVideo.STOPLIVE),
        # io_group.io_adapter(TangoLimaCtImage.FLIP)
        ]
    ios = {io.io_name.name:io for io in ios}
    # node = Node(name='Lima', data=ios)

idx = tree.model().insertNodeData(name='Lima', data=ios)
tree.expandAll()

win = Qt.QMainWindow()
win.resize(Qt.QSize(500, 500))
win.setCentralWidget(tree)


# win = LimaGuiMainWindow()

# if 0:
#     # img = CameraWindow(cam_type='Simulator')
#     # win.setCentralWidget(img)
#     win.addCamera(cam_type='Simulator')
#     win.addCamera(cam_type='limaccds', cam_args=('d20-cam2',))
# else:
#     from limagui.paramtree import IoGroup
#     from limagui.applications.limagui.lima.lima_io.limaionames import *
#     from limagui.applications.limagui.limagui.widgets.camerawindow import _init_lima_camera
#     from limagui.paramtree import Node
#     from limagui.paramtree.qt import QModel, TreeView
#     from limagui.applications.limagui.limatango.tango_io.tangoionames import LimaCCDsTango
#     io_group = IoGroup(name='simu')
#     _init_lima_camera(io_group, cam_type='Simulator')
#     # _init_lima_camera(io_group, 'd20-simu', cam_type='tango')
#     ios = [
#         # io_group.io_adapter(CtImage.BIN),
#         # io_group.io_adapter(LimaCCDsTango.TANGO_STATE),
#         io_group.io_adapter(LimaImages.LASTIMAGE),
#         io_group.io_adapter(CtControl.STATUS),
#         # io_group.io_adapter(LimaTriggers.ACTIONTRIGGERED),
#         io_group.io_adapter(CtControl.STARTACQ),
#         io_group.io_adapter(CtControl.PREPAREACQ),
#     ]
#     # print(ios)
#     model = QModel()
#     model.startModel()
#     treeA = TreeView()
#     treeA.setModel(model)
#     node = Node(name='Lima', data={io.io_name.name:io for io in ios})
#     model.insertNode(node)
#     win.setCentralWidget(treeA)
#     treeA.expandAll()



win.show()

print('===\n\n\n')

# app.exec_()

gevent.joinall([gevent.spawn(geventLoop)])


print('END')
