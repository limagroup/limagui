from limagui.paramtree import IoNamesEnum
from limagui.paramtree.tree.extra import create_tg_io_class


class Cp5IoNames(IoNamesEnum):
    CURRENT1 = 'cp5cooler:current1'
    CURRENT2 = 'cp5cooler:current2'
    VOLTAGE1 = 'cp5cooler:voltage1'
    VOLTAGE2 = 'cp5cooler:voltage2'
    POWER1 = 'cp5cooler:power1'
    POWER2 = 'cp5cooler:power2'
    TEMPERATURE = 'cp5cooler:temperature'
    TCOMP = 'cp5cooler:tcomp'
    THEAD = 'cp5cooler:thead'


for io_name in Cp5IoNames:
    create_tg_io_class(io_name, poll_ms=3000)