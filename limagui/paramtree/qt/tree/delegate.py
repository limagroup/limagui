# coding: utf-8

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from .. import Qt
from .editor import QEditorMixin, qeditor_for_type
from .qhelpers import nodeFromModelIndex
from .errors import EmptyModelIndexError, InvalidModelIndexError


# TODO :
# QStyledItemDelegate.createEditor and setEditorData
# both call Cell.get_data (i.e : get data is called twice in a row
# find a way to optimize this (e.g : for when it retrieves data over a network)
# Also: after committing data, setEditorData (then get data) on an editor is called for
# each views connected to that editor's cell


class QDelegate(Qt.QStyledItemDelegate):
    sigDelegateEvent = Qt.Signal(object, object)

    def __init__(self, parent=None):
        super(QDelegate, self).__init__(parent)

        self._locked = False

        # to keep track of opened editors
        self.closeEditor.connect(self._onEditorClosed)

        self._openedIndices = {}
        self._openedEditors = {}

    def setLocked(self, locked):
        self._locked = locked

    def createEditor(self, parent, option, index):
        node = nodeFromModelIndex(index)
        column = index.column()
        klass = node.view_adapter_type(column)
        editor = None
        if klass is None:
            klass = node.data_type(column)
        editorClass = qeditor_for_type(klass)
        if editorClass is not None:
            if issubclass(editorClass, (QEditorMixin,)):
                editor = editorClass(parent, option, index)
                editor._setCommitCallback(self._commitEditorData)
                # editor.destroyed.connect(self._editorDestroyed)
            else:
                print('Error : editor is not a subclass of QEditor ({0}).'
                      ''.format(editorClass))
        if editor is None:
            editor = super(QDelegate, self).createEditor(parent, option, index)
        return editor

    def _commitEditorData(self, widget):
        """
        Tells the model that the editor is has some data to
        commit.
        """
        self.commitData.emit(widget)

    def paint(self, painter, option, index):
        try:
            node = nodeFromModelIndex(index)
            editor = None
            column = index.column()
            if column == 1:
                klass = node.view_adapter_type(column)
                if klass is None:
                    klass = node.data_type(column)
                editor = qeditor_for_type(klass)
            if editor and issubclass(editor, (QEditorMixin,)):
                try:
                    painted = editor.paintDelegate(painter, option, index)
                except Exception as ex:
                    print('Exception caught while painting delegate.')
                    print('node : {0}'.format(node.name()))
                    print('Exception text: {0}.'.format(ex))
                    painted = False

                if painted:
                    return

        except (InvalidModelIndexError, EmptyModelIndexError):
            pass
        super(QDelegate, self).paint(painter, option, index)

    def updateEditorGeometry(self, editor, option, index):
        super(QDelegate, self).updateEditorGeometry(editor, option, index)
        # print('UEG OUT', editor.geometry())
        # editor.move(option.rect.topLeft())
        # editor.setGeometry(option.rect)

    def _firstSet(self, editor, index):
        """
        Returns True if this is the first time that setEditorData
        is called on the
        """
        persistentIndex = self._openedEditors.get(editor)
        if persistentIndex is None:
            persistentIndex = Qt.QPersistentModelIndex(index)
            self._openedIndices[persistentIndex] = editor
            self._openedEditors[editor] = persistentIndex
            return True

        if isinstance(editor, (QEditorMixin,)) and editor.persistent:
            # TODO: handle the case where a persistent editor can be edited
            # like a widget with a line edit.
            return True

        return False

    def setEditorData(self, editor, index):
        enab = not self._locked
        if not self._firstSet(editor, index):
            editor.setEnabled(enab)
            return
        if isinstance(editor, QEditorMixin) and editor.setEditorData(index):
            editor.setEnabled(editor.isEnabled() and enab)
            return
        super(QDelegate, self).setEditorData(editor, index)
        editor.setEnabled(enab)

    def setModelData(self, editor, model, index):
        """
        Writes editor data to the model. Usually
        called when _commitEditorData is emitted.
        """
        if isinstance(editor, QEditorMixin):
            data = editor.editorData()
            if data is not None:
                model.setData(index, data, role=Qt.Qt.EditRole)
                return
        super(QDelegate, self).setModelData(editor, model, index)
        
    def _editorDestroyed(self, editor):
        pass

    def _onEditorClosed(self, editor, hint=Qt.QAbstractItemDelegate.NoHint):
        self._forgetEditor(editor)

    def _forgetIndex(self, index):
        try:
            editor = self._openedIndices.pop(Qt.QPersistentModelIndex(index))
            del self._openedEditors[editor]
        except KeyError:
            pass

    def _forgetEditor(self, editor):
        try:
            index = self._openedEditors.pop(editor)
            del self._openedIndices[index]
        except KeyError:
            pass

    def sizeHint(self, option, index):
        try:
            idx = Qt.QPersistentModelIndex(index)
            editor = self._openedEditors[idx]
            sh = editor.sizeHint()
        except KeyError:
            sh = super(QDelegate, self).sizeHint(option, index)
        return sh
        # print(self.__openedIndices)
        # try:
        #     editor = self.__openedIndices[Qt.QPersistentModelIndex(index)]
        #     sh = editor.sizeHint()
        #     print('ESH = ', sh)
        # except KeyError:
        #     size = index.data(Qt.Qt.SizeHintRole)
        #     print('sv', size)
        #     if size and size.isValid():
        #         sh = size
        #         print('model sh', sh)
        #     else:
        #         sh = super(QDelegate, self).sizeHint(option, index)
        #         print('DEF SH', sh)

        # sh = super(QDelegate, self).sizeHint(option, index)
        # sh2 = index.data(Qt.Qt.SizeHintRole)
        # wid = option.styleObject.indexWidget(index)
        # if wid:
        #     geo = wid.sizeHint()
        #     sh = Qt.QSize(50, 150)
        # else:
        #     geo = None
        # print('SH out', sh)#, wid, geo)
        # return sh
    #     return Qt.QSize(-1, -1)
    #     return Qt.QSize(-1, -1)
