from enum import Enum


__registered_io_names = {}


def _registered_io_names():
    return __registered_io_names.copy()


def register_io_names_enum(group_name, cls):
    group = __registered_io_names.get(group_name, {})
    if cls in group:
        raise ValueError("ionames {0} is already registered."
                         "".format(cls))
    cls_name = cls.__name__.split('.')[-1]
    group[cls_name] = cls
    __registered_io_names[group_name] = group


def get_io_names_enum(group_name, names_name):
    return __registered_io_names[group_name][names_name]


def registered_io_names_groups():
    return list(__registered_io_names.keys())


def registered_io_names_enums(group_name):
    return list(__registered_io_names[group_name].keys())


class IoNamesEnum(Enum):
    """
    Custom Enum class to allow duplicates.
    """
    def __new__(cls, io_name):
        idx = len(cls.__members__)
        obj = object.__new__(cls)
        obj._value_ = (idx, io_name)
        return obj

    @property
    def io_name(self):
        return self._value_[1]
        

def ionames_enum_def(group_name, register=True):
    def inner(cls):
        if register:
            register_io_names_enum(group_name, cls)
        return cls

    return inner
