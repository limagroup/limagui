# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"

"""
TODO : TESTS
TODO : rename
"""

import enum
import inspect


def sipenum2python(base_module, enum_base_cls, extra=None):
    enum_lst = [member for member in inspect.getmembers(base_module)
                if isinstance(member[1], enum_base_cls)]

    if extra:
        enum_lst.append(extra)

    if len(enum_lst) == 0:
        raise RuntimeError('Could not find any enum values for enum {0}'
                            ' in module {1}'.format(enum_base_cls,
                                                    base_module))

    py_enum = enum.Enum(enum_base_cls.__name__, enum_lst)
    setattr(py_enum, '_sip_base_cls', enum_base_cls)

    return py_enum