import tango
from tango import AttrWriteType, AttributeProxy, EventType


from ... import IoAdapterBase, Observer, Subject, ioadapterdef
from ..ioadapter import create_io_class, parse_io_name


def _tango_writable(writable):
    return writable in (AttrWriteType.WRITE, AttrWriteType.READ_WRITE)


def _tango_readable(readable):
    return readable in (AttrWriteType.READ, AttrWriteType.READ_WRITE)


_tango2python_types = {
    tango.DevVoid: None,
    tango.DevDouble: float,
    tango.DevString: str,
    tango.DevLong: int,
    tango.DevBoolean: bool,
}


def create_tg_io_class(*args, **kwargs):
    cls = kwargs.get('cls')
    if cls is None:
        kwargs['cls'] = TangoIoAdapter
    return create_io_class(*args, **kwargs)


# def new_tango_io_adapter_class(io_name_enum, conversions=None, **kwargs):
#     if conversions is None:
#         conversions = {}
#     parsed = parse_io_name(io_name_enum)
#     name = '_limaguitango_{0}'.format(''.join(c if c.isalnum() else '_'
#                                               for c in parsed.path))
#     conv = conversions.get(io_name_enum, [None, None])
#     new_class =  type(name, (TangoIoAdapter,))
#     return ioadapterdef(io_name=io_name_enum, 
#                         tango_conversions=conv,
#                         cls=new_class,
#                         **kwargs)


class TangoIoAdapter(IoAdapterBase):
    attribute_proxy = property(lambda self: self._attr_proxy)
    device_proxy = property(lambda self: self.source())

    def __init__(self,
                 *args,
                 tango_name=None,
                 tango_events=None,
                 tango_event_same_thread=False,
                 tango_readable=None,
                 tango_writable=None,
                 tango_conversion=[None, None],
                 tango_read_args=None,
                 tango_read_devfailed=None,
                 **kwargs):
        super().__init__(*args, **kwargs)

        self._tango_name = tango_name
        self._tango_events = tango_events
        self._tango_event_same_thread = tango_event_same_thread
        self._tango_readable = tango_readable
        self._tango_writable = tango_writable
        self._tango_conversion = tango_conversion
        self._tango_read_args = tango_read_args
        self._tango_read_devfailed = tango_read_devfailed

        self._tango_is_init = False
        if self._tango_conversion is None:
            self._tango_conversion = [None, None]
        self._attr = None
        self._command = None
        if self._tango_name is None:
            parsed = parse_io_name(self.io_name)
            self._tango_name = parsed.path.split('/')[-1]
        self._tango_no_args = None
        self._attr_proxy = None
        self._tango_event_obs = None
        self._tango_event_subject = None

    def _is_writable(self):
        return self.tango_writable

    def _is_readable(self):
        return self.tango_readable

    def _on_start(self):
        super(TangoIoAdapter, self)._on_start()
        self._init_tango()
        
    def _init_tango(self):
        self._tango_is_init = False
        if self._tango_name:
            source = self.source()
            if not source:
                return
            try:
                info = source.attribute_query(self._tango_name)
                self._attr = self._tango_name
                if self._tango_writable is None:
                    self._tango_writable = True if _tango_writable(info.writable) else None #o.writable_attr_name else None
                if self._tango_readable is None:
                    self._tango_readable = True if _tango_readable(info.writable) else None #info.writable_attr_name else None
                name = '{0}/{1}'.format(source.name(), self._attr)
                self._attr_proxy = AttributeProxy(name)
                if self._tango_readable and self._tango_events is not None:
                        attr = self._attr_proxy# = AttributeProxy(name)
                        # set_logging_level
                        try:
                            self._tango_event_obs = Observer(callback=self._on_tango_event)
                            self._tango_event_subject = Subject()
                            self._tango_event_obs.register(self._tango_event_subject)
                            evtid = attr.subscribe_event(EventType.CHANGE_EVENT,
                                                         self._tango_event)
                        except tango.DevFailed as ex:
                            print('Failed to register to change events '
                                  'for attribute {0}.'.format(attr.name()))
                            # print(ex)
                if self._data_type is None:
                    self._data_type = _tango2python_types.get(info.data_type)
            except tango.DevFailed as ex:
                try:
                    info = source.command_query(self._tango_name)
                    self._command = self._tango_name
                    if self._tango_writable is None:
                        self._tango_writable = True
                        if self._tango_no_args is None:
                            if info.out_type not in ('Uninitialised',
                                                      None,
                                                      'none',
                                                      tango.DevVoid,):
                                self._tango_no_args = False
                            else:
                                self._tango_no_args = True
                    if self._tango_readable is None:
                        if info.out_type_desc not in ('Uninitialised',
                                                      None,
                                                      'none',
                                                      tango.DevVoid,):
                            self._tango_readable = True
                        else:
                            self._tango_readable = False
                except tango.DevFailed as ex:
                    pass
        if self._tango_name and self._attr is None and self._command is None:
            print('Failed to init io_name={0}, tango_name={1}.'
                  ''.format(self.io_name, self._tango_name))
            self._tango_is_init = False
        else:
            self._tango_is_init = True
            if self._tango_readable and self._io_getter is None:
                self._io_getter = self._tango_io_getter
            if self._tango_writable and self._io_setter is None:
                self._io_setter = self._tango_io_setter
        return self._tango_is_init

    def _tango_event(self, event):
        value = event.attr_value
        if value is not None:
            data = self._tango_to_io(value.value)
            if self._tango_event_same_thread:
                self.set_data(data, _internal=True)
            else:
                self._tango_event_subject.notify(data)

    def _on_tango_event(self, data):
        self.set_data(data, _internal=True)

    def _tango_to_io(self, data):
        conv = self._tango_conversion[0]
        if data is not None and conv is not None:
            data = conv(data)
        return data

    def _io_to_tango(self, data):
        conv = self._tango_conversion[1]
        if conv is not None:
            try:
                data = conv(data)
            except Exception as ex:
                raise
                # print('Exception calling conversion fonction {0}: {1}.',
                #       self.tango_conversion, ex)
        return data

    def _tango_io_getter(self, source, **kwargs):
        # if not self._tango_is_init and not self._init_tango():
        #     return None
        data = None
        try:
            # print('CALL', self.io_name, kwargs, *kwargs.values())
            data = self._read_tango(source, *kwargs.values())
            data = self._tango_to_io(data)
        except tango.DevFailed as ex:
            err_msg = ex.args[0].desc
            print(f'TangoIoAdapter: {self._tango_name} : {err_msg}.')
            return self._tango_read_devfailed
        except Exception as ex:
            print(f'Error reading {self._tango_name}: {ex}.')
            data = None
        # print('GOT', self.io_name, data)
        return data

    def _read_tango(self, source, *args):
        if not self._tango_is_init and not self._init_tango():
            return None
        data = None
        if self._attr:
            data = source.read_attribute(self._attr).value
        elif self._command:
            if self._tango_read_args:
                data = source.command_inout(self._command, *self._tango_read_args, *args)
            else:
                data = source.command_inout(self._command, *args)
        return data

    def _tango_io_setter(self, source, data):
        data = self._io_to_tango(data)
        self._write_tango(source, data)
        return None

    def _write_tango(self, source, data):
        if self._attr:
            source.write_attribute(self._attr, data)
        elif self._command:
            if self._tango_no_args:
                source.command_inout(self._command)
            else:
                source.command_inout(self._command, data)