# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from functools import partial
from collections import OrderedDict

from silx.utils.weakref import WeakMethodProxy

_schedulers = OrderedDict()


def add_scheduler(name, scheduler):
    global _schedulers

    if name in _schedulers:
        raise ValueError('A scheduler with this name has already been'
                         'registered.')

    _schedulers[name] = scheduler


def get_scheduler(name=None):
    if not _schedulers:
        raise ValueError('No scheduler has been registered yet.')
    if name:
        return _schedulers[name]
    return _schedulers[list(_schedulers.keys())[0]]


def Scheduler(*args, **kwargs):
    sname = kwargs.pop('sname', None)
    sched_class = get_scheduler(name=sname)
    return sched_class(*args, **kwargs)


class SchedulerBase(object):

    delay_ms = property(lambda self: self.__delay_ms)
    single_shot = property(lambda self: self.__single_shot)

    def __init__(self,
                 callback,
                 delay_ms,
                 single_shot=True,
                 cb_args=None,
                 cb_kwargs=None):
        # # TODO : allow args, kwargs
        # self.__callback = WeakMethodProxy(callback)
        self.__delay_ms = delay_ms
        self.__single_shot = single_shot

        # should we use weakmethod?
        if callback is not None:
            partial_args = tuple()
            partial_kwargs = {}
            if cb_args is not None:
                partial_args += cb_args
            if cb_kwargs is not None:
                partial_kwargs.update(cb_kwargs)
            if partial_args or partial_kwargs:
                self.__callback = partial(WeakMethodProxy(callback),
                                          *partial_args,
                                          **partial_kwargs)
            else:
                self.__callback = WeakMethodProxy(callback)

    def start(self, delay_ms=None, single_shot=None):
        if delay_ms is not None:
            self.__delay_ms = delay_ms
        if single_shot is not None:
            self.__single_shot = single_shot

        self._start(self.__delay_ms, self.__single_shot)

    def stop(self):
        raise NotImplementedError('')

    def reset(self):
        raise NotImplementedError('')

    def _start(self, delay_ms, single_shot):
        raise NotImplementedError('')

    def _fire(self):
        try:
            self.__callback()
        except ReferenceError as ex:
            # print(self.__callback)
            import traceback
            traceback.print_stack()
            print(__file__)
            print(ex)
