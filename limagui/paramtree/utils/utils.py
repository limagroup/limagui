# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/09/2017"


from importlib import import_module
from collections import OrderedDict


_uid_counter = 1


def new_uid():
    global _uid_counter

    _uid_counter += 1

    return _uid_counter


class DefaultODict(OrderedDict):
    def __init__(self, default, missing_constructor=None, **kwargs):
        super(DefaultODict, self).__init__(**kwargs)
        self.__default = default
        self.__missing_constructor = missing_constructor

    def __missing__(self, key):
        if self.__missing_constructor:
            self[key] = missing = self.__missing_constructor(key)
            return missing
        return self.__default


def get_module(module_path, package):
    """
    Imports a module.
    module_path: my.mod.path, or ..my.mod.path, etc...
    Relative to package module.
    """
    found_mod = None
    try:
        print('Trying to import {0} module.'.format(module_path))
        mod_path = package#.rsplit('.', 1)[0]
        found_mod = import_module(module_path, package=mod_path)
    except ImportError:
        raise
    return found_mod