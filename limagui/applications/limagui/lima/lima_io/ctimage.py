# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"

from Lima import Core


from limagui.paramtree import IoAdapterBase, create_io_class, ioadapterdef

from .limaionames import CtImage, LimaTriggers
from ..type import enums


LimaImageBin = create_io_class(CtImage.BIN,
                    io_setter=lambda ctimage, data: ctimage.setBin(data),
                    io_getter=lambda ctimage: ctimage.getBin(),
                    data_type=Core.Bin,
                    triggers=LimaTriggers.REFRESH_TRIGGER)


LimaImageRoi = create_io_class(CtImage.ROI,
                    io_setter=lambda ctimage, data: ctimage.setRoi(data),
                    io_getter=lambda ctimage: ctimage.getRoi(),
                    data_type=Core.Roi,
                    triggers=LimaTriggers.REFRESH_TRIGGER)


LimaImageFlip = create_io_class(CtImage.FLIP,
                    io_setter=lambda ctimage, data: ctimage.setFlip(data),
                    io_getter=lambda ctimage: ctimage.getFlip(),
                    data_type=Core.Flip,
                    triggers=LimaTriggers.REFRESH_TRIGGER)


LimaImageRotation = create_io_class(CtImage.ROTATION,
                    io_setter=lambda ctimage, data: ctimage.setRotation(data),
                    io_getter=lambda ctimage: ctimage.getRotation(),
                    data_type=enums.RotationMode,
                    triggers=LimaTriggers.REFRESH_TRIGGER)
