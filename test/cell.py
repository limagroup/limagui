import enum
from functools import partial
from collections import OrderedDict

from limagui.paramtree import Node, Action, Actions, IoAdapterBase, Cell
from limagui.paramtree.qt import QModel, TreeView
from limagui.paramtree.tree.types import Progress
from limagui.paramtree.qt import Qt
from limagui.paramtree.qt.tree.widgets import QCellWidget
from limagui.paramtree.qt.tree.widgets import widgets as cellwids


cell = Cell(data=1)

print(cell.data())

print(cell.view_adapter())
print(cell.get_params())