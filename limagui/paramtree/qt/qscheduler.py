# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from . import Qt
from ..utils.scheduler import SchedulerBase, add_scheduler


class QScheduler(SchedulerBase):
    def __init__(self, *args, **kwargs):
        super(QScheduler, self).__init__(*args, **kwargs)
        self.__timer = timer = Qt.QTimer()
        self.__timer.setSingleShot(self.single_shot)
        self.__timer.setInterval(self.delay_ms)
        timer.timeout.connect(self._fire)

    def _start(self, delay_ms, single_shot):
        if self.__timer.isActive():
            self.__timer.stop()
        self.__timer.setSingleShot(single_shot)
        self.__timer.setInterval(delay_ms)
        self.__timer.start()

    def stop(self):
        self.__timer.stop()

    def reset(self, start=False):
        # restarting a QTimer == reset
        if self.__timer.isActive() or start:
            self.__timer.start()


add_scheduler('Qt', QScheduler)
