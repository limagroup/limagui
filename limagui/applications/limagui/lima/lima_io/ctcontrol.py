# coding: utf-8

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"

from Lima import Core

from limagui.paramtree import IoAdapterBase, create_io_class, ioadapterdef

from ..type.limaimage import LimaImage, lima_processlib_data_to_image

from .limaionames import CtControl, LimaTriggers
from ..type import enums


LimaStoptAcq = create_io_class(CtControl.STOPACQ,
                            io_setter=lambda ctctrl, data: ctctrl.stopAcq(),
                            icon='lima:icons/acq_stop')


LimaPrepareAcq = create_io_class(CtControl.PREPAREACQ,
                    io_setter=lambda ctctrl, data: ctctrl.prepareAcq(),
                    icon='lima:icons/apply_config')


LimaStartAcq = create_io_class(CtControl.STARTACQ,
                    io_setter=lambda ctctrl, data: ctctrl.startAcq(),
                    icon='lima:icons/shoot')


LimaAbortAcq = create_io_class(CtControl.ABORTACQ,
                    io_setter=lambda ctctrl, data: ctctrl.abortAcq(),
                    icon='lima:icons/abort')


LimaReset = create_io_class(CtControl.RESET,
                    io_setter=lambda ctctrl, data: ctctrl.reset(),
                    icon='lima:icons/reset')


@ioadapterdef(CtControl.IMAGESTATUS,
                    data_type=Core.CtControl.ImageStatus,
                    io_getter=lambda ctctrl: ctctrl.getImageStatus(),
                    triggers=[CtControl.STATUS])
class LimaImageStatus(IoAdapterBase):
    def _on_trigger(self, sender=None, data=None, **kwargs):
        if sender == CtControl.STATUS:
            self.set_data(data.ImageCounters, _internal=True)
        else:
            return super()._on_trigger(sender=sender, data=data, **kwargs)


LimaStatus = create_io_class(CtControl.STATUS,
                    io_getter=lambda ctctrl: ctctrl.getStatus(),
                    data_type=Core.CtControl.Status,
                    poll_ms=300,
                    triggers=[LimaTriggers.ACTIONTRIGGERED,
                              LimaTriggers.STATUS_CALLBACK])


def _read_image(ctctrl):
    try:
        return lima_processlib_data_to_image(ctctrl.ReadImage())
    except Core.Exception:
        # TODO : logger
        return None

LimaReadImage = create_io_class(CtControl.READIMAGE,
                                data_type=LimaImage,
                                io_getter=_read_image,
                                hidden=True)
        

def _read_base_image(ctctrl):
    try:
        return lima_processlib_data_to_image(ctctrl.ReadBaseImage())
    except Core.Exception:
        # TODO : logger
        return None


LimaAcquisitionStatusImageCounters = create_io_class(CtControl.READBASEIMAGE,
                                                     data_type=LimaImage,
                                                     io_getter=_read_base_image,
                                                     hidden=True)
