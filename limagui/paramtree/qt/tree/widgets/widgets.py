import os
import enum

from PyQt5.QtWidgets import qApp

from ....tree.types.progress import Progress
from ....tree.types.action import Action, Actions
from ....tree.types.paths import FilePath

from ... import Qt, getQIcon
from .qwidgetmixin import QCellWidgetMixin, qcellwidgetdef, QCellWidget, QCellWidgetClass
from ....tree.viewadapters.actionadapter import ActionsAdapter
from ....tree.viewadapters.viewadapter import ViewAdapterBase
from .qshapes import paintDiode


class QCellLabelWidget(QCellWidgetMixin, Qt.QLabel):
    def _cell_set_editable(self, editable):
        pass

    def _cell_set_text(self, text):
        self.setText(text)


class QCellDefaultWidget(QCellWidgetMixin, Qt.QWidget):
    def __init__(self, *args, **kwargs):
        self._wid = None
        super(QCellDefaultWidget, self).__init__(*args, **kwargs)

    def _setupCellWidget(self):
        super()._setupCellWidget()
        layout = self.layout()
        if layout is None:
            layout = Qt.QVBoxLayout(self)
            layout.setContentsMargins(0, 0, 0, 0)
        wid = self._wid
        self._wid = None
        if wid:
            layout.removeWidget(self._wid)
            wid.setParent(None)
            del wid
        klass = QCellWidgetClass(self.cell)
        if klass is QCellDefaultWidget:
            klass = QCellLabelWidget
        self._wid = klass(self.cell)
        layout.addWidget(self._wid)


@qcellwidgetdef(enum.EnumMeta)
@qcellwidgetdef(enum.Enum)
class QCellComboBox(QCellWidgetMixin, Qt.QComboBox):
    def __init__(self, *args, **kwargs):
        super(QCellComboBox, self).__init__(*args, **kwargs)
        self.activated.connect(self._textActivated)
        self._resetup()

    def _resetup(self):
        enum_type = self.cell.data_type()
        if not isinstance(enum_type, (enum.Enum, enum.EnumMeta,)):
            self.clear()
            return
        list_values = [val.name for val in list(enum_type)]
        self.addItems(list_values)
        # self.activated.connect(self._textActivated)

    def _textActivated(self, index):
        text = self.itemText(index)
        enum_type = self.cell.data_type()
        if not isinstance(enum_type, (enum.Enum, enum.EnumMeta,)):
            # shouldnt happen
            return
        self._cell_editor_commit(enum_type[text])

    def _cell_set_view_data(self, view_data):
        values = [self.itemText(i) for i in range(self.count())]
        if values:
            try:
                idx = values.index(view_data.name)
                self.setCurrentIndex(idx)
            except:
                # TODO
                raise RuntimeError(f'Value {view_data.name} not found in combobox.')

    def setEditable(self, editable):
        super().setEditable(False)


@qcellwidgetdef(Progress)
class QCellProgressBar(QCellWidgetMixin, Qt.QProgressBar):
    def _cell_set_view_data(self, view_data):
        self.setValue(view_data)


@qcellwidgetdef(int)
class QCellSpinBox(QCellWidgetMixin, Qt.QSpinBox):
    def __init__(self, *args, **kwargs):
        super(QCellSpinBox, self).__init__(*args, **kwargs)

        # self.setToolTip(ioItem.tooltip())
        # self.setStatusTip(ioItem.tooltip())
        self.setFrame(False)
        self.setMinimum(-10**9)
        self.setMaximum(10**9)
        self.editingFinished.connect(self._onEditingFinished)

    def _onEditingFinished(self):
        value = self.value()
        self._cell_editor_commit(value)

    def _cell_set_view_data(self, view_data):
        self.setValue(view_data)


@qcellwidgetdef(float)
class QCellDoubleSpinBox(QCellWidgetMixin, Qt.QDoubleSpinBox):
    def __init__(self, *args, **kwargs):
        super(QCellDoubleSpinBox, self).__init__(*args, **kwargs)
        self.setDecimals(5)
        self.setFrame(False)
        self.setMinimum(-10**9)
        self.setMaximum(10**9)

        # self.setValue(self._cell().editor_data())
        self.editingFinished.connect(self._editingFinished)

    def _editingFinished(self):
        value = self.value()
        self._cell_editor_commit(value)

    def _cell_set_view_data(self, view_data):
        self.setValue(view_data)

    def textFromValue(self, value):
        return f'{value}'


@qcellwidgetdef(Action)
@qcellwidgetdef(Actions)
class QCellActionWidget(QCellWidgetMixin, Qt.QWidget):
    persistent = True

    def __init__(self, *args, **kwargs):
        self._qaw_is_init = False
        self._qaw_icons = {}
        super(QCellActionWidget, self).__init__(*args, **kwargs)
        # self._qaw_setup()

    def _resetup(self):
        if self._qaw_is_init:
            return
        super()._resetup()
        # self._qaw_setup()

    def _setupCellWidget(self):
        super()._setupCellWidget()
        self._qaw_setup()

    def _qaw_setup(self):
        view_adapter = self._cell_view_adapter()
        if not isinstance(view_adapter, (ActionsAdapter,)):
            return
        actions = view_adapter.actions()

        layout = Qt.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)

        self.__qActionGrp = qActionGrp = Qt.QActionGroup(self)
        qActionGrp.setExclusive(actions.exclusive)

        # size = min(option.rect.height(), option.rect.width())
        # size = Qt.QSize(size, size)

        icons = self._cell_view_adapter().icon()

        # view_init = self.view_init_data()
        # icons = view_init.get('icon')
        if not isinstance(icons, (list, tuple,)):
            icons = [icons]
        # if len(icons) < len(actions):
        #     icons += [None] * (len(actions) - len(icons))

        for i_action, action in enumerate(actions):
            text = action.text or ''
            qAction = qActionGrp.addAction(text)
            if not actions.exclusive and action.checkable is None:
                checkable = self.cell.checkable() #not view_adapter.write_only()
            else:
                checkable = actions.exclusive or action.checkable or False
            qAction.setCheckable(checkable)
            if action.radio:
                qAction.setCheckable(True)
                qBn = Qt.QRadioButton(text)
                qBn.clicked.connect(qAction.trigger)
                qAction.toggled.connect(qBn.setChecked)
                qBn.setAutoExclusive(actions.exclusive)
            elif action.checkbox:
                qAction.setCheckable(True)
                qBn = Qt.QCheckBox(text)
                qBn.clicked.connect(qAction.trigger)
                qAction.toggled.connect(qBn.setChecked)
                qBn.setAutoExclusive(actions.exclusive)
            else:
                qBn = Qt.QToolButton()
                qBn.setDefaultAction(qAction)
            icon = action.icon or icons[i_action] or self._qaw_icons.get(i_action)
            if icon:
                icon = getQIcon(icon)
                qAction.setIcon(icon)

            # qToolBn.setFixedSize(size)
            # style = Qt.QApplication.style()
            # icon = style.standardIcon(Qt.QStyle.SP_DialogCloseButton)Oui, c’est une vraie personne que je connais.
            # qToolBn.setIcon(icon)
            # print(qToolBn.size())
            # qToolBn.setMinimumHeight(option.rect.height())
            # qToolBn.setMaximumHeight(option.rect.height())
            layout.addWidget(qBn)

        qActionGrp.triggered.connect(self._triggered)

        layout.addStretch(1)
        # self._cell_refresh()

        self._qaw_is_init = True

    def setIcon(self, icon, idx=0):
        self._qaw_icons[idx] = icon
        if self._qaw_is_init:
            actions = self.__qActionGrp.actions()
            actions[idx].setIcon(icon)

    # def _cell_set_enabled(self, enabled):
    #     print('ENABBBBB', enabled)
    #     self.setEnabled(enabled)
    #     self.__qActionGrp.setEnabled(enabled)
    #     for action in self.__qActionGrp.actions():
    #         action.setEnabled(enabled)
    #     print('SSHHHH', self.__qActionGrp.isEnabled())

    # def setEnabled(self, enabled):
    #     super().setEnabled(enabled)
    #     self.__qActionGrp.setEnabled(enabled)

    def _triggered(self, action):
        qActionGrp = self.__qActionGrp
        actions = qActionGrp.actions()

        if qActionGrp.isExclusive():
            data = [act.isChecked() for act in actions]
        else:
            data = []
            for act in actions:
                if act.isCheckable():
                    data.append(act.isChecked())
                elif act is action:
                    if act.isCheckable():
                        data.append(act.isChecked())
                    else:
                        data.append(True)
                else:
                    data.append(False)

        if len(data) == 1:
            data = data[0]

        self._cell_editor_commit(data)

    def _cell_set_view_data(self, view_data):
        if self._qaw_is_init:
            actions = self.__qActionGrp.actions()
            try:
                nData = len(view_data)
            except TypeError:
                nData = 1
                view_data = [view_data]

            nActions = len(actions)

            if nData != nActions:
                raise ValueError(f'mismatch betwen data and actions: {view_data}.')
            if nData >= 1:
                if isinstance(view_data[0], (Action,)):
                    view_data = [act.checked for act in view_data]

            if nData == 1 and actions[0].isCheckable():
                if view_data[0] is not None:
                    actions[0].setChecked(view_data[0])
            else:
                for action, checked in zip(actions, view_data):
                    if action.isCheckable():
                        action.setChecked(checked)

        return True


class QCellDiodeWidget(QCellWidgetMixin, Qt.QWidget):
    def __init__(self, *args, **kwargs):
        super(QCellDiodeWidget, self).__init__(*args, **kwargs)

        self._colors = {}
        self._color = Qt.QColor()

        size = Qt.QApplication.style().pixelMetric(Qt.QStyle.PM_ToolBarIconSize)
        self.setMinimumSize(Qt.QSize(size, size))

    def setColorDict(self, colorDict):
        self._colors = colorDict
        self._cell_refresh()

    def colorDict(self):
        return self._colors

    def _cell_set_view_data(self, view_data):
        if self._colors:
            color = self._colors.get(view_data)
        else:
            color = self.cell.decoration()
        if color is None:
            color = Qt.QColor()
        else:
            try:
                color = Qt.QColor(color)
            except:
                print(f'Failed to convert {color} to QColor.')
                color = None
        self._color = color
        self.update()

    def paintEvent(self, event):
        super(QCellDiodeWidget, self).paintEvent(event)
        paintDiode(self._color,
                   self.rect(),
                   Qt.QPainter(self))


        size = Qt.QApplication.style().pixelMetric(Qt.QStyle.PM_ToolBarIconSize)
        # self.setMinimumSize(Qt.QSize(size, size))


class QCellAction(QCellWidgetMixin, Qt.QAction):
    def __init__(self, *args, **kwargs):
        super(QCellAction, self).__init__(*args, **kwargs)

    def _setupCellWidget(self):
        super()._setupCellWidget()
        self.triggered.connect(self._onClicked)
        icon = self._cell_view_adapter().icon()
        # TODO : super ugly.
        if icon:
            try:
                icon = icon[0]
                if icon:
                    self.setIcon(getQIcon(icon[0]))
            except:
                pass

        cell = self.cell
        # checkable = not cell.write_only()
        checkable = cell.checkable()
        self.setCheckable(checkable)

    def _cell_set_data(self, data):
        """
        Model to widget
        """
        if self.isCheckable():
            self.setChecked(data)

    def _onClicked(self, checked):
        self._cell_editor_commit(checked or not self.isCheckable())


class QCellWidgetAction(QCellWidgetMixin, Qt.QWidgetAction):
    def __init__(self, *args, cellWidKlass=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._cellWidgetKlass = cellWidKlass
        self._widget = None

    def _data_changed(self, *args, **kwargs):
        pass

    def _resetup(self):
        if self._widget:
            wid = self._widget
            self._widget = None
            self.releaseWidget(self._widget)

    def createWidget(self, parent):
        cell = self._cell
        if self._cellWidgetKlass is None:
            self._cellWidgetKlass = QCellWidgetClass(cell)
        widget = self._cellWidgetKlass(cell, parent=parent)
        # couldnt find another way to keep the QToolButton from expanding
        widget.setSizePolicy(Qt.QSizePolicy.Maximum, Qt.QSizePolicy.Maximum)
        return widget


class QCellToolButton(QCellWidgetMixin, Qt.QToolButton):
    def __init__(self, *args, **kwargs):
        super(QCellToolButton, self).__init__(*args, **kwargs)
        # self.setAutoRaise(True)
        action = QCellAction(self.cell, parent=self)
        self.setDefaultAction(action)


class QCellToolButtonMenu(QCellWidgetMixin, Qt.QToolButton):
    def __init__(self, *args, cellWidKlass=None, **kwargs):
        super(QCellToolButtonMenu, self).__init__(*args, **kwargs)

        hPolicy = self.sizePolicy().horizontalPolicy()
        self.setSizePolicy(hPolicy, Qt.QSizePolicy.Expanding)

        menu = Qt.QMenu(self)
        self.setMenu(menu)

        widAction = QCellWidgetAction(data=self.cell,
                                      parent=self,
                                      cellWidKlass=cellWidKlass)
        menu.addAction(widAction)

        self.setPopupMode(Qt.QToolButton.InstantPopup)
        self.setToolButtonStyle(Qt.Qt.ToolButtonTextBesideIcon)
        # couldnt find another way to keep the QToolButton from expanding
        self.setSizePolicy(Qt.QSizePolicy.Maximum, Qt.QSizePolicy.Maximum)

        style = """
       QToolButton {
            border: 0.5px solid #8f8f91;
            border-radius: 2px;
            padding-right: 6px;
            padding-top: 4px;
            padding-bottom: 4px;
            background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                            stop: 0 #f6f7fa, stop: 1 #dadbde);
        }

        QToolButton:pressed {
            background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                            stop: 0 #dadbde, stop: 1 #f6f7fa);
        }

        QToolButton:default {
            border-color: navy; /* make the default button prominent */
        }
        """
        self.setStyleSheet(style)

    def _cell_set_text(self, text):
        self.setText(text)



# @qcellwidgetdef(FilePath)
class PathSelectionWidget(Qt.QWidget):
    sigPathSelected = Qt.Signal(str)

    def __init__(self, parent=None,
                 directory=False,
                 title=None,
                 filter=None):
        super(PathSelectionWidget, self).__init__(parent=parent)

        layout = Qt.QGridLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)

        self._directory = directory
        self._title = title or 'Select'
        self._filter = filter

        self._lEdit = lEdit = Qt.QLabel()
        lEdit.setFrameStyle(Qt.QFrame.Panel | Qt.QFrame.Sunken)
        icon = Qt.qApp.style().standardIcon(Qt.QStyle.SP_DirOpenIcon)
        pickBn = Qt.QToolButton()
        pickBn.setIcon(icon)
        pickBn.clicked.connect(self._pickClicked)

        layout.addWidget(lEdit, 0, 0)
        layout.addWidget(pickBn, 0, 1)

    # def _cell_set_text(self, text):
    #     self._lEdit.set_text(text)

    def setPath(self, path):
        # if os.path.exists(path):
        self._lEdit.setText(path)
        self.sigPathSelected.emit(path)
        # else:
            # Qt.QMessageBox.critical(self, 'Invalid selection',  f'Path doesn\'t exist: {path}.')

    def getCurrentPath(self):
        return self._lEdit.text()

    def _pickClicked(self):
        if self._directory:
            path = Qt.QFileDialog.getExistingDirectory(self,
                                                       self._title,
                                                       self._lEdit.text())
        else:
            path = Qt.QFileDialog.getOpenFileName(self,
                                                  'Select image folder',
                                                  self._lEdit.text(),
                                                  self._filter or 'All files (*.*)')
            path = path and path[0]
        if path:
            self.setPath(path)
