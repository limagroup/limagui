# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


def checkTypes(*argsType, **kwargsType):
    def inner(func):
        if not __debug__:
            return func
        else:
            def wrapper(*args, **kwargs):
                for argIdx, arg in enumerate(args):
                    check = (len(argsType[argIdx]) == 0
                             or (arg is None and None in argsType[argIdx])
                             or any([isinstance(arg, argType)
                                     for argType in argsType[argIdx]
                                     if argType is not None]))
                    if not check:
                        msg = ('Invalid type for argument #{0} in function {1}'
                               ''.format(argIdx, func.__name__))
                        raise TypeError(msg)
                for argKey, argValue in kwargs.items():
                    check = (argKey not in kwargsType
                             or len(kwargsType[argKey]) == 0
                             or (
                                 argValue is None and None in kwargsType[
                                     argKey])
                             or any([isinstance(argValue, argType)
                                     for argType in kwargsType[argKey]
                                     if argType is not None]))
                    if not check:
                        msg = ('Invalid type for argument {0} in function {1}'
                               ''.format(argKey, func.__name__))
                        raise TypeError(msg)

                return func(*args, **kwargs)

            return wrapper

    return inner
