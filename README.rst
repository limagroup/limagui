limagui
=======

.. image:: .limagui/applications/limagui/doc/limagui.png

Installation
------------

The latest development version can be obtained from the git repository::

    git clone https://gitlab.esrf.fr/limagroup/limagui.git
    cd limagui

And then install::
    pip install . [--user]

Dependencies using conda and pip
--------------------------------

Required:

 * conda packages:

  * `lima-core`
  * `python=3.7`
  * `numpy`
  * `ruamel.yaml`
  * `gevent`
  * ` silx`
  * `python-dateutil`

 * pip packages:
 
  * `opencv-python`

Optional:

* pytango (conda package only)
* any lima camera you want to use locally (i.e: not through tango)

Conda installation example
--------------------------

::

 # create a new conda environment named limagui_env for instance
 conda create -n limagui_env
 conda activate limagui_env
 # install all the conda packages in once
 conda install -c conda-forge -c esrf-bcu -c tango-controls lima-core ruamel.yaml gevent pytango silx python-dateutil
 # only pip opencv package is compatible with the previous installation, do not install the conda opencv pacakage
 pip install opencv-python
 # now install limagui from your git repo.
 cd <mylimagui-repo.git>
 pip install . [--user]


Supported platforms
-------------------
* Linux
* Windows (should be...)

Control and view Lima cameras (local or tango).

Usage
=====

Using limagui
-------------

::

    usage: limagui [-h] [-c config_file] [-r] [-n [camera_name]] camera_type ...

    Limagui

    positional arguments:
    camera_type       Camera type
        simulator       Lima Simulator
        prosilica       Prosilica camera
        basler          Basler camera
        v4l2            V4L2 camera
        tango           remote LimaCCDs server

    optional arguments:
    -h, --help        show this help message and exit
    -c config_file    Config file
    -r                Start in read only mode
    -n [camera_name]  Camera name


Several cameras can be used at the same time.
Type limagui <camera_type> -h to display specific help.

Example: Basler camera:

::

    limagui basler 192.168.1.1

Example: Simulator
::

    limagui simulator

Example: A Prosilica and a Basler in read only mode (note the comma).
::

    limagui basler 192.168.1.1, prosilica 192.168.2.1 -r

Example: a LimaCCDs tango camera named id00/limaccds/my-cam (needs pytango):
::

    limagui tango my-cam

Example: a LimaCCDs tango camera (needs pytango), with the BPM widget and the "screen" wago control:
::

    limagui tango my-cam --bpm --scr


Tested (aka : they work on my computer) camera types: Simulator, Basler, Prosilica
    

Install limagui then execture '''limagui''' or "python -m limagui.applications.limagui"


License
-------

The source code of limagui is licensed under the ? license.

