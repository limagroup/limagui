# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


import threading

from collections import OrderedDict

from silx.utils.weakref import WeakMethod

_runners = OrderedDict()


def add_runner(name, runner):
    global _runners

    if name in _runners:
        raise ValueError('A runner with this name has already been'
                         'registered.')

    _runners[name] = runner


def get_runner(name=None):
    global _runners
    if not _runners:
        raise ValueError('No runner has been registered yet.')
    if name:
        return _runners[name]
    return _runners[list(_runners.keys())[0]]


def Runner(*args, **kwargs):
    rname = kwargs.pop('rname', None)
    run_class = get_runner(name=rname)
    return run_class(*args, **kwargs)


class RunnerBase(object):
    """"
    An object that will call a function in a new thread, then
    """

    func = property(lambda self: self.__func())

    callback = property(lambda self: self.__callback and self.__callback())

    stopped = property(lambda self: self.__stopped)

    data = property(lambda self: self.__data)

    def __init__(self, func, callback=None):
        # TODO: callback in caller's thread
        super(RunnerBase, self).__init__()
        # TODO : allow args, kwargs
        if callback:
            self.__callback = WeakMethod(callback)
        else:
            self.__callback = None

        self.__func = WeakMethod(func)
        self.__thread = None
        self.__stopped = False
        self.__data = None

    def start(self):
        if self.is_alive():
            return
            # raise ValueError('Thread already running.')
        self.__data = None
        func = self.__func()
        self.__stopped = False
        if func:
            self.__thread = threading.Thread(target=self._run)
            self.__thread.start()

    def stop(self):
        self.__stopped = True
        if self.__thread:
            self.__thread.join()

    def wait(self, timeout=None):
        if self.is_alive():
            if timeout is not None:
                self.__thread.join(timeout)
            else:
                self.__thread.join()
        return self.__data

    def is_alive(self):
        return self.__thread and self.__thread.is_alive()

    def _run(self):
        func = self.func
        if func:
            data = func()
        else:
            data = None
        self._callback(data)

    def _callback(self, data=None):
        self.__data = data
        if self.__callback:
            try:
                callback = self.__callback()
                if callback:
                    callback(data)
            except Exception as ex:
                print(__file__)
                print(self.__class__, ex)
                raise


# add_runner('python', Runner)
