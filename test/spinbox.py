import enum
from functools import partial
from collections import OrderedDict

from limagui.paramtree import Node, Action, Actions, IoAdapterBase, Cell
from limagui.paramtree.qt import QModel, TreeView
from limagui.paramtree.tree.types import Progress
from limagui.paramtree.qt import Qt
from limagui.paramtree.qt.tree.widgets import QCellWidget
from limagui.paramtree.qt.tree.widgets import widgets as cellwids


app = Qt.QApplication([])

data = OrderedDict({'int': 666,
                    'float': 123.456})

node = Node(name='Foo', data=data)

model = QModel()
model.startModel()
idx = model.insertNode(node)

win = Qt.QMainWindow()

treeA = TreeView()
treeA.setModel(model)

wid = Qt.QWidget()
layout =Qt.QHBoxLayout(wid)
layout.addWidget(treeA)

index = model.index(0, 0)
treeA.expand(index)

win.setCentralWidget(wid)
win.show()

app.exec_()