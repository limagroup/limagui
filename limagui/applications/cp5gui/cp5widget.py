import tango

from limagui.paramtree import IoGroup

from limagui.paramtree.qt import Qt, ParamTreeView
from limagui.paramtree.qt.tree.widgets import QCellLabelWidget, QCellDiodeWidget
from limagui.paramtree.qt.tree.widgets.tangowidgets import TangoStatusWidget

from .cp5ios import Cp5IoNames


def get_cp5coolerds(srv_name):
    db = tango.Database()
    if not srv_name.startswith('cp5coolerDS/'):
        srv_name = 'cp5coolerDS/' + srv_name
    devices = db.get_device_class_list(srv_name)
    devices = dict(zip(*[reversed(devices)] * 2))
    return tango.DeviceProxy(devices['cp5cooler'])


class Cp5Widget(Qt.QWidget):
    def __init__(self, *args, io_group=None, cp5_dev_name=None, **kwargs):
        super().__init__(*args, **kwargs)

        if io_group is None:
            io_group = IoGroup()
        if not io_group.has_source('cp5cooler'):
            if cp5_dev_name is None:
                raise RuntimeError(f'cp5coolerDS name not provided.')
            device = get_cp5coolerds(cp5_dev_name)
            io_group.add_source('cp5cooler', device)
        
        layout = Qt.QGridLayout(self)

        tgStateWidget = TangoStatusWidget(io_group.source('cp5cooler'))
        layout.addWidget(tgStateWidget, 0, 0)

        tree = ParamTreeView()
        for io_name in Cp5IoNames:
            tree.model().insertNodeData(name=io_name.name, data=io_group.io_adapter(io_name))
        layout.addWidget(tree, 1, 0)