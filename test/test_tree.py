#!/usr/bin/env python
#
# Usage : python limatango.py LimaCCDs/bcu_basler
#

import sys
import time

# from Lima.Core import CtVideo as LimaCtVideo

from limagui.paramtree.qt import Qt
# from limagui.paramtree.qt.iomodel.qtapp import QtApp

from limagui.applications.limagui.lima.factories.limafactory import LimaFactory
from limagui.applications.limagui.limatango.limatangofactory import LimaTangoFactory

# from limagui.paramtree.qt.iomodel import iobaseioadapter, iobasetreeadapter


from limagui.applications.limagui.limagui.widgets.mainwindow import new_lima_model
from limagui.paramtree.qt import TreeView
from limagui.paramtree.qt import QModel
from limagui.paramtree.iomodel.iobase import registered_io_classes, registered_io_names, IOBase, ioclassdef

# from limagui.plugins.limatango.model.tangoionames import TangoLimaCtImage
from limagui.applications.limagui.lima.lima_io.limaionames import CtImage, CtControl, CtAcquisition, CtVideo, CtAccumulation, LimaTriggers

try:
  from limagui.applications.limagui.lima.lima_io.camera.simulator import SimulatorIoNames
except:
  pass

from limagui.paramtree.qt.tree.widgets.qwidgetmixin import QCellWidget
from limagui.paramtree.qt.tree.widgets.widgets import QCellDiodeWidget, QCellToolButtonMenu


from limagui.paramtree.iomodel import IoNamesEnum, ionames_enum_def
class Bla(IoNamesEnum):
    BLA = 'Bli@:Bla'

ss = 0
xx = {'xx': 3}

@ioclassdef(Bla.BLA,
            data_type=int)
class Blabla(IOBase):
    def io_getter(self, ctctrl):
      global ss
      global xx
      print('time in', ss, xx)
      time.sleep(ss)
      print('time out')
      return xx['xx']

    def io_setter(self, ctctrl, x):
      global xx
      print('SET', xx)
      xx['xx'] = x
      print(xx)


app = Qt.QApplication([])

if 1:
    # model_args = (sys.argv[1],)
    mdl = new_lima_model('yolo', 'Simulator')
else:
    model_args = ('d20-simu',)
    mdl = new_lima_model('yolo', 'LimaCCDs', *model_args)

# from limagui.applications.limagui import limagui#.paramtree.limaflip import LimaFlipTreeAdapter
# from limagui.core.type.enum import register_enums, registered_enums, get_enum
from limagui.applications.limagui.limatango.tango_io.tangoionames import TangoLimaCCDs

# print(registered_io_names())
# print('#####', io)
# print('===', io.data())

win = Qt.QWidget()
layout = Qt.QVBoxLayout(win)

if 1:
  tree = TreeView()
  qmodel = QModel(tree)
  tree.setModel(qmodel)
  qmodel.startModel()

  # mdl.add_io_class(Blabla)

  m = 1
  if m == 1:
      ios = [
        # mdl.get_io_instance(CtAccumulation.ACTIVE),
        # mdl.get_io_instance(CtAccumulation.PIXEL_THRESHOLD_VALUE),
          # mdl.get_io_instance(Bla.BLA)
          # mdl.get_io_instance(CtVideo.ACTIVE),
            # mdl.get_io_instance(CtAcquisition.ACQNBFRAMES),
            # mdl.get_io_instance(CtImage.ROTATION),
            mdl.get_io_instance(CtImage.BIN),
            # mdl.get_io_instance(CtImage.FLIP),
            # mdl.get_io_instance(CtImage.ROI),
            # mdl.get_io_instance(CtVideo.STARTLIVE),
            # mdl.get_io_instance(CtVideo.STOPLIVE),
            # mdl.get_io_instance(CtVideo.GAIN),
            # mdl.get_io_instance(CtVideo.MODE),
            # mdl.get_io_instance(CtVideo.ROI),
            # mdl.get_io_instance(CtVideo.VIDEOSOURCE),
            # mdl.get_io_instance(CtVideo.SUPPORTEDVIDEOMODE),
            # mdl.get_io_instance(CtVideo.BIN),
            # mdl.get_io_instance(CtImage.ACQNBFRAMES),
            # mdl.get_io_instance(CtAcquisition.ACQEXPOTIME),
            # mdl.get_io_instance(CtControl.PREPAREACQ),
            # mdl.get_io_instance(CtControl.STARTACQ),
            # mdl.get_io_instance(LimaTriggers.STATUS_CALLBACK),
            # mdl.get_io_instance(CtControl.STATUS),
            # mdl.get_io_instance(CtControl.IMAGESTATUS),
            # mdl.get_io_instance(CtControl.STATUS_ACQUISITIONSTATUS),
            # mdl.get_io_instance(TangoLimaCCDs.TANGO_PUSH_IMAGE_EVENTS),
          #    mdl.get_io_instance(CtVideo.MODE),
            # mdl.get_io_instance(CtAcquisition.ACQMODE),
            # mdl.get_io_instance(CtAcquisition.ACCDEADTIME),
            # mdl.get_io_instance(CtAcquisition.ACCNBFRAMES),
            # mdl.get_io_instance(SimulatorIoNames.FILL_TYPE),
            # mdl.get_io_instance(SimulatorIoNames.ROTATION_AXIS),
            # mdl.get_io_instance(SimulatorIoNames.ROTATION_ANGLE),
            # mdl.get_io_instance(SimulatorIoNames.ROTATION_SPEED),
            #mdl.get_io_instance(SimulatorIoNames.DIFFRACTION_SPEED),
            #mdl.get_io_instance(SimulatorIoNames.DIFFRACTION_POS),
            # mdl.get_io_instance(SimulatorIoNames.PEAKS),
            # mdl.get_io_instance(SimulatorIoNames.PEAK_ANGLES),
            # mdl.get_io_instance(BpmDeviceServerNames.START),
            # mdl.get_io_instance(BpmDeviceServerNames.STOP),
            # mdl.get_io_instance(BpmDeviceServerNames.STATE),
            # mdl.get_io_instance(BpmDeviceServerNames.FWHM_X),
            # mdl.get_io_instance(BpmDeviceServerNames.FWHM_Y),
            # mdl.get_io_instance(BpmDeviceServerNames.COLORMAP),
            # mdl.get_io_instance(LimaTriggers.STATUS_CALLBACK)
            ]
      qmodel.insertNodeData({io.io_name.name:io for io in ios})
  elif m == 2:
      treedict = mdl.ios_tree(plugins='Lima', sources=['CtImage'], instances=True)
      qmodel.insertNodeData(treedict)
  elif m == 3:
    io = mdl.get_io_instance(CtImage.ROTATION)



# class BpmDeviceServerNames(IoNamesEnum):
#     START
#     STOP
#     STATE
#     FWHM_X = 'BpmDeviceServer@BpmDeviceServer:fwhm_x'
#     FWHM_Y = 'BpmDeviceServer@BpmDeviceServer:fwhm_y'
#     COLORMAP = 'BpmDeviceServer@BpmDeviceServer:color_map'
#     BVDATA = 'BpmDeviceServer@BpmDeviceServer:bvdata'
#     AUTOSCALE = 'BpmDeviceServer@BpmDeviceServer:autoscale'
#     X = 'BpmDeviceServer@BpmDeviceServer:x'
#     Y = 'BpmDeviceServer@BpmDeviceServer:y'
#     LUT = 'BpmDeviceServer@BpmDeviceServer:lut_method'

  # io = mdl.get_io_instance(CtAcquisition.ACQNBFRAMES)
  # sub_io = mdl.get_substitute_io(CtAcquisition.ACQNBFRAMES)
  # print('SUB: ', sub_io)
  # print('BUS', io.data())

  rIndex = qmodel.index(0, 0)
  tree.setRootIndex(rIndex)

  # ss = 5
  # xx['xx'] = 34
  # io = mdl.get_io_instance(Bla.BLA)

  # print('YOLO')
  # print('IN', io.data(cache=False, wait=False))
  # print('OUT')

  # i = 12

  # def setval():
  #     global i
  #     #print(i)
  #     # io1.set_data(i)
  #     ct.setAcqNbFrames(i)
  #     i += 1

  # ct = mdl.data_source('CtAcquisition')
  # ct.setAcqNbFrames(3)

  # timer = Qt.QTimer()
  # timer.timeout.connect(setval)
  # timer.setSingleShot(False)
  # # timer.start(300)

  layout.addWidget(tree)

if 0:
  acqwid = QCellToolButtonMenu(mdl.get_io_instance(CtAcquisition.ACQNBFRAMES))
  layout.addWidget(acqwid)
  acqwid = QCellToolButtonMenu(mdl.get_io_instance(CtAcquisition.ACQEXPOTIME))
  layout.addWidget(acqwid)

print('SHOWIN')
win.show()
print('SHOWIN0')
app.exec_()
# print('===', io1.data())