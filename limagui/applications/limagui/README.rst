Control and view Lima cameras (local or tango).

limagui
=======

.. image:: ./doc/limagui.png

Using limagui
-------------

    python -m limagui.applications.limagui [-n name] camera_type [camera_args] ...

Several cameras can be used at the same time.

Example: Basler camera:

    python -m limagui.applications.limagui basler 192.168.1.1

Example: Simulator

    python -m limagui.applications.limagui simulator

Example: A Prosilica and a Basler

    python -m limagui.applications.limagui basler 192.168.1.1 prosilica 192.168.2.1

Example: a LimaCCDs tango camera (needs pytango):

    # connecting to a tango LimaCCDs server named id00/limaccds/my-cam

    python -m limagui.applications.limagui tango my-cam

Tested (aka : they work on my computer) camera types: Simulator, Basler, Prosilica
    

Install limagui then run the script: limagui/scripts/limagui or "python -m limagui.applications.limagui"

Dependencies
------------

- limagui
- lima-core (conda-package)
- pytango (optional, conda package)
- target Lima camera module (optional, only Simulator supported at the moment)
- opencv-python (pip package)