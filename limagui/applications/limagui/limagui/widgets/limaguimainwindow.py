from collections import OrderedDict
from limagui.paramtree.qt import Qt
from .camerawindow import CameraWindow


class LimaGuiMainWindow(Qt.QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self._cameraWindows = OrderedDict()
        self._tabWidget = Qt.QTabWidget()
        self._tabWidget.setTabPosition(Qt.QTabWidget.West)

        style = """
            QTabBar::tab:selected {
                background: qlineargradient(spread:pad,
                                            x1:0, y1:0,
                                            x2:1, y2:0,
                                            stop:0.653465 rgba(0, 140, 0, 255),
                                            stop:1 rgba(255, 255, 255, 255))
            }
            # QTabBar::tab:!selected {
            #     background: qlineargradient(spread:pad,
            #                                 x1:0, y1:0,
            #                                 x2:1, y2:0,
            #                                 stop:0.653465 rgba(240, 0, 0, 255),
            #                                 stop:1 rgba(255, 255, 255, 255))
            # }
        """
        self._tabWidget.tabBar().setStyleSheet(style)

        self.setCentralWidget(self._tabWidget)

    def closeEvent(self, *args, **kwargs):
        for camWindow in self._cameraWindows.values():
            camWindow.stop_io_group()
        return super().closeEvent(*args, **kwargs)

    def addCamera(self,
                  name=None,
                  cam_type=None,
                  read_only=False,
                  screen_io=False,
                  bpm_win=False,
                  cam_args=(),
                  cam_kwargs={}):
        if name is not None and name in self._cameraWindows:
            raise RuntimeError(f'Camera name already in use: {name}.')
        camWindow = CameraWindow(name=name,
                                 bpm_win=bpm_win,
                                 read_only=read_only,
                                 cam_type=cam_type,
                                 cam_args=cam_args,
                                 cam_kwargs=cam_kwargs)
        if name is None:
            name = camWindow.io_group().name()
        if name in self._cameraWindows.keys() or name is None:
            name = cam_type
            postfix = 1
            names = list(self._cameraWindows.keys())
            while name in names:
                name = f'{cam_type}#{postfix}'
                postfix += 1
        tabIdx = self._tabWidget.addTab(camWindow, '')
        self._cameraWindows[name] = camWindow
        self._tabWidget.tabBar().setTabButton(tabIdx,
                                              Qt.QTabBar.LeftSide,
                                              camWindow.tabBarWidget(name=name,
                                                                     screen_io=screen_io))
        #self._tabWidget.tabBar().setVisible(len(self._cameraWindows) > 1)
        return name

    def getCamera(self, name):
        return self._cameraWindows[name]
