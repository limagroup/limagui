# coding: utf-8

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"

import tango


from limagui.paramtree import get_module, io_name_is_registered
from limagui.paramtree.tree.extra import create_tg_io_class
# from limagui.paramtree.iomodel import get_io_class
# from limagui.paramtree.iomodel.extra.tangomixin import new_tango_io_class
# from limagui.paramtree.iomodel.extra.tangosource import DeviceProxySource

# from .plugins.bpmdeviceserver import get_bpm_tango_io_class
# from .tangoionames import LIMA_TANGO_IO_NAMES
# from .conversions import LIMA_TANGO_CONVERSIONS
from . import ctacquisition, ctimage, ctcontrol, ctvideo, triggers, limaccds
from .conversions import LIMA_TANGO_CONVERSIONS

# from .utils import match_io_enum_names

from ...lima.lima_io.limaionames import (CtVideo,
                                        CtControl,
                                        CtImage,
                                        CtImage,
                                        CtAcquisition,
                                        HwInterface,
                                        CtAccumulation,
                                        LimaTriggers)

from .tangoionames import (TangoLimaCtControl,
                                    TangoLimaCtVideo,
                                    TangoLimaCtImage,
                                    TangoLimaCtAcquisition,
                                    TangoLimaHwSync,
                                    TangoLimaCtAccumulation,
                                    TangoLimaTriggers,
                                    TangoLimaCCDs,
                                    LIMA_TANGO_IO_NAMES)


# from .triggers import LimaCCDsCounterCallback, LimaCCDsImgCallback, LimaCCDsStatusCallback, LimaCCDsImgStatusCallback


tango_io_triggers = {
    # TangoLimaTriggers.STATUS_CALLBACK: TangoLimaCtControl.STATUS_ACQUISITIONSTATUS,
    # TangoLimaTriggers.STATUS_CALLBACK_IMG: TangoLimaTriggers.STATUS_CALLBACK,
    # TangoLimaTriggers.STATUS_CALLBACK_CNT: (TangoLimaTriggers.STATUS_CALLBACK,
    #                                         TangoLimaTriggers.STATUS_CALLBACK_IMG),
    # TangoLimaTriggers.STATUS_CALLBACK_STATUS: (TangoLimaTriggers.STATUS_CALLBACK,
    #                                            TangoLimaTriggers.STATUS_CALLBACK_CNT,
    #                                            TangoLimaTriggers.STATUS_CALLBACK_IMG),
    # TangoLimaCCDs.TANGO_PUSH_IMAGE_EVENTS: (TangoLimaTriggers.STATUS_CALLBACK_STATUS,
    #                                         TangoLimaTriggers.STATUS_CALLBACK_IMG),
    TangoLimaCtVideo.STOPLIVE: (TangoLimaCtVideo.STARTLIVE),
    TangoLimaCtVideo.STARTLIVE: (TangoLimaCtVideo.STOPLIVE)
}


ios = [create_tg_io_class(io_name,
                          hidden=False,
                          tango_conversion=LIMA_TANGO_CONVERSIONS.get(io_name),
                          triggers=tango_io_triggers.get(io_name))
       for limaccdsenum in LIMA_TANGO_IO_NAMES
       for io_name in limaccdsenum
       if not io_name_is_registered(io_name)]


# def get_lima_tango_camera_ios(camera_type):
#     return []


# def get_lima_tango_task_ios(camera_type):
#     return []


# def get_lima_tango_io_classes():
#     tango_io_names = [e for enum in LIMA_TANGO_IO_NAMES for e in enum]
#     tango_io_classes = []#get_io_class(name) for name in lima_io_names]

#     for io_name in tango_io_names:
#         klass = get_io_class(io_name)
#         if klass is None:
#             klass = new_tango_io_class(io_name, conversions=LIMA_TANGO_CONVERSIONS,
#                                         register=False)
            
#         tango_io_classes.append(klass)

#     return tango_io_classes


# def get_substitutes():
#     ctaccumulation_ios = match_io_enum_names(TangoLimaCtAccumulation, CtAccumulation)
#     ctcontrol_ios = match_io_enum_names(TangoLimaCtControl, CtControl)
#     ctvideo_ios = match_io_enum_names(TangoLimaCtVideo, CtVideo)
#     ctimage_ios = match_io_enum_names(TangoLimaCtImage, CtImage)
#     ctacquisition_ios = match_io_enum_names(TangoLimaCtAcquisition,
#                                        CtAcquisition)
#     hwiface_ios = match_io_enum_names(TangoLimaHwSync, HwInterface)
#     return (ctaccumulation_ios + ctcontrol_ios + ctvideo_ios + ctimage_ios +
#             hwiface_ios + ctacquisition_ios)


# def device_list(srv_name):
#     # TODO : error management
#     db = tango.Database()
#     if not srv_name.startswith('LimaCCDs/'):
#         srv_name = 'LimaCCDs/' + srv_name
#     devices = db.get_device_class_list(srv_name)
#     return dict(zip(*[reversed(devices)] * 2))

    
# def add_lima_tango_sources(model, srv_name, *args, **kwargs):
#     dev_dict = device_list(srv_name)

#     print(f'Looking for LimaCCDs (srv_name={srv_name})')
#     try:
#         lima_ccds = tango.DeviceProxy(dev_dict['LimaCCDs'])
#     except KeyError as ex:
#         print('Error ', ex)
#         raise RuntimeError('LimaCCDs Tango device not found: {0}'
#                             ''.format(srv_name))
#     model.set_data_source('LimaCCDs', DeviceProxySource(lima_ccds))
#     print('LimaCCDs device found.')

#     prop = lima_ccds.get_property('LimaCameraType')
#     camera_type = prop['LimaCameraType'][0]
#     print(f'Camera type: {camera_type}')
    
#     print(f'Looking for BpmDeviceServer device (srv_name={srv_name})')
#     tango_bpm = dev_dict.get('BpmDeviceServer')
#     if tango_bpm:
#         tango_bpm_server = tango.DeviceProxy(tango_bpm)
#         print('Succes. Found a BpmDeviceServer!')
#         model.set_data_source('BpmDeviceServer',
#                               DeviceProxySource(tango_bpm_server))
#     else:
#         print('Warning: BpmDeviceServer not found.')

#     print(f'Looking for {camera_type} device (srv_name={srv_name})')
#     camera_dev = tango.DeviceProxy(dev_dict[camera_type])
#     model.set_data_source('TangoCamera', DeviceProxySource(camera_dev))
#     print('Ok. Found the camera device!')


# def add_lima_tango_ios(model,
#                        *args,
#                        **kwargs):
                 
#     tango_io_classes = get_lima_tango_io_classes()
#     triggers = [LimaCCDsCounterCallback,
#                 LimaCCDsImgCallback,
#                 LimaCCDsStatusCallback,
#                 LimaCCDsImgStatusCallback]

#     has_bpm = model.has_data_source('BpmDeviceServer')
#     lima_ccds = model.data_source('LimaCCDs')
#     prop = lima_ccds.get_property('LimaCameraType')
#     camera_type = prop['LimaCameraType'][0] 

#     if has_bpm:
#         print('Loading BpmDeviceServer io classes.')
#         bpm_ios = get_bpm_tango_io_class()
#         tango_io_classes.extend(bpm_ios)

#     plugin_name = f'..tango_io.camera.{camera_type.lower()}'
#     print(f'Lima plugin: Trying to import {plugin_name} '
#           f'camera TANGO specific module.')
#     try:
#         cam_module = get_module(plugin_name, __name__)
#         cam_ios = cam_module.get_lima_tango_io_classes()
#         tango_io_classes.extend(cam_ios)
#     except ImportError as ex:
#         print(f'Failed to import camera TANGO module: {ex}).')
#         cam_module = None
#     else:
#         print('Lima plugin: LOADED {0} module.'.format(cam_module.__name__))


# #    for poller in self._pollers:
# #        poller.start()

#     for io in tango_io_classes + triggers:
#         model.add_io_class(io)

#     tango_subs = get_substitutes()

#     if cam_module:
#         print(f'Loading substitutes from camera module '
#                 f'{cam_module}')
#         cam_subs = cam_module.get_lima_tango_substitutes()
#         tango_subs.extend(cam_subs)

#     for (new, orig) in tango_subs:
#         model.add_substitution(orig, new)

#     model.add_substitution(LimaTriggers.STATUS_CALLBACK,
#                            TangoLimaTriggers.STATUS_CALLBACK)

#     #     triggers = factory.triggers()
#     #     enable_conditions = factory.enable_conditions()

#     model._io_event_mgr.add_triggers(_tango_io_triggers)

#     #     self._io_event_mgr.add_enable_conditions(enable_conditions)
        
#         # except Exception as ex:
#         #     print('Error while setting up factory {0}({1}).'
#         #           ''.format(factory.__class__.__name__, factory.name()))
#         #     import traceback
#         #     import sys
#         #     traceback.print_exc(file=sys.stdout)
#         #     self.notify_error()