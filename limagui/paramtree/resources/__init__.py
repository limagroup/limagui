# coding: utf-8
__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"

from silx.resources import register_resource_directory
register_resource_directory('paramtree', __name__)
