# coding: utf-8
from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from limagui.paramtree.qt import Qt, getQIcon
from limagui.paramtree.qt.tree.widgets.widgets import (QCellAction,
                                                       QCellDiodeWidget,
                                                       QCellLabelWidget,
                                                       QCellToolButtonMenu,
                                                       QCellToolButton)

from ...lima.lima_io.limaionames import CtControl, CtAcquisition, CtVideo, LimaTriggers


class StatusToolBar(Qt.QToolBar):
    def __init__(self, io_group, parent=None):
        super().__init__(parent)

        self.setWindowTitle('Status')
        self.setToolTip('Status')
        self.setStatusTip('Status')

        # icon = getQIcon('lima:icons/microscope')
        icon = getQIcon(Qt.QStyle.SP_MessageBoxInformation)
        self.setWindowIcon(icon)

        acqstatus = io_group.io_adapter(
                    CtControl.STATUS)
        statusDiode = QCellDiodeWidget(acqstatus)
        self.addWidget(statusDiode)

        statusLabel = QCellLabelWidget(acqstatus)
        self.addWidget(statusLabel)


class AcquisitionToolBar(Qt.QToolBar):
    def __init__(self, io_group, parent=None):
        super().__init__(parent)

        self.setWindowTitle('Acquisition')
        self.setToolTip('Acquisition')
        self.setStatusTip('Acquisition')

        expoTime = io_group.io_adapter(CtAcquisition.ACQEXPOTIME)
        nbFrames = io_group.io_adapter(CtAcquisition.ACQNBFRAMES)

        icon = getQIcon('lima:icons/shoot')
        self.setWindowIcon(icon)

        io_names = [CtControl.PREPAREACQ,
                    CtControl.STARTACQ,
                    CtControl.STOPACQ]

        icons = ['apply_config', 'shoot', 'acq_stop']

        for io_name, icon in zip(io_names, icons):
            io = io_group.io_adapter(io_name)
            action = QCellAction(io, parent=self)
            icon = getQIcon(f'lima:/icons/{icon}')
            action.setIcon(icon)
            self.addAction(action)
            self.widgetForAction(action).setAutoRaise(False)

        self.addSeparator()

        self.addWidget(Qt.QLabel('Nb. Frames:'))
        nbFramesTb = QCellToolButtonMenu(nbFrames)
                                    #   ,
                                    #   qProps={'suffix': ' frames',
                                    #           'minimum': 0,
                                    #           'maximum': 100000},
                                    #   txtFormat='{0} frames')
        self.addWidget(nbFramesTb)

        self.addSeparator()

        self.addWidget(Qt.QLabel('Expo. Time (s):'))
        expoTimeTb = QCellToolButtonMenu(expoTime)
                                    #   qProps={'prefix': 'Expo. time : ',
                                    #           'suffix': ' s',
                                    #           'minimum': 1e-6,
                                    #           'maximum': 10000,
                                    #           'decimals': 6},
                                    #   txtFormat='{0} s')
        self.addWidget(expoTimeTb)


class VideoToolBar(Qt.QToolBar):
    def __init__(self, io_group, parent=None):
        super(VideoToolBar, self).__init__(parent)

        self.setWindowTitle('Lima Video')
        self.setToolTip('Lima Video')
        self.setStatusTip('Lima Video')

        startLive = io_group.io_adapter(CtVideo.STARTLIVE)
        stopLive = io_group.io_adapter(CtVideo.STOPLIVE)
        videoExpoTime = io_group.io_adapter(CtVideo.EXPOSURE)
        videoActive = io_group.io_adapter(CtVideo.LIVE)

        icon = getQIcon('lima:icons/camera')
        self.setWindowIcon(icon)

        self.addSeparator()

        ledWid = QCellDiodeWidget(videoActive)
        ledWid.setColorDict({True:'blue', False:'green'})
        self.addWidget(ledWid)

        self.addSeparator()

        icon = getQIcon('lima:icons/live_start')
        action = QCellAction(data=startLive, parent=self)
        action.setIcon(icon)
        self.addAction(action)
        self.widgetForAction(action).setAutoRaise(False)

        icon = getQIcon('lima:icons/live_stop')
        action = QCellAction(data=stopLive, parent=self)
        action.setIcon(icon)
        self.addAction(action)
        self.widgetForAction(action).setAutoRaise(False)

        self.addSeparator()

        self.addWidget(Qt.QLabel('Expo. Time (s):'))
        expoTimeTb = QCellToolButtonMenu(data=videoExpoTime, parent=self)
                                    #   qProps={'prefix': 'Expo. time : ',
                                    #           'suffix': ' s',
                                    #           'minimum': 1e-6,
                                    #           'maximum': 10000,
                                    #           'decimals': 6},
                                    #   txtFormat='{0} s')
        self.addWidget(expoTimeTb)


class LimaOptionsToolBar(Qt.QToolBar):
    def __init__(self, io_group, parent=None):
        super().__init__(parent)

        self.setWindowTitle('Options')
        self.setToolTip('Options')
        self.setStatusTip('Options')

        icon = getQIcon('paramtree:icons/gears')
        self.setWindowIcon(icon)

        action = QCellToolButtonMenu(io_group.io_adapter(LimaTriggers.IMG_POLL_DELAY))
        self.addWidget(Qt.QLabel('ReadImage delay (ms):'))
        self.addWidget(action)