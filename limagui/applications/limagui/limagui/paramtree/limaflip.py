# -*- coding: utf-8 -*-

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"


from Lima.Core import Flip
from limagui.paramtree import treeadapterdef, SubListTreeAdapter


@treeadapterdef(Flip)
class LimaFlipTreeAdapter(SubListTreeAdapter):
    def __init__(self, *args, **kwargs):
        kwargs['sub_names'] = ['X', 'Y']
        kwargs['editable'] = False
        super(LimaFlipTreeAdapter, self).__init__(*args, **kwargs)

    def _sub_to_root(self, root_data, row, cell_data):
        flip = None
        if row == 0:
            flip = Flip(cell_data, root_data.y)
        elif row == 1:
            flip = Flip(root_data.x, cell_data)
        return flip

    def _root_to_sub(self, root_data):
        return [root_data.x, root_data.y]
