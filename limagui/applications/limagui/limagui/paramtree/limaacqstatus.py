# -*- coding: utf-8 -*-

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"


from Lima import Core
from Lima.Core import CtControl

from limagui.paramtree import treeadapterdef, TreeAdapterBase, ViewAdapterBase, viewadapterdef

from ...lima.type.enums import AcqStatus


ACQ_STATUS_COLORS = {
    Core.AcqReady: 'light green',
    Core.AcqFault: 'red',
    Core.AcqRunning: 'blue',
    Core.AcqConfig: 'orange'
}


@viewadapterdef(CtControl.Status)
class LimaAcqStatusViewAdapter(ViewAdapterBase):
    def to_text(self, model_data):
        if model_data is not None:
            return AcqStatus(model_data.AcquisitionStatus).name
        return super().to_text(model_data)

    def to_color(self, model_data):
        return ACQ_STATUS_COLORS.get(model_data.AcquisitionStatus)


# @treeadapterdef(CtControl.Status)
# class LimaAcqStatusTreeAdapter(TreeAdapterBase):

#     def child_count(self):
#         return 2

#     def _sort_data(self, data):
#         status = data.data()
#         cell_data = [{'data':'Status', 'editable':False},
#                      {'data':data, 'editable':False,
#                       'view_adapter_cls':LimaAcqStatusViewAdapter}]
#         node_data = [{'name':'Counters',
#                       'data':status.ImageCounters,
#                       'editable':False}]
#         return cell_data, node_data

#     def _data_changed(self, cell, data=None, **kwargs):
#         # print('DATA=', data)
#         if data and cell.column() == 1:
#             # self.child(0).cell(1).set_data(data.AcquisitionStatus)
#             self.child(0).cell(1).set_data(data.ImageCounters)
#         super(LimaAcqStatusTreeAdapter, self)._data_changed(cell, data=data, **kwargs)
