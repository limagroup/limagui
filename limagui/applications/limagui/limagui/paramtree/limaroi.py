# -*- coding: utf-8 -*-

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/10/2018"


from Lima.Core import Roi, Size, Point
from limagui.paramtree import SubListTreeAdapter, treeadapterdef


_TOPLEFT_X_NODE_NAME = 'Topleft X'
_TOPLEFT_Y_NODE_NAME = 'Topleft Y'
_WIDTH_NODE_NAME = 'Width'
_HEIGHT_NODE_NAME = 'Height'


@treeadapterdef(Roi)
class LimaRoiTreeAdapter(SubListTreeAdapter):
    def __init__(self, *args, **kwargs):
        # kwargs['sub_data'] = [-1, -1, -1, -1]
        kwargs['sub_names'] = [_TOPLEFT_X_NODE_NAME, _TOPLEFT_Y_NODE_NAME,
                               _WIDTH_NODE_NAME, _HEIGHT_NODE_NAME]
        kwargs['editable'] = False
        super(LimaRoiTreeAdapter, self).__init__(*args, **kwargs)

    def _sub_to_root(self, root_data, row, cell_data):
        topLeft = root_data.getTopLeft()
        size = root_data.getSize()
        if row == 0:
            topLeft = Point(cell_data, topLeft.y)
        elif row == 1:
            topLeft = Point(topLeft.x, cell_data)
        elif row == 2:
            size = Size(cell_data, size.getHeight())
        elif row == 3:
            size = Size(size.getWidth(), cell_data)
        return Roi(topLeft, size)

    def _root_to_sub(self, root_data):
        top_left = root_data.getTopLeft()
        size = root_data.getSize()
        return [top_left.x, top_left.y, size.getWidth(), size.getHeight()]
