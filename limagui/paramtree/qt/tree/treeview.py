# coding: utf-8

from __future__ import absolute_import


__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from six import string_types


from .. import Qt
from .qmodel import QModel
from .delegate import QDelegate
from .editor import qeditor_for_type
from .qhelpers import nodeFromModelIndex, IndexExceptions


class TreeView(Qt.QTreeView):

    def __init__(self, parent=None, model=None):
        super(TreeView, self).__init__(parent)

        self._locked = False
        self._userEditTriggers = self.editTriggers()

        if int(Qt.qVersion().split('.')[0]) <= 4:
            self.header().setResizeMode(Qt.QHeaderView.ResizeToContents)
        else:
            self.header().setSectionResizeMode(Qt.QHeaderView.ResizeToContents)

        self.expanded.connect(self.__expanded)
        self.collapsed.connect(self.__collapsed)
        self.__delegates = []

        if model:
            self.setModel(model)

    def lockTree(self, lock=True):
        if not lock:
            self.unlockTree()
        elif not self._locked: 
            self._userEditTriggers = self.editTriggers()
            for delegate in self.__delegates.values():
                delegate.setLocked(True)
            self.setEditTriggers(Qt.QAbstractItemView.NoEditTriggers)
            self._locked = True

    def unlockTree(self):
        if self._locked:
            for delegate in self.__delegates.values():
                delegate.setLocked(False)
            self.setEditTriggers(self._userEditTriggers)
            self._locked = False

    def resizeEvent(self, event):
        self.__relayout()
        super(TreeView, self).resizeEvent(event)

    def __relayout(self):
        """
        This forces the TreeView to refetch the sizeHint of every cell.
        Only way I found to correctly set the row heights.
        Also, DictTree doesn work without this.
        TODO : find a better way to do this
        """
        model = self.model()
        if model:
            model.layoutChanged.emit()

    def refresh(self, index):
        model = self.model()
        if model:
            model.refresh(index=index)

    def setModel(self, model):
        curModel = self.model()

        if curModel:
            curModel.rowsRemoved.disconnect(self.__rowsRemoved)
            curModel.sigCellReset.disconnect(self._onCellReset)
            self.__openPersistentEditors(Qt.QModelIndex(),
                                         openEditor=False)
            for col in range(curModel.columnCount()):
                self.setItemDelegateForColumn(col, None)

        self.__delegates = {}

        super(TreeView, self).setModel(model)

        if model:
            for col in range(model.columnCount()):
                self.__delegates[col] = delegate = QDelegate()
                self.setItemDelegateForColumn(col, delegate)
            model.rowsRemoved.connect(self.__rowsRemoved)
            model.sigCellReset.connect(self._onCellReset)
            self.__openPersistentEditors(Qt.QModelIndex(),
                                         openEditor=True)

    def rowsInserted(self, index, start, end):
        self.__openPersistentEditors(index, False)
        super(TreeView, self).rowsInserted(index, start, end)
        self.__openPersistentEditors(index, True)

    def rowsAboutToBeRemoved(self, index, start, end):
        self.__openPersistentEditors(index, False)
        super(TreeView, self).rowsAboutToBeRemoved(index, start, end)
    
    def _onCellReset(self, index):
        print('RESET IN')
        self.__closePersistentEditors(index.parent())
        self.__openPersistentEditors(index.parent())
        print('RESET OUT')

    def __rowsRemoved(self, parent, start, end):
        self.__openPersistentEditors(parent, True)

    def __expanded(self, index):
        self.__openPersistentEditors(index, True)
        # self.__relayout()

    def __collapsed(self, index):
        self.__openPersistentEditors(index, False, onCollapse=True)
        # self.__relayout()

    def __closePersistentEditors(self,
                                 parent,
                                 onCollapse=False):
        self.__openPersistentEditors(parent,
                                     openEditor=False,
                                     onCollapse=onCollapse)

    def __openPersistentEditors(self,
                                parent,
                                openEditor=True,
                                onCollapse=False):
        # TODO : close editors on column hide/show events
        model = self.model()

        if not model:
            return

        if openEditor:
            meth = self.openPersistentEditor
        else:
            meth = self.closePersistentEditor

        if not parent.isValid():
            parent = self.rootIndex()

        children = []
        if onCollapse or self.isExpanded(parent)\
                or parent == self.rootIndex()\
                or not parent.isValid():
            children = [model.index(row, 0, parent)
                        for row in range(model.rowCount(parent))]

        columns = [col for col in range(model.columnCount())
                   if isinstance(self.itemDelegateForColumn(col), QDelegate)
                   and not self.isColumnHidden(col)]

        while len(children) > 0:
            curParent = children.pop(-1)

            if self.isExpanded(curParent):
                children.extend([model.index(row, 0, curParent)
                                 for row in range(model.rowCount(curParent))])

            try:
                node = nodeFromModelIndex(curParent)
            except IndexExceptions as ex:
                # TODO
                # print('abc', self.__class__, ex)
                continue
            for colIdx in columns:
                klass = node.view_adapter_type(colIdx)
                # if klass is None:
                #     klass = node.data_type(colIdx)
                editor = qeditor_for_type(klass)
                sibling = curParent.sibling(curParent.row(), colIdx)
                if editor:
                    if editor._isPersistent() or not openEditor:
                        meth(sibling)

                        if not openEditor:
                            delegate = self.itemDelegate(sibling)
                            delegate._forgetIndex(sibling)
                            # delegate.destroyEditor(editor)
                            # delegate._editorClosed(sibling)
                else:
                    self.dataChanged(sibling, sibling)

    def editorDestroyed(self, editor):
        super(TreeView, self).editorDestroyed(editor)

    def pathToIndex(self, tokens, fromRoot=True):
        """
        Returns a QModelIndex refering to the node with the given path.
        :param tokens: Lists of node names leading to the node.
        :param fromRoot: if True, starts from this tree's rootIndex, else
            starts from the model's first node.
        :return: A QModelIndex, which will be invalid if the node was not
            found (use QModelIndex::isValid).
        """
        if isinstance(tokens, (string_types,)):
            tokens = tokens.split('/')

        model = self.model()
        if fromRoot:
            root = self.rootIndex()
        else:
            root = Qt.QModelIndex()

        if model:
            return model.pathToIndex(tokens, parent=root)
        return Qt.QModelIndex()

    def clear(self, parent=Qt.QModelIndex()):
        """
        Clears the children of the given QModelIndex.
        :param parent: the index of the node to be cleared. Defaults to the
            root node.
        :return:
        """
        print('CLEAR')
        model = self.model()
        if model:
            self.__closePersistentEditors(None)
            model.clear(parent)

    # def dataChanged(self, topLeft, bottomRight, roles=[]):
    #     print('DC', roles)
    #     super(TreeView, self).dataChanged(topLeft, bottomRight)

    def setRootIndex(self, index):
        current = self.rootIndex()
        self.__closePersistentEditors(current.parent())
        super(TreeView, self).setRootIndex(index)
        self.__openPersistentEditors(index.parent())


class ParamTreeView(TreeView):
    def __init__(self, *args, model=None, **kwargs):
        if model is None:
            model = QModel()
        super().__init__(*args, model=model, **kwargs)
        model.setParent(self)

        stylesheet = """
                QTreeView {
                    background-color: #d2d8e0;
                    alternate-background-color: #FFFFFF;
                }
        """

        self.setAlternatingRowColors(True)
        self.setStyleSheet(stylesheet)