# coding: utf-8

from __future__ import absolute_import

from limagui.paramtree.tree.ioadapter import ioadapterdef

__authors__ = ["D. Naudet"]
__license__ = ""
__date__ = "01/09/2017"


from Lima import Core

from limagui.paramtree.tree.extra import create_tg_io_class

from .tangoionames import TangoLimaCtAcquisition


_TRIG2STRING = {"INTERNAL_TRIGGER": Core.IntTrig,
                    "EXTERNAL_TRIGGER": Core.ExtTrigSingle,
                    "EXTERNAL_TRIGGER_MULTI": Core.ExtTrigMult,
                    "INTERNAL_TRIGGER_MULTI": Core.IntTrigMult,
                    "EXTERNAL_TRIGGER_READOUT": Core.ExtTrigReadout,
                    "EXTERNAL_START_STOP": Core.ExtStartStop,
                    "EXTERNAL_GATE" : Core.ExtGate }

_STRING2TRIG = {val:key for key, val in _TRIG2STRING.items()}


TangoAcqTriggerMode = create_tg_io_class(TangoLimaCtAcquisition.TRIGGERMODE,
                                         data_type=Core.TrigMode,
                                         tango_name='acq_trigger_mode',
                                         tango_conversion=(_TRIG2STRING.get, _STRING2TRIG.get))
    
